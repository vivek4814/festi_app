//
//  SGPicker.swift
//  FESTI
//
//  Created by salman on 18/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class SGPicker: NSObject {
    override init() {}

    typealias CompletionBlock = (_ selectedItem: String?)-> Void
    var completion: CompletionBlock?
    private var dataSource: [String]?
    let picker = UIPickerView()
    func showPicker(datasource: [String], completion: @escaping CompletionBlock)-> UIPickerView {
        self.completion = completion
        self.dataSource = datasource
        print("receive", self.dataSource)
        DispatchQueue.main.async {
            self.picker.dataSource = self
            self.picker.delegate = self
            self.picker.reloadAllComponents()
        }
        return picker
    }
}


extension SGPicker: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        print("return",dataSource?.count ?? 0)
        return dataSource?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource?[row] ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        completion?(dataSource?[row] ?? "")
    }
}
