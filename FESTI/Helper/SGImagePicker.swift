//
//  SGImagePicker.swift
//  SGImagePickerDemo
//
//  Created by salman on 22/08/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit


//Keep strong reference


class SGImagePicker: NSObject {
    private override init() {}
    let imagePicker = UIImagePickerController()
    typealias CompletionBlock = (_ pickedImage: UIImage?)-> Void
    var completion: CompletionBlock?
    var enableEditing: Bool!
    
    enum ImageType {
        case camera
        case library
    }
    
    init(enableEditing: Bool) {
        super.init()
        imagePicker.delegate = self
        imagePicker.allowsEditing = enableEditing
        self.enableEditing = enableEditing
    }
    
    private func checkPlistKeys(isFor: ImageType) -> Bool {
        if isFor == .camera {
            if let camera = Bundle.main.object(forInfoDictionaryKey: "NSCameraUsageDescription") as? String {
                print(camera)
            } else {
                self.showAlert(title: "Camera", description: "NSCameraUsageDescription not found in Info.plist.")
                print("NSCameraUsageDescription not found in Info.plist.")
                return false
            }
        }else {
            if let library = Bundle.main.object(forInfoDictionaryKey: "NSPhotoLibraryUsageDescription") as? String {
                print(library)
            } else {
                self.showAlert(title: "Library", description: "NSPhotoLibraryUsageDescription not found in Info.plist.")
                print("NSPhotoLibraryUsageDescription not found in Info.plist.")
                return false
            }
        }
        return true
    }
    
    func getImage(from: ImageType, completion: @escaping CompletionBlock) {
        self.completion = completion
        if checkPlistKeys(isFor: from) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let rootVC = appDelegate.window?.rootViewController
            if from == .camera {
                if !UIImagePickerController.isSourceTypeAvailable(.camera){
                    self.showAlert(title: "Camera", description: "Camera is not supprted")
                    return
                }
                imagePicker.sourceType = .camera
            } else {
                if !UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                    self.showAlert(title: "Camera", description: "PhotoLibrary is not supprted")
                    return
                }
                imagePicker.sourceType = .photoLibrary
            }
            rootVC?.modalPresentationStyle = .fullScreen
            rootVC?.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func showAlert(title: String, description: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let rootVC = appDelegate.window?.rootViewController
        
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        rootVC?.present(alert, animated: true, completion: nil)
    }
    
}

extension SGImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if enableEditing {
            if let image = info[.editedImage] as? UIImage{
                completion?(image)
            } else {
                completion?(nil)
            }
        } else {
            if let image = info[.originalImage] as? UIImage{
                completion?(image)
            } else {
                completion?(nil)
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
