//
//  SGHelper2.1.swift
//  SG
//
//  Created by Salman Ghumsani on 6/30/16.
//  Copyright © 2016 Salman Ghumsani. All rights reserved.
//

import UIKit
import CoreLocation

enum Themes : Int {
    case black
    case white
}

enum Language : Int {
    case en
    case ar
}
enum DateFormat: String {
    case EEE_MMM_DD_YYYY = "EEE MMM dd, yyyy"
    case MM_DD = "MMM d"
    case DD_MM = "d MMM"
    case MMM_DD_YYYY = "MMM dd, yyyy"
    case hhmm_a_MMMDDYYYY = "h:mm a, MMM dd, yyyy"
    case MM_d_YYYY = "MM/dd/yyyy"
    case dd_MM_yyyy = "dd-MM-yyyy"
    case num_dd_MM_yyyy = "dd/MM/yyyy"
    case yyyy_MM_dd = "yyyy-MM-dd"
    case yyyy_dd_MM = "yyyy-dd-MM"
    case yyyyMMDD = "yyyy/MM/dd"
    case HHmmss = "HH:mm:ss"
    case hhmm_a = "h:mm a"
    case ddMMhhmm_a = "d MMM, h:mm a"
    case MMddyyyy = "MM-dd-yyyy"
    case ddMMyyyyhhmm_a = "dd-MM-yyyy, h:mm a"
    case MMddyyyyhhmm_a = "MM-dd-yyyy, h:mm a"
    case yyyyMMddHHMMSS = "yyyy-MM-dd HH:mm:ss"
    case ddMMMyyyy_a = "dd-MMM-yyyy HH:mm:ss"
    case EEEE = "EEEE"
    case HHMM = "HH:mm"
    case dd_MMM_YYYY = "dd, MMM yyyy"
    case dd_MM_yyyy_HHmmss = "dd MMM  yyyy HH:mm:ss"
    case EEE_MMM_DD = "EEE MMM dd"
    case yyyy_MM_ddTHHmmss_sssZ = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
    
}

typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint {
        return points.startPoint
    }
    
    var endPoint : CGPoint {
        return points.endPoint
    }
    
    var points : GradientPoints {
        switch self {
        case .topRightBottomLeft:
            return (CGPoint(x: 0.0,y: 1.0), CGPoint(x: 1.0,y: 0.0))
        case .topLeftBottomRight:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 1,y: 1))
        case .horizontal:
            return (CGPoint(x: 0.0,y: 0.5), CGPoint(x: 1.0,y: 0.5))
        case .vertical:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 0.0,y: 1.0))
        }
    }
}

struct AppColor {
    static var kPINK_COLOR_GRADIENT = [UIColor.getRGBColor(250, g: 26, b: 169).cgColor, UIColor.getRGBColor(250, g: 0, b: 94).cgColor]
    static var kBLUE_COLOR_GRADIENT = [UIColor.getRGBColor(166, g: 26, b: 240).cgColor, UIColor.getRGBColor(54, g: 41, b: 199).cgColor]
    static var kPINK_COLOR = UIColor.getRGBColor(250, g: 28, b: 172)
    static var kDARKGREY_COLOR = UIColor.getRGBColor(31, g: 31, b: 31)
    static var kBLUE_COLOR = UIColor.getRGBColor(98, g: 35, b: 220)
    static var kGREY_COLOR = UIColor.getRGBColor(85, g: 85, b: 85)
}
var myBundle:Bundle?
var viewBackground:UIView?
var mainNavigation:UINavigationController?
var APP_DELEDATE = UIApplication.shared.delegate as! AppDelegate

//MARK: Extension UIColor
extension UIColor {
    var r: CGFloat {
        return self.cgColor.components![0]
    }
    
    var g: CGFloat {
        return self.cgColor.components![1]
    }
    
    var b: CGFloat {
        return self.cgColor.components![2]
    }
    
    var alpha: CGFloat {
        return self.cgColor.components![3]
    }
    class func getRGBColor(_ r:CGFloat,g:CGFloat,b:CGFloat)-> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
}
extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}
//MARK: Extension UIImage
extension UIImage {
    func imageWithColor(_ color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        let context = UIGraphicsGetCurrentContext()
        context!.translateBy(x: 0, y: self.size.height)
        context!.scaleBy(x: 1.0, y: -1.0);
        context!.setBlendMode(CGBlendMode.normal)
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context!.clip(to: rect, mask: self.cgImage!)
        context!.fill(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension UIImage {
     func fromColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}


extension UINavigationController {
    func applyNavigationShadow() {
        UINavigationBar.appearance().layer.masksToBounds = false
        UINavigationBar.appearance().layer.shadowColor = UIColor.lightGray.cgColor
        UINavigationBar.appearance().layer.shadowOpacity = 0.8
        UINavigationBar.appearance().layer.shadowOffset = CGSize(width: 0, height: 2.0)
        UINavigationBar.appearance().layer.shadowRadius = 2
    }
}

//MARK: Extension UIViewController
extension UIViewController {
    
    func showDescriptionInAlert(_ desc: String, title: String, animationTime: Double, descFont: UIFont) {
        viewBackground?.removeFromSuperview()
        viewBackground = UIView(frame: UIScreen.main.bounds)
        let viewAlertContainer = UIView()
        let txtViewDesc = UITextView()
        viewAlertContainer.backgroundColor = UIColor.white
        viewBackground!.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        txtViewDesc.backgroundColor = UIColor.white
        viewBackground!.alpha = 0.0
        txtViewDesc.text = desc
        txtViewDesc.isEditable = false
        txtViewDesc.font = descFont
        viewAlertContainer.makeRoundCorner(10)
        txtViewDesc.textAlignment = .center
        var txtViewHeight = CGRect(x: 0, y: 30, width: UIScreen.main.bounds.width-80, height: 100)
        let height = heightForLabel(desc, width: UIScreen.main.bounds.width-80, font: descFont)
        if height > 80 && height < 300 {
            txtViewHeight.size.height = height+20
        }
        let lblTitle = UILabel(frame: CGRect(x: 0, y: 8, width: UIScreen.main.bounds.width-80, height: 20))
        lblTitle.text = title
        lblTitle.textColor = UIColor.black
        lblTitle.font = UIFont.systemFont(ofSize: 18)
        lblTitle.backgroundColor = UIColor.white
        lblTitle.textAlignment = .center
        txtViewDesc.frame = txtViewHeight
        viewAlertContainer.frame = CGRect(x: 40, y: 100, width: UIScreen.main.bounds.width-80, height: txtViewHeight.size.height+30)
        viewAlertContainer.center = viewBackground!.center
        viewAlertContainer.addSubview(lblTitle)
        viewAlertContainer.addSubview(txtViewDesc)
        viewBackground!.addSubview(viewAlertContainer)
        self.view.addSubview(viewBackground!)
        UIView.animate(withDuration: animationTime) {
            viewBackground!.alpha = 1.0
        }
    }
    
    func hideDescriptionAlert(_ animationTime: Double) {
        UIView.animate(withDuration: animationTime, animations: { 
            viewBackground?.alpha = 0.0
        }) { (compelete) in
            if compelete {
                viewBackground?.removeFromSuperview()
            }
        }
    }
    
    func showOkAlert(_ msg: String) {
        let alert = UIAlertController(title:
            "", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showOkAlertWithHandler(_ msg: String, handler: @escaping ()->Void){
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (type) -> Void in
            handler()
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithActions(msg: String, titles: [String], handler:@escaping (_ clickedIndex: Int) -> Void) {
        if titles.last?.lowercased() == "cancel" {
            let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
            for title in titles {
                if title.lowercased() == "cancel" {
                    let action  = UIAlertAction(title: title, style: .cancel, handler: { (alertAction) in
                        //fall the Call when user clicked
                        let index = titles.index(of: alertAction.title!)
                        if index != nil {
                            handler(index!+1)
                        }
                        else {
                            handler(0)
                        }
                    })
                    alert.addAction(action)
                } else {
                    let action  = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                        //fall the Call when user clicked
                        let index = titles.index(of: alertAction.title!)
                        if index != nil {
                            handler(index!+1)
                        }
                        else {
                            handler(0)
                        }
                    })
                    alert.addAction(action)
                }
            }
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view //to set the source of your alert
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
            }
            present(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
            
            for title in titles {
                
                let action  = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                    //Call back fall when user clicked
                    let index = titles.index(of: alertAction.title!)
                    if index != nil {
                        handler(index!+1)
                    }
                    else {
                        handler(0)
                    }
                    
                })
                alert.addAction(action)
            }
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view //to set the source of your alert
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
            }
            present(alert, animated: true, completion: nil)
        }
    }
    
    func showOkCancelAlertWithAction(_ msg: String, handler:@escaping (_ isOkAction: Bool) -> Void) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let okAction =  UIAlertAction(title: "OK", style: .default) { (action) -> Void in
            return handler(true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            return handler(false)
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

//MARK: Extension NSDate
extension Date {
    func toString(_ validDateFormatter: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = validDateFormatter //"dd MMM yyyy" //yyyy-mm-dd hh:mm 
        return dateFormatter.string(from: self as Date)
    }
    
   
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year ?? 0
    }
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        return dateFormatter.string(from: self).capitalized
    }
//    func month() -> String? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "LLLL"
//        return dateFormatter.string(from: self).capitalized
//    }
    func day()-> Int? {
        return Calendar.current.ordinality(of: .day, in: .month, for: self)
    }
    func year()-> Int? {
        return Calendar.current.component(.year, from: self)
    }
}

//MARK: Extension CLLocation

extension CLLocation{
    func getCityName(_ City:@escaping (_ city: String)->Void){
        
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(self, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
            print(placeMark.addressDictionary ?? "")
            
            // Location name
            if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
                print(locationName)
            }
            
            // Street address
            if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                print(street)
            }
            
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                City(city as String)
                print(city)
            }
            
            // Zip code
            if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                print(zip)
            }
            
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? NSString {
                print(country)
            }
        })
    }
    
}


//MARK: Extension String

extension String {
    
    func getDateInstance(validFormate: String)-> Date? {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = validFormate
        dateFormater.date(from:self)
        return dateFormater.date(from:self)
    }
    
    func hasSpecialCharacter() -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    
    func hasSpecialSymbolCharacter() -> Bool {
        let characterset = CharacterSet(charactersIn: ",")
        if self.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    
    var onlyNumbers : String {
        get{
            return self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        }
    }
    
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var isContainAnyNumber : Bool{
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRange = self.rangeOfCharacter(from: decimalCharacters)
        if decimalRange != nil {
            return true
        }
        else{
            return false
        }
    }
    
    var isContainAnyAlfabits : Bool {
        let str1 = self
        let str2 = self
        let capitalizedLetters = CharacterSet.capitalizedLetters
        let lowercaseLetters   = CharacterSet.lowercaseLetters
        let capitalRange = str1.rangeOfCharacter(from: capitalizedLetters)
        let lowerRange = str2.rangeOfCharacter(from: lowercaseLetters)

        if capitalRange != nil{
            return true
        }
        else if  lowerRange != nil{
            return true
        }
        else{
            return false
        }
    }
//    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
//
//        return ceil(boundingBox.height)
//    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func fromBase64() -> String? {
        let sDecode = self.removingPercentEncoding
        return sDecode
    }
}

//MARK: Extension UIView
extension UIView {
    
    
    func applyGradient(colors: [CGColor], orientation: GradientOrientation = .horizontal) {
        let gradient = CAGradientLayer()
        DispatchQueue.main.async {
            gradient.frame = self.bounds
            gradient.cornerRadius = self.layer.cornerRadius
        }
        if self is UIButton {
            let btn = self as! UIButton
            if let imgView = btn.imageView {
                btn.bringSubviewToFront(imgView)
            }
        }
        gradient.colors = colors
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.withAlphaComponent(0.8).cgColor, colorTop.withAlphaComponent(0.8).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        DispatchQueue.main.async {
            gradientLayer.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
            gradientLayer.cornerRadius = self.layer.cornerRadius
        }
        
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    class func fromNib<T : UIView>(xibName: String) -> T { 
        return Bundle.main.loadNibNamed(xibName, owner: nil, options: nil )![0] as! T
    }
    
    
    func roundedCorner(cornerRadii: CGSize, corners: UIRectCorner) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: cornerRadii)
        
        let shape = CAShapeLayer()
        shape.frame = bounds
        shape.path = maskPath.cgPath
        layer.mask = shape
     
    }
    
    
    func typeName(_ some: Any) -> String {
        return (some is Any.Type) ? "\(some)" : "\(type(of: some))"
    }
    
    func makeRounded(){
            self.layer.cornerRadius = self.frame.size.width/2
            self.clipsToBounds = true
    }
    
    func makeRoundCorner(_ radius:CGFloat){
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func makeBorder(_ width:CGFloat,color:UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.clipsToBounds = true
    }
    
    //give the border to UIView
    func border(radius : CGFloat,borderWidth : CGFloat,color :CGColor){
        self.layer.masksToBounds = true
        self.layer.cornerRadius  = radius
        self.layer.borderWidth   = borderWidth
        self.layer.borderColor   = color
    }
    
    //give the circle border to UIView
    func circleBorder() {
        let hight = self.layer.frame.height
        let width = self.layer.frame.width
        if hight < width {
            self.layer.cornerRadius = hight/2
            self.layer.masksToBounds = true
        }
        else {
            self.layer.cornerRadius  = width/2
            self.layer.masksToBounds = true
        }
    }
    
    func addShadow(radius: CGFloat, cornerRadius: CGFloat = 1){
        self.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.8).cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = radius
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.masksToBounds = false
        self.layer.cornerRadius = cornerRadius
    }
    
    
    func snapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, true, UIScreen.main.scale)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    func close() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0.0
        }, completion: { (compelete) in
            if compelete {
                self.removeFromSuperview()
            }
        })
    }
    
    func addThreeSideShadow() {
        layer.cornerRadius = 10
        let shadowSize:CGFloat = 3
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: shadowSize + shadowSize + 2,
                                                   width: self.frame.size.width + shadowSize,
                                                   height: self.frame.size.height + shadowSize))
        self.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.4).cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
    }
    
}

@IBDesignable
class ShadowView: UIView {
    @IBInspectable var corner: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = corner
        }
    }
    
    @IBInspectable var shadowSize: CGFloat = 0.0 {
        didSet {
            let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                       y: shadowSize + shadowSize + 2,
                                                       width: self.frame.size.width + shadowSize,
                                                       height: self.frame.size.height + shadowSize))
            self.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.4).cgColor
            self.layer.shadowOpacity = 0.5
            self.layer.shadowPath = shadowPath.cgPath
            
        }
    }
}

@IBDesignable
class SearchShadow: UIView {
    @IBInspectable var corner: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = corner
        }
    }
    
    @IBInspectable var shadowSize: CGFloat = 0.0 {
        didSet {
            let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                       y: shadowSize + shadowSize + 2,
                                                       width: self.frame.size.width + shadowSize,
                                                       height: self.frame.size.height + shadowSize))
            self.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.4).cgColor
            self.layer.shadowOpacity = 0.5
            self.layer.shadowPath = shadowPath.cgPath

        }
    }
}

//MARK: Global Helper Method
func setLanguage(_ lang:String){
    let path    =   Bundle.main.path(forResource: lang, ofType: "lproj")
    if path == nil{
        myBundle  = Bundle.main
    }
    else{
        myBundle = Bundle(path:path!)
        if (myBundle == nil) {
            myBundle = Bundle.main
        }
    }
}



func setUserDefault(_ key:String,value:String){
    let defaults = UserDefaults.standard
    defaults.setValue(value, forKey: key)
}

func getUserDefault(_ key:String) ->String{
    let defaults = UserDefaults.standard
    let val = defaults.value(forKey: key)
    if val != nil{
        return val! as! String 
    }
    else{
        return ""
    }
}

func removeUserDefault(_ key:String){
    let defaults = UserDefaults.standard
    defaults.removeObject(forKey: key)
}

func setDictUserDefault(_ detail:Dictionary<String,Any>){
    for (key,value) in detail{
        if(detail[key] != nil) {
            setUserDefault(key, value: "\(value)")
        }
    }
}

func heightForLabel(_ text:String,width:CGFloat,font:UIFont) -> CGFloat{
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.sizeToFit()
    return label.frame.height
}

func print_debug<T>(_ obj:T,file: String = #file, line: Int = #line, function: String = #function) {
    print("Line:'\(line)' Function:'\(function)' ::\(obj)")
}

func uniq<S: Sequence, E: Hashable>(_ source: S) -> [E] where E==S.Iterator.Element {
    var seen: [E:Bool] = [:]
    return source.filter { seen.updateValue(true, forKey: $0) == nil }
}

//MARK: Helper Class

class SGHelper: NSObject {
    static let viewLogo = UIView()
    struct Platform {
        static var isSimulator: Bool {
            return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
            //return TARGET_IPHONE_SIMULATOR != 0 // Use this line in Xcode 6
        }
    }
    class func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func getDictionary(_ detail: Any?) -> Dictionary<String, Any> {
        guard let dict = detail as? Dictionary<String, Any> else {
            return ["":"" as Any]
        }
        return dict
    }
    
    class func convertArrayToString(arr:Array<Any>)->String{
        var retStr = ""
        for str in arr{
            if retStr != ""{
                retStr = "\(retStr),\(str)"
            }
            else{
                retStr = "\(str)"
            }
        }
        
        return retStr
    }
}

//MARK: Extension UIDatePicker
extension UIDatePicker {
    /// Returns the date that reflects the displayed date clamped to the `minuteInterval` of the picker.
    /// - note: Adapted from [ima747's](http://stackoverflow.com/users/463183/ima747) answer on [Stack Overflow](http://stackoverflow.com/questions/7504060/uidatepicker-with-15m-interval-but-always-exact-time-as-return-value/42263214#42263214})
    public var clampedDate: Date {
        let referenceTimeInterval = self.date.timeIntervalSinceReferenceDate
        let remainingSeconds = referenceTimeInterval.truncatingRemainder(dividingBy: TimeInterval(minuteInterval*60))
        let timeRoundedToInterval = referenceTimeInterval - remainingSeconds
        return Date(timeIntervalSinceReferenceDate: timeRoundedToInterval)
    }
}


extension String {
    var isValidEmail: Bool {
        get {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: self)
        }
    }
    
    var isNumeric: Bool {
        get {
            let components = self.components(separatedBy: CharacterSet.decimalDigits.inverted)
            return components.count == 1
        }
    }
    
    func getDate(format: DateFormat) -> Date? {
        let dateformater = DateFormatter()
        dateformater.dateFormat = format.rawValue
        let date = dateformater.date(from: self)
        return date
    }
    
    func changeDateFormat(fromFromat currentFormat:DateFormat, toFormat changedFormat:DateFormat ) -> String {
        let dateString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        if dateString.isEmpty {
            return dateString
        }
        
        let date = dateString.getDate(format: currentFormat)
        return date?.getString(format: changedFormat) ?? self
    }
    
    
}

extension Date {
    func getString(format: DateFormat) -> String {
        let dateformater = DateFormatter()
        dateformater.dateFormat = format.rawValue
        return dateformater.string(from: self)
    }
    
    func timeAgoSinceDate() -> String {

        // From Time
        let fromDate = self

        // To Time
        let toDate = Date()

        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "year ago" : "\(interval)" + " " + "years ago"
        }

        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "month ago" : "\(interval)" + " " + "months ago"
        }

        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "day ago" : "\(interval)" + " " + "days ago"
        }

        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {

            return interval == 1 ? "\(interval)" + " " + "hour ago" : "\(interval)" + " " + "hours ago"
        }

        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {

            return interval == 1 ? "\(interval)" + " " + "minute ago" : "\(interval)" + " " + "minutes ago"
        }

        return "a moment ago"
    }
    
    
    func timeAgoSinceDate1() -> String {
        
        // From Time
        let fromDate = self
        
        // To Time
        let toDate = Date()
        
        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM, yyyy"
            let dayInWeek = dateFormatter.string(from: self)
            return dayInWeek
        }
        
        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEE, dd MMMM"
            let dayInWeek = dateFormatter.string(from: self)
            return dayInWeek
        }
        
        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {
            
            if interval == 1 {
                return "Yesterday"
            }else if interval < 8 {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "EEEE"
                let dayInWeek = dateFormatter.string(from: self)
                return dayInWeek
                
            } else {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "EEE, dd MMMM"
                let dayInWeek = dateFormatter.string(from: self)
                return dayInWeek
            }
            
        }
        
        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {
            
            return "Today"
        }
        
        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {
            
            return "Today"
        }
        
        return "Today"
    }
}


