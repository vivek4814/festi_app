//
//  User.swift
//  FESTI
//
//  Created by salman on 16/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseStorage
var globalUser: User?


class User: NSObject {
    
    var uid = ""
    var firstName = ""
    var lastName = ""
    var galleryImage0 = ""
    var galleryImage1 = ""
    var galleryImage2 = ""
    var galleryImage4 = ""
    var galleryImage3 = ""
  
    var genderPreference = ""
    var dob: Date?
    var email = ""
    var gender = ""
    var bio = ""
    var profileImage = ""
    var lat : String = ""
    var long : String = ""
    var profilePic = ""

    var favArtistId = [String]()
    var favMusicId = [String]()
    
    init(info: [String: Any]) {
        firstName = info["first_name"] as? String ?? ""
        lastName = info["last_name"] as? String ?? ""
        galleryImage0 = info["gallery_image0"] as? String ?? ""
        galleryImage1 = info["gallery_image1"] as? String ?? ""
        galleryImage2 = info["gallery_image2"] as? String ?? ""
        galleryImage3 = info["gallery_image3"] as? String ?? ""
        galleryImage4 = info["gallery_image4"] as? String ?? ""
        
        if let prefence = info["gender_preferences"] as? String {
            genderPreference = prefence
        }else {
            genderPreference = info["gender_preference"] as? String ?? ""
        }
        
        
        print("dob:",info["dob"] as? Timestamp)
        if let _dob = info["dob"] as? Int{
            dob = Timestamp.init(seconds: Int64(_dob), nanoseconds: 0).dateValue()
        }
        
       // dob = (info["dob"] as? Timestamp)?.dateValue()
        email = info["email"] as? String ?? ""
        gender = info["gender"] as? String ?? ""
        bio = info["bio"] as? String ?? ""
        lat = info["lat"] as? String ?? ""
        long = info["long"] as? String ?? ""
        profileImage = info["profile"] as? String ?? ""
        favArtistId = info["favourites_artist_ids"] as? [String] ?? []
        favMusicId = info["favourites_music_ids"] as? [String] ?? []
        uid = info["user_id"] as? String ?? ""
        profilePic = info["profile"] as? String ?? ""

        
    }
    
    
    
    class func getUser(id: String = "", handler: @escaping (_ user: User)-> Void) {
        var userId = id
        if userId == "" {
            if let user = Auth.auth().currentUser?.uid {
                userId = user
            }
            if userId == "" {
                return
            }
        }
        Firestore.firestore().collection("users").document(userId).getDocument { (snapshot, error) in
            if let data = snapshot?.data() {
                let user = User(info: data)
                globalUser = user
                handler(user)
              
            }
        }
    }
    
    class func getUserById(userId: String, handler: @escaping (User?) -> Void) {
        SwiftLoader.show(title: "Loading...", animated: true)
        Firestore.firestore().collection("users").document(userId).getDocument { (doc, error) in
            SwiftLoader.hide()
            if let value = doc?.data() {
                handler(User(info: value))
            } else {
                handler(nil)
            }
        }
    }
    
    
    class func updateProfile(image: UIImage, handler: @escaping (_ success: Bool)-> Void) {
        SwiftLoader.show(title: "Uploading...", animated: true)
        if let imageData = image.jpegData(compressionQuality: 0.5) {
            if let userId = Auth.auth().currentUser?.uid {
                Storage.storage().reference().child("userPics").child(userId).child("\(Date().timeIntervalSince1970).jpeg").putData(imageData, metadata: nil, completion: { (metadata, error) in
                    if error == nil {
                        let path = metadata?.path
                        let values = ["profile": path!] as [String : Any]
                        Firestore.firestore().collection("users").document(userId).setData(values, merge: true)
                    }
                    handler(true)
                    SwiftLoader.hide()
                })
            }else {
                handler(false)
                SwiftLoader.hide()
            }
        }
    }
    
    class func updateGalleryPhoto(image: UIImage, atIndex: Int,  handler: @escaping (_ success: Bool)-> Void) {
        SwiftLoader.show(title: "Uploading...", animated: true)
        if let imageData = image.jpegData(compressionQuality: 0.5) {
            if let userId = Auth.auth().currentUser?.uid {
                Storage.storage().reference().child("galleryImage").child(userId).child("\(Date().timeIntervalSince1970).jpeg").putData(imageData, metadata: nil, completion: { (metadata, error) in
                    if error == nil {
                        let path = metadata?.path
                        let values = ["gallery_image\(atIndex)": path!] as [String : Any]
                        Firestore.firestore().collection("users").document(userId).setData(values, merge: true)
                    }
                    handler(true)
                    SwiftLoader.hide()
                })
            } else {
                handler(false)
                SwiftLoader.hide()
            }
        }
    }
    
    class func updateUser(data: [String: Any]) {
        if let userId = Auth.auth().currentUser?.uid {
            Firestore.firestore().collection("users").document(userId).updateData(data)
        }
    }
    
    class func login(email: String, pass: String, handler: @escaping (_ success: Bool)-> Void) {
        SwiftLoader.show(title: "Loading...", animated: true)
        Auth.auth().signIn(withEmail: email, password: pass) { (result, error) in
            SwiftLoader.hide()
            if let _ = result?.user {
                handler(true)
            } else {
                APP_DELEDATE.window?.rootViewController?.showOkAlert(error?.localizedDescription ?? "")
                handler(false)
            }
        }
    }
    
    class func resetPass(email: String, handler: @escaping (_ success: Bool)-> Void) {
        SwiftLoader.show(title: "Loading...", animated: true)
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            print(error)
            SwiftLoader.hide()
            if error == nil {
                handler(true)
            } else {
                APP_DELEDATE.window?.rootViewController?.showOkAlert(error?.localizedDescription ?? "")
                handler(false)
            }
        }
    }
    
    class func signup(firstName: String, lastName: String, gender: String, dob: Double, email: String, pass: String, handler: @escaping (_ success: Bool)-> Void) {
        SwiftLoader.show(title: "Loading...", animated: true)
        Auth.auth().createUser(withEmail: email, password: pass) { (result, error) in
            SwiftLoader.hide()
            if let user = result?.user {
                let data = ["date_created": Timestamp(date: Date()) , "email": email, "first_name": firstName,"last_name": lastName, "dob": dob, "gender": gender.lowercased(), "user_id": user.uid] as [String : Any]
                Firestore.firestore().collection("users").document(user.uid).setData(data, merge: true)
                handler(true)
            } else {
                
                APP_DELEDATE.window?.rootViewController?.showOkAlert(error?.localizedDescription ?? "")
                handler(false)
            }
        }
    }
    
    class func SignUpWithFacebook(faceBookToken : AuthCredential,firstName: String, lastName: String, gender: String, dob: Double, email: String, handler: @escaping (_ success: Bool)-> Void) {
        
        Auth.auth().signIn(with: faceBookToken, completion: { (user, error) in
            SwiftLoader.hide()
            if let user = user?.user {
                let data = ["date_created": Timestamp(date: Date()) , "email": email, "first_name": firstName,"last_name": lastName, "dob": dob, "gender": gender.lowercased(), "user_id": user.uid] as [String : Any]
                Firestore.firestore().collection("users").document(user.uid).setData(data, merge: true)
                handler(true)
            } else {
                
                print("errorerror-\(error)")
                APP_DELEDATE.window?.rootViewController?.showOkAlert(error?.localizedDescription ?? "")
                handler(false)
            }
        })
        
    }
    
    
    class func loginWithFacebook(faceBookToken : AuthCredential, handler: @escaping (_ success: Bool)-> Void) {
        
        Auth.auth().signIn(with: faceBookToken, completion: { (user, error) in
            SwiftLoader.hide()
            if let _ = user?.user {
                handler(true)
            } else {
                APP_DELEDATE.window?.rootViewController?.showOkAlert(error?.localizedDescription ?? "")
                handler(false)
            }
            // Present the main view
            
        })
        
    }
}

extension Int{
    var dateVal: Date?{
        // convert Int to Double
        let interval = Double(self)
        if let d = Date.coordinate{
            return  Date(timeInterval: interval, since: d)
        }
        return nil
    }
}

extension Date{
    var intVal: Int?{
        if let d = Date.coordinate{
             let inteval = Date().timeIntervalSince(d)
             return Int(inteval)
        }
        return nil
    }
    
    func string(format: String) -> String {
           let formatter = DateFormatter()
           formatter.dateFormat = format
           return formatter.string(from: self)
       }


    // today's time is close to `2020-04-17 05:06:06`

    static let coordinate: Date? = {
        let dateFormatCoordinate = DateFormatter()
        dateFormatCoordinate.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let d = dateFormatCoordinate.date(from: Date().string(format: "yyyy-MM-dd HH:mm:ss")) {
            return d
        }
        return nil
    }()
}
