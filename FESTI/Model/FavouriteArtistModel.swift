//
//  FavouriteArtistModel.swift
//  FESTI
//
//  Created by salman on 29/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class FavouriteArtistModel : NSObject {
    var isSelected: Bool = false
    var id: String = "0"
    var userName = ""
    var image = UIImage()
    init(id: String, name: String, image: UIImage, isSelected: Bool) {
        self.id = id
        self.isSelected = isSelected
        self.image = image
        userName = name
    }
    
    class func getDummyData() -> [FavouriteArtistModel] {
        let names = ["Bon Jovi", "Britney Spears", "Bob Dylan", "Tupac Shakur", "Backstreet Boys", "Tim McGraw", "Rod Stewart", "Taylor Swift", "Santana", "Alan Jackson", "Guns N’ Roses", "Eminem","Kenny Rogers", "Shania Twain", "Kenny G", "Journey", "Celine Dion"]
        var arrData = [FavouriteArtistModel]()
        for (index,name) in names.enumerated() {
            arrData.append(FavouriteArtistModel(id: "\(index)", name: name, image: UIImage(named: "\(index+1)")!, isSelected: false))
        }
        return arrData
    }
    
}

