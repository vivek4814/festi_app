//
//  HomeModel.swift
//  FESTI
//
//  Created by salman on 04/03/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class HomeModel {
    class func getHomeData(handler: @escaping (_ user: [User]?)-> Void) {
        guard let userId = Auth.auth().currentUser?.uid else {
            handler(nil)
            return
        }
        
        User.getUser { (user) in
            print("My preference",user.genderPreference)
            Firestore.firestore().collection("users").whereField("gender", isEqualTo: user.genderPreference.lowercased()).getDocuments { (snapshot, error) in
                var users = [User]()
                for doc in snapshot!.documents {
                    let myUser = User(info: doc.data())
                    print("My gender", myUser.gender,myUser.uid,myUser.firstName,myUser.email)
                    if myUser.uid != userId {
                        users.append(myUser)
                    }
                    handler(users)
                }
            }
        }
    }
}
