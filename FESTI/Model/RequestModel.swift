//
//  RequestModel.swift
//  FESTI
//
//  Created by salman on 06/03/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class RequestModel: NSObject {
    var toId = ""
    var fromId = ""
    var fromFirstName = ""
    var fromLastName = ""
    var fromImage = ""
    var fromEmail = ""
    
    private init(info: [String: Any]) {
        toId = info["to_id"] as? String ?? ""
        fromId = info["from_id"] as? String ?? ""
        fromFirstName = info["first_name"] as? String ?? ""
        fromLastName = info["last_name"] as? String ?? ""
        fromImage = info["from_image"] as? String ?? ""
        fromEmail = info["from_email"] as? String ?? ""
    }
    
    class func makeRequest(toId: String, handler: @escaping (_ success: Bool)-> Void) {
        print("toIdtoId:-\(toId) :: globalUser?.uid:-\(globalUser?.uid)")
        let path = toId+":"+(globalUser?.uid ?? "")
        let request = Firestore.firestore().collection("request").document(path)
        let data = ["to_id": toId , "from_id": globalUser?.uid ?? "", "first_name": globalUser?.firstName ?? "", "last_name": globalUser?.lastName ?? "", "from_image": globalUser?.profileImage ?? "", "from_email": globalUser?.email ?? "", "request_id": request.documentID]
        request.setData(data)
        handler(true)
    }
    
    class func getAllMyRequest(handler: @escaping (_ allRequest: [RequestModel]?)-> Void) {
        Firestore.firestore().collection("request").getDocuments { (snap, error) in
            guard let documents = snap?.documents else{ return }
            var allRequest = [RequestModel]()
            for doc in documents {
                let request = RequestModel(info: doc.data())
                print("requestrequest",request.fromFirstName)
            
                print("\(globalUser?.uid ?? "") == \(request.toId)")
                if globalUser?.uid ?? "" == request.toId {
                    allRequest.append(request)
                    print(request.fromFirstName)
                }
            }
            handler(allRequest)
//            Firestore.firestore().collection("request").addSnapshotListener { (snapshot, err) in
//                guard let documents = snapshot?.documents else{ return }
//                for doc in documents {
//                    let request = RequestModel(info: doc.data())
//                    if globalUser?.uid ?? "" == request.toId {
//                        allRequest.append(request)
//                        print(request.fromFirstName)
//                    }
//                }
//                handler(allRequest)
//            }
        }
    }
    
}
