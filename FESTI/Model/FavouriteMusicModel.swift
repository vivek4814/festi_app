//
//  FavouriteMusicModel.swift
//  FESTI
//
//  Created by salman on 29/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class FavouriteMusicModel : NSObject {
    var isSelected: Bool = false
    var id: String = "0"
    var name = ""
    init(id: String, name: String, isSelected: Bool) {
        self.id = id
        self.isSelected = isSelected
        self.name = name
    }
    class func getDummyData()-> [FavouriteMusicModel] {
        let arrNames = ["Rock", "Alternative", "Classic Rock", "Classical", "Jazz", "Blues", "Classical Rock/Progressive", "Oldies", "Progressive Rock", "Progressive Rock", "Rap/hip-hop", "Dance/Electronica", "Folk", "Indie Rock", "Soul/funk", "Soundtracks/theme Songs", "Thrash Metal", "Grunge", "Country", "Muzziac added Púnk Rock", "Axelanti added Reggae"]
        
        var arrData = [FavouriteMusicModel]()
        for (index, name) in arrNames.enumerated() {
            arrData.append(FavouriteMusicModel(id: "\(index)", name: name, isSelected: false))
        }
        return arrData
    }

}

