//
//  LoginVC.swift
//  FESTI
//
//  Created by salman on 26/09/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FacebookLogin
import FacebookCore
import FirebaseAuth
import FBSDKLoginKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLogin.applyGradient(colors: AppColor.kPINK_COLOR_GRADIENT)
    }
    
    @IBAction func tapLogin(_ sender: Any) {
       if isValid() {
            User.login(email: txtEmail.text!, pass: txtPassword.text!) { (success) in
                print(success)
                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
                self.navigationController?.pushViewController(home, animated: true)
            }
        }
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapForgotPassword(_ sender: Any) {
        let reset = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
        self.navigationController?.pushViewController(reset, animated: true)
    }
    
    @IBAction func tapLoginWithFacebook(_ sender: Any) {
        
        let loginManager = LoginManager()
        SwiftLoader.show(title: "Loading...", animated: true)
        loginManager.logIn(permissions: ["email"], from: self) { (result, error) in
            print("\n\n result: \(String(describing: result))")
            print("\n\n Error: \(String(describing: error))")
            if (error == nil) {
                if(result?.isCancelled ?? false) {
                    SwiftLoader.hide()
                    //Show Cancel alert
                } else if(result?.grantedPermissions.contains("email") ?? false) {
                    self.returnUserData()
                    //fbLoginManager.logOut()
                }
            }else {
                SwiftLoader.hide()
            }
        }
    }
    
    func returnUserData() {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"email,name,id,first_name,gender,birthday,last_name"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil) {
                // Process error
                print("\n\n Error: \(String(describing: error) )")
            } else {
                guard let accessToken = AccessToken.current else {
                    print("Failed to get access token")
                    return
                }
                let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
                User.loginWithFacebook(faceBookToken: credential) { (success) in
                    print("success:\(success)")
                    let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
                    self.navigationController?.pushViewController(home, animated: true)
                }
                
                //                let resultDic = result as! [String : Any]
                //                print("fetched user",resultDic)
                //                let email = resultDic["email"] as? String ?? ""
                //                let first_name = resultDic["first_name"] as? String ?? ""
                //                let last_name = resultDic["last_name"] as? String ?? ""
                //                let birthday = resultDic["birthday"] as! String
                //                let facebookID = resultDic["id"] as! String
                //
                //                print("birthday",birthday, birthday.toDate(withFormat: "dd/MM/yyyy"))
                //                SwiftLoader.hide()
                //                User.signup(firstName: first_name, lastName: last_name, gender: "", dob: birthday.toDate(withFormat: "dd/MM/yyyy").timeIntervalSince1970, email: email, pass: facebookID) { (success) in
                //                    if success {
                //                        let profile = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoVC")
                //                        self.navigationController?.pushViewController(profile!, animated: true)
                //                    }
                //                }
            }
        })
    }
    
    func isValid() -> Bool {
        if txtEmail.text?.trim() == "" {
               self.showOkAlertWithHandler("Please enter your email.") {
                   self.txtEmail.becomeFirstResponder()
               }
               return false
        }
        if !SGHelper.isValidEmail(txtEmail.text?.trim() ?? "") {
               self.showOkAlertWithHandler("Please enter your valid email.") {
                   self.txtEmail.becomeFirstResponder()
               }
               return false
        }
        if txtPassword.text?.trim() == "" {
               self.showOkAlertWithHandler("Please enter your password.") {
                   self.txtPassword.becomeFirstResponder()
               }
               return false
        }
        return true
    }
}
