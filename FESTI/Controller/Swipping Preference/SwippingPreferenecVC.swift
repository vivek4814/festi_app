//
//  SwippingPreferenecVC.swift
//  FESTI
//
//  Created by Salman on 27/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit
import CoreLocation

class SwippingPreferenecVC: UIViewController {
    
    @IBOutlet weak var _tableView: UITableView!
    var selectdArray = [String]()
    var numberOfRow : Int = 1
    var boolAdd : Bool = false
    var curentLocation = ""
    
    private let locationManager = CLLocationManager()
    
    var arrMusic = [FavouriteMusicModel]()
    var arrArtist = [FavouriteMusicModel]()
    var selectedArrMusic = [FavouriteMusicModel]()
    var selectedArrArtist = [FavouriteMusicModel]()
    var arrMusicData = [FavouriteMusicModel]()
    var arrArtistData = [FavouriteArtistModel]()
    var gender = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print("globalUser?.genderPreference:-\(globalUser?.favMusicId) :::\(globalUser?.favArtistId)")
        
        if globalUser?.genderPreference == "male" {
            gender = 0
        }else if globalUser?.genderPreference == "female" {
            gender = 1
        }else {
            gender = 2

        }
        _tableView.separatorStyle = .singleLine
        _tableView.backgroundColor = UIColor.clear
        arrMusicData = FavouriteMusicModel.getDummyData()
        arrArtistData = FavouriteArtistModel.getDummyData()

        self.mapSelectedModel()
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = CLLocationDistance(exactly: 100)!
            locationManager.startUpdatingLocation()
        }
        
    }
    
   private func mapSelectedModel() -> Void {
        globalUser?.favMusicId .forEach({ (item) in
            for itemObj in self.arrMusicData {
                if itemObj.id == item {
                    self.arrMusic.append(itemObj)
                }
            }
        })
        
        globalUser?.favArtistId .forEach({ (item) in
            for itemObj in self.arrArtistData {
                if itemObj.id == item {
                    let model = FavouriteMusicModel(id: itemObj.id, name: itemObj.userName, isSelected: false)
                    self.arrArtist.append(model)
                }
            }
        })
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.reloadData()
        
    }
    
    func removeDuplicateElements(post: [FavouriteMusicModel]) -> [FavouriteMusicModel] {
           var uniquePosts = [FavouriteMusicModel]()
           for post in post {
               if !uniquePosts.contains(where: {$0.id == post.id }) {
                   uniquePosts.append(post)
               }
           }
           return uniquePosts
       }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    self.curentLocation = addressString
                    
                    self._tableView.reloadData()
                    print("self.curentLocation :\(self.curentLocation )")
                }
        })
        
    }
    
    @IBAction func tapOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveBtnAction(_ sender: Any) {
        if selectedArrMusic.count > 0 {
            var ids = [String]()
            for fav in selectedArrMusic {
                    ids.append(fav.id)
               
            }
            
            User.updateUser(data: ["favourites_music_ids": ids])

        }
        
        if selectedArrArtist.count > 0 {
            var ids = [String]()
            for fav in selectedArrArtist {
                ids.append(fav.id)
    
            }
            
            User.updateUser(data: ["favourites_artist_ids": ids])
        }
        
        if gender == 0  {
            User.updateUser(data: ["gender_preference": "male"])

        }else if gender == 1 {
            User.updateUser(data: ["gender_preference": "female"])

        }else {
            User.updateUser(data: ["gender_preference": "both"])

        }
        
        self.showOkAlertWithHandler("Save Successfully") {
            self.navigationController?.popViewController(animated: true)
        }
    }
    func moveToArtistView() -> Void {
        let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "ArtistPreferenceVC") as! ArtistPreferenceVC
        ctrl.classPrefix = "SwippingPre"
        ctrl.artistDelegate = self
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
    
    func moveToMusicView() -> Void {
        let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "MusicPreferenceVC") as! MusicPreferenceVC
        ctrl.musicDelegate = self
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
}

extension SwippingPreferenecVC : UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat((indexPath.row == 0) ? 80 : (indexPath.row == 1 ) ? 120 : (indexPath.row == 2) ? 140 : (numberOfRow == 0) ? 115 : 100 + (numberOfRow * 30))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            break
            
        case 1:
            break
            
        case 3:
            self.moveToArtistView()
            break
            
        case 4:
            self.moveToMusicView()
            break
        default:
            break
        }
    }
}

extension SwippingPreferenecVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.row  {
        case 0:
            let cell : GenderPreferenceCell = tableView.dequeueReusableCell(withIdentifier: "GenderPreferenceCell", for: indexPath) as! GenderPreferenceCell
            cell.delegate = self
            if gender == 0 {
                cell.btnMale.setImage(UIImage.init(named: "checkboxSelected_icon"), for: .normal)
                cell.btnFemale.setImage(UIImage.init(named: "chechbox_icon"), for: .normal)
                cell.btnBoth.setImage(UIImage.init(named: "chechbox_icon"), for: .normal)
                
            }else if gender == 1 {
                
                cell.btnFemale.setImage(UIImage.init(named: "checkboxSelected_icon"), for: .normal)
                cell.btnMale.setImage(UIImage.init(named: "chechbox_icon"), for: .normal)
                cell.btnBoth.setImage(UIImage.init(named: "chechbox_icon"), for: .normal)
            }else {
                cell.btnBoth.setImage(UIImage.init(named: "checkboxSelected_icon"), for: .normal)
                cell.btnMale.setImage(UIImage.init(named: "chechbox_icon"), for: .normal)
                cell.btnFemale.setImage(UIImage.init(named: "chechbox_icon"), for: .normal)
            }
            
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            return cell
            
        case 1:
            let cell : LocationPrefrenceCell = tableView.dequeueReusableCell(withIdentifier: "LocationPrefrenceCell", for: indexPath) as! LocationPrefrenceCell
            cell._titleLbl.text = self.curentLocation
            cell._headLbl.text = "Location"
            
            cell._catImageView.image = #imageLiteral(resourceName: "location_icon")
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            return cell
            
            
        case 2:
            let cell : DistancePrefrenceCell = tableView.dequeueReusableCell(withIdentifier: "DistancePrefrenceCell", for: indexPath) as! DistancePrefrenceCell
            cell.selectionStyle = .none
            cell.distanceDelegate = self
            cell.backgroundColor = UIColor.clear
            return cell
            
        case 3:
            let cell : ArtistPrefrenceCell = tableView.dequeueReusableCell(withIdentifier: "ArtistPrefrenceCell", for: indexPath) as! ArtistPrefrenceCell
            
            cell.arrData = self.arrArtist
            if self.arrArtist.count > 0 {
                self.selectedArrArtist = self.arrArtist
            }
            cell.addTag(_selectedArray: self.arrArtist)
            self.arrArtist.removeAll()
            cell.lblHead.text = "Favorite artist"
            cell._tagView.tag = 100
            cell.imageRight.image = UIImage.init(named: "artist_icon")
            cell.artistPreDelegate = self
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            return cell
            
        case 4:
            
            let cell : ArtistPrefrenceCell = tableView.dequeueReusableCell(withIdentifier: "ArtistPrefrenceCell", for: indexPath) as! ArtistPrefrenceCell
            cell.arrData = self.arrMusic
            if self.arrMusic.count > 0 {
                self.selectedArrMusic = self.arrMusic
                
            }

            cell.addTag(_selectedArray: self.arrMusic)
            self.arrMusic.removeAll()

            cell.lblHead.text = "Favorite Music"
            cell.imageRight.image = UIImage.init(named: "artist_icon")
            cell.artistPreDelegate = self
            cell._tagView.tag = 200
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            return cell
            
        default:
            break
        }
        
        let cell : LocationPrefrenceCell = tableView.dequeueReusableCell(withIdentifier: "LocationPrefrenceCell", for: indexPath) as! LocationPrefrenceCell
        return cell
    }
}

extension SwippingPreferenecVC : FavouriteArtistDelegate {
    func selectedArtist(idArray: inout [FavouriteArtistModel]) {
        globalUser?.favArtistId .forEach({ (item) in
            for itemObj in idArray {
                if itemObj.id == item {
                    let index = idArray.firstIndex(of: itemObj)
                    idArray.remove(at: index ?? 0)
                }
            }
        })
        
        
        for item in idArray {
          let musicModel = FavouriteMusicModel(id: item.id, name: item.userName, isSelected: false)
            self.arrArtist.append(musicModel)
        }
        
      self.arrArtist =  self.removeDuplicateElements(post: self.arrArtist)

        boolAdd = true
        _tableView.reloadData()

    }

}

extension SwippingPreferenecVC : ArtistPrefrenceDelegate {
    func calulateHeightDynamiclly(_row: Int) {
        numberOfRow = _row
        if boolAdd {
            _tableView.reloadData()
            boolAdd = false
        }

    }
    
    
}

extension SwippingPreferenecVC : FavouriteMusicDelegate {
    func selectedMusic( idArray: inout[FavouriteMusicModel]) {
        globalUser?.favMusicId .forEach({ (item) in
            for itemObj in idArray {
                if itemObj.id == item {
                    let index = idArray.firstIndex(of: itemObj)
                    idArray.remove(at: index ?? 0)
                }
            }
        })
        self.arrMusic = idArray
        self.arrMusic =  self.removeDuplicateElements(post: self.arrMusic)

        boolAdd = true
        _tableView.reloadData()
        
    }
}

extension SwippingPreferenecVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
       
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    
        print("locationString:-\(self.getAddressFromLatLon(pdblLatitude: "\(locValue.latitude)", withLongitude: "\(locValue.longitude)"))")
     
    }
}

extension SwippingPreferenecVC : genderPreferenceDelegate {
    func getActionOnMale() {
        gender = 0
        _tableView.reloadData()
    }
    
    func getActionOnFemale() {
        gender = 1
        _tableView.reloadData()
    }
    
    func getActionOnBoth() {
        gender = 2
        _tableView.reloadData()
    }
    
    
}


extension SwippingPreferenecVC : DistanceUpdateDelgate {
    func getUpdatedValue(selectdIndex: Int) {
        print("selectdIndex:-\(selectdIndex)")
    }
    
    
}
