//
//  ArtistPrefrenceCell.swift
//  FESTI
//
//  Created by Salman on 27/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

protocol ArtistPrefrenceDelegate {
    func calulateHeightDynamiclly(_row :Int)
}

class ArtistPrefrenceCell: UITableViewCell {

    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var imageRight: UIImageView!
    @IBOutlet weak var _tagView: TagListView!
    var selectedArray = [String]()
    var artistPreDelegate : ArtistPrefrenceDelegate?
    
    var arrData = [FavouriteMusicModel]()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
 
        _tagView.alignment = .left
        _tagView.delegate = self
        _tagView.textFont = UIFont.init(name: "Montserrat-Regular", size: 13.0) ?? UIFont.systemFont(ofSize: 13)

    }
    
    func addTag(_selectedArray : [FavouriteMusicModel]) -> Void {
        for item in _selectedArray {
            print("addTagaddTag.name:-\(item.name)")

            _tagView.addTag(item.name)
        }
        print("_tagView:-\(_tagView.tagViewHeight):::\(_tagView.rows)")
        artistPreDelegate?.calulateHeightDynamiclly(_row: _tagView.rows)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ArtistPrefrenceCell: TagListViewDelegate {
    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
}
