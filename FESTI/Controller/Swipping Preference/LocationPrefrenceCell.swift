//
//  LocationPrefrenceCell.swift
//  FESTI
//
//  Created by Salman on 27/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

class LocationPrefrenceCell: UITableViewCell {
    @IBOutlet weak var _catImageView: UIImageView!
    
    @IBOutlet weak var _headLbl: UILabel!
    @IBOutlet weak var _titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
