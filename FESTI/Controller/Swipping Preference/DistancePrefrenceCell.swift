//
//  DistancePrefrenceCell.swift
//  FESTI
//
//  Created by Salman on 27/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit
import StepSlider

protocol DistanceUpdateDelgate {
    func getUpdatedValue(selectdIndex : Int)
}

class DistancePrefrenceCell: UITableViewCell {

    @IBOutlet weak var _stepSlider: StepSlider!
    var distanceDelegate : DistanceUpdateDelgate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func valueChanged(_ sender: StepSlider) {
        print("::::-\(sender.index)")
        distanceDelegate?.getUpdatedValue(selectdIndex: Int(sender.index))
    }
}
