//
//  ArtistPreferenceCollectionCell.swift
//  FESTI
//
//  Created by Salman on 10/06/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

class ArtistPreferenceCollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblArtistname: UILabel!
    @IBOutlet weak var artistsImageView: UIImageView!
    
    var info :  FavouriteArtistModel? {
        didSet {
            self.lblArtistname.text = info?.userName
            self.artistsImageView.image = info?.image
        }
    }
    
    
}
