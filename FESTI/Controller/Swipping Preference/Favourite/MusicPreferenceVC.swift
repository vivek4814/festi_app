//
//  MusicPreferenceVC.swift
//  FESTI
//
//  Created by Salman on 10/06/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

protocol FavouriteMusicDelegate {
    func selectedMusic( idArray : inout [FavouriteMusicModel])
}

class MusicPreferenceVC: UIViewController {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tagListView: TagListView!
    var arrFav = [FavouriteMusicModel]()
    var musicDelegate  : FavouriteMusicDelegate?
    
    var arrCollection = [FavouriteMusicModel]()
    
    var filterArrFav = [FavouriteMusicModel]()
    var isFiltered : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
      
        collectionView.delegate = self
            collectionView.dataSource = self
        tblView.separatorStyle = .none
        arrFav = FavouriteMusicModel.getDummyData()
        self.mapSelectedModel()
        txtSearch.delegate = self
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search music...",
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    
    }
    
   private func mapSelectedModel() -> Void {
        globalUser?.favMusicId .forEach({ (item) in
            
            for itemObj in self.arrFav {
                if itemObj.id == item {
                    itemObj.isSelected = true
                    self.arrCollection.append(itemObj)
                }
            }
        })
       
        tblView.reloadData()
        collectionView.reloadData()
    }
    
    func applyFilter(_text : String) -> Void {
        self.filterArrFav.removeAll()
        self.filterArrFav  = arrFav.filter{(x) -> Bool in
            (x.name.lowercased().range(of: _text.lowercased()) != nil)
        }
        if self.filterArrFav.count > 0 {
            isFiltered = true
            print("applyFilter:-\(self.filterArrFav.count)")

        }else {
            isFiltered = false

        }
        tblView.reloadData()
    }
    
    @IBAction func tapBack(_ sender: Any) {
        var ids = [FavouriteMusicModel]()
        for fav in arrFav {
            if fav.isSelected {
             
                ids.append(fav)
            }
        }
        self.musicDelegate?.selectedMusic(idArray: &ids)
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func tapDone(_ sender: Any) {
        var ids = [FavouriteMusicModel]()
        for fav in arrFav {
            if fav.isSelected {
             
                ids.append(fav)
            }
        }
        self.musicDelegate?.selectedMusic(idArray: &ids)
       
    }
}

extension MusicPreferenceVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (isFiltered) ? self.filterArrFav.count :  self.arrFav.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFavMusic", for: indexPath) as! CellFavMusic
        cell.info = (isFiltered) ? filterArrFav[indexPath.row] : arrFav[indexPath.row]
        cell.cellMusicDelegate = self
        return cell
    }
}




extension MusicPreferenceVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MusicPreferenceCollectionCell", for: indexPath) as! MusicPreferenceCollectionCell
        cell.lblMusic.text = self.arrCollection[indexPath.row].name
        cell.viewBackground.layer.cornerRadius = 10
        cell.btnCross.layer.cornerRadius = 6.5
        cell.btnCross.layer.masksToBounds = true
        cell.btnCross.tag = indexPath.row
        cell.musicDelegate = self
        
        let fontAttributes = [NSAttributedString.Key.font: UIFont.init(name: "Montserrat-Regular", size: 13.0) ?? UIFont.systemFont(ofSize: 13)]
               let myText = self.arrCollection[indexPath.row].name
               let size = (myText as NSString).size(withAttributes: fontAttributes)
        cell._viewBackgroudWidthConstant.constant = (size.width + 50.0)
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: UIFont.init(name: "Montserrat-Regular", size: 13.0) ?? UIFont.systemFont(ofSize: 13)]
        let myText = self.arrCollection[indexPath.row].name
        let size = (myText as NSString).size(withAttributes: fontAttributes)
        print("sizesize:-\(size.width)")
        
        return CGSize(width: (size.width + 50.0), height: 35)
    }
}


extension MusicPreferenceVC : cellFavMusicDelegate {
    func getSelectedModel(model: FavouriteMusicModel?) {
        if self.arrCollection.contains(model!) {
            let index = self.arrCollection.firstIndex(of: model!)
            self.arrCollection.remove(at: index ?? 0)
            
        }else {
            self.arrCollection.append(model!)
        }
      
        collectionView.reloadData()
    }
    
    
}

extension MusicPreferenceVC : MusicPreferenceCollectionDelegate {
    func tapOnCross(tag: Int) {
         
        for itemObj in self.arrFav {
            if itemObj.id == self.arrCollection[tag].id {
                itemObj.isSelected = false
            }
        }
        
        
        self.arrCollection.remove(at: tag)
        tblView.reloadData()
        collectionView.reloadData()
    }
    
    
}


extension MusicPreferenceVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            self.applyFilter(_text: updatedText)
            
            debugPrint("updatedText:-\(updatedText)")
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
