//
//  MusicPreferenceCollectionCell.swift
//  FESTI
//
//  Created by Salman on 11/06/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

protocol MusicPreferenceCollectionDelegate {
    func tapOnCross(tag:Int)
}

class MusicPreferenceCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var _viewBackgroudWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var lblMusic: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    var musicDelegate : MusicPreferenceCollectionDelegate?
    
    @IBAction func tapOnCross(_ sender: UIButton) {
        self.musicDelegate?.tapOnCross(tag: sender.tag)
        
    }
    
}
