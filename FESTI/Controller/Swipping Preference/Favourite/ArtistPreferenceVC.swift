//
//  ArtistPreferenceVC.swift
//  FESTI
//
//  Created by Salman on 10/06/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

protocol FavouriteArtistDelegate {
    func selectedArtist(idArray : inout [FavouriteArtistModel])
}

class ArtistPreferenceVC: UIViewController {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnAdd: UIButton!
    var artistDelegate : FavouriteArtistDelegate?
    
    @IBOutlet weak var tblView: UITableView!
    var arrFav = [FavouriteArtistModel]()
    var filterArrFav = [FavouriteArtistModel]()
    var isFiltered : Bool = false
    
    var arrCollection = [FavouriteArtistModel]()

    var classPrefix : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrFav = FavouriteArtistModel.getDummyData()
        collectionView.delegate = self
        collectionView.dataSource = self
        self.mapSelectedModel()
        txtSearch.delegate = self
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search Artists...",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        // Do any additional setup after loading the view.
    }
    
    func mapSelectedModel() -> Void {
        globalUser?.favArtistId .forEach({ (item) in
            
            for itemObj in self.arrFav {
                if itemObj.id == item {
                    itemObj.isSelected = true
                    self.arrCollection.append(itemObj)
                }
            }
        })
       
        tblView.reloadData()
        collectionView.reloadData()
    }
    
    func applyFilter(_text : String) -> Void {
        self.filterArrFav.removeAll()
        self.filterArrFav  = arrFav.filter{(x) -> Bool in
            (x.userName.lowercased().range(of: _text.lowercased()) != nil)
        }
        if self.filterArrFav.count > 0 {
            isFiltered = true
            print("applyFilter:-\(self.filterArrFav.count)")

        }else {
            isFiltered = false

        }
        tblView.reloadData()
    }
    
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapDone(_ sender: Any) {
        
        //favourites_artist_ids
        var ids = [FavouriteArtistModel]()
        
        if isFiltered {
            for fav in filterArrFav {
                if fav.isSelected {
                    ids.append(fav)
                }
                
            }
        }else {
            for fav in arrFav {
                if fav.isSelected {
                    ids.append(fav)
                }
                
            }
        }
        
        
        
        artistDelegate?.selectedArtist(idArray: &ids)
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension ArtistPreferenceVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (isFiltered) ? self.filterArrFav.count : arrFav.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFavArtists", for: indexPath) as! CellFavArtists
        cell.info = (isFiltered) ? filterArrFav[indexPath.row] : arrFav[indexPath.row]
        cell.cellArtistsDelegate = self
        return cell
    }
}


extension ArtistPreferenceVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtistPreferenceCollectionCell", for: indexPath) as! ArtistPreferenceCollectionCell
        cell.artistsImageView.layer.cornerRadius = 25
        cell.artistsImageView.layer.masksToBounds = true
        cell.info = self.arrCollection[indexPath.row]
        cell.backgroundColor = UIColor.clear
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        for itemObj in self.arrFav {
            if itemObj.id == self.arrCollection[indexPath.row].id {
                itemObj.isSelected = false
            }
        }
        self.arrCollection.remove(at: indexPath.row)
        tblView.reloadData()
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 75, height: 75)
    }
}

extension ArtistPreferenceVC : cellFavArtistsDelegate {
    func getSelectedModel(model: FavouriteArtistModel?) {
         if self.arrCollection.contains(model!) {
              let index = self.arrCollection.firstIndex(of: model!)
              self.arrCollection.remove(at: index ?? 0)
              
          }else {
              self.arrCollection.append(model!)
          }
        
          collectionView.reloadData()
    }
    
    
}


extension ArtistPreferenceVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            self.applyFilter(_text: updatedText)
            
            debugPrint("updatedText:-\(updatedText)")
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
