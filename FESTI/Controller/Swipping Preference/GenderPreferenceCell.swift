//
//  GenderPreferenceCell.swift
//  FESTI
//
//  Created by Salman on 27/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

protocol genderPreferenceDelegate {
    func getActionOnMale()
    func getActionOnFemale()
    func getActionOnBoth()

}

class GenderPreferenceCell: UITableViewCell {

    @IBOutlet weak var btnBoth: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    
    var delegate : genderPreferenceDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func tapOnBoth(_ sender: Any) {
        self.delegate?.getActionOnBoth()
    }
    @IBAction func tapOnFemale(_ sender: Any) {
        self.delegate?.getActionOnFemale()

    }
    @IBAction func tapOnMale(_ sender: UIButton) {
        self.delegate?.getActionOnMale()

    }
}
