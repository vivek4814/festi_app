//
//  ResetPasswordVC.swift
//  FESTI
//
//  Created by salman on 06/11/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {

    @IBOutlet weak var btnSendPass: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSendPass.applyGradient(colors: AppColor.kPINK_COLOR_GRADIENT)
    }
    

    @IBAction func tapSendPass(_ sender: Any) {
        if txtEmail.text?.trim() == "" {
               self.showOkAlertWithHandler("Please enter your email.") {
                   self.txtEmail.becomeFirstResponder()
               }
               return
        }
        if !SGHelper.isValidEmail(txtEmail.text?.trim() ?? "") {
               self.showOkAlertWithHandler("Please enter your valid email.") {
                   self.txtEmail.becomeFirstResponder()
               }
               return
        }
        
        User.resetPass(email: txtEmail.text!) { (success) in
            self.showOkAlertWithHandler("Reset password link sent to your email id: '\(self.txtEmail.text!)'") {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
