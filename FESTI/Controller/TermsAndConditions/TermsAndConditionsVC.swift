//
//  TermsAndConditionsVC.swift
//  FESTI
//
//  Created by ashish kumar on 14/04/21.
//  Copyright © 2021 Salman. All rights reserved.
//

import UIKit

var headText : String?



class TermsAndConditionsVC: UIViewController {
    
    @IBOutlet weak var _crossImageView: UIImageView!
    @IBOutlet weak var lblTitel: UILabel!
    @IBOutlet weak var txtPrivacy: UITextView!
    
    var headText : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitel.text = headText
        txtPrivacy.isEditable = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
              _crossImageView.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer?
        = nil) {
        self.dismiss(animated: true, completion: nil)

    }

}
