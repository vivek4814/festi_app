//
//  HomeScreenVC.swift
//  FESTI
//
//  Created by salman on 04/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import Koloda
import CoreLocation
import Firebase

var curentLat = 0.0
var curentLong = 0.0


class HomeScreenVC: UIViewController , ButtonDelegate{
    
    func didPressButton(button: UIButton) {
        viewPopUp.isHidden = false
    }
    
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var viewSwipe: KolodaView!
    let locationManager = CLLocationManager()
    var myUsers = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPopUp.isHidden = true
        viewSwipe.dataSource = self
        viewSwipe.delegate = self
        viewSwipe.makeRoundCorner(10)
        
        btnCancel.layer.cornerRadius = 12
        btnCancel.clipsToBounds = true
        btnCancel.layer.masksToBounds = true
        
        btnReport.layer.cornerRadius = 12
        btnReport.clipsToBounds = true
        btnReport.layer.masksToBounds = true
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = CLLocationDistance(exactly: 100)!
            locationManager.startUpdatingLocation()
        }
        User.getUser { (user) in
            globalUser = user
        }
        HomeModel.getHomeData { (users) in
            if users != nil {
                self.myUsers = users!
            }
            self.viewSwipe.reloadData()
        }
        
    }
    
    @IBAction func tapSideMenu(_ sender: Any) {
        let menuVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuListVC") as! MenuListVC
        let navigationCtroller = UINavigationController(rootViewController: menuVC)
        navigationCtroller.setNavigationBarHidden(true, animated: false)
        
        navigationCtroller.modalPresentationStyle = .overCurrentContext
        self.present(navigationCtroller, animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func openViewControllerBasedOnStoryIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        self.navigationController!.pushViewController(destViewController, animated: true)
        
    }
    
    @IBAction func tapReport(_ sender: UIButton) {
        viewPopUp.isHidden = true
        
        self.showOkAlert("Thank you for flagging this content.")
    }
    
    
    @IBAction func tapCancel(_ sender: UIButton) {
        
        viewPopUp.isHidden = true
    }
    
    @IBAction func tapLogo(_ sender: Any) {
        
        self.openViewControllerBasedOnStoryIdentifier("ChatUserListVC")
        
        
        //        SwiftLoader.show(title: "Loading...", animated: true)
        //
        //        let config = ChatUIConfiguration()
        //        config.configureUI()
        //        let remoteData = ATCRemoteData()
        //        // Checks if user's firestore actually has channels setup
        //        remoteData.getChannels(handler: { (users) in
        //
        //        })
        //        { (theads) in
        //            print("thereds count", theads.count)
        //            SwiftLoader.hide()
        //            let threadsDataSource = ATCGenericLocalHeteroDataSource(items: theads)
        //            let Ctrl  = ATCChatHomeViewController.homeVC(uiConfig: config, threadsDataSource: threadsDataSource, viewer: ATCChatMockStore.users[3])
        //            let backItem = UIBarButtonItem()
        //            backItem.title = ""
        //            print("ATCChatMockStore.users",ATCChatMockStore.users.count)
        //            self.navigationItem.backBarButtonItem = backItem
        //            self.navigationController?.isNavigationBarHidden = false
        //            self.navigationController?.pushViewController(Ctrl, animated: true)
        //        }
        
        
        
    }
}

extension HomeScreenVC: KolodaViewDelegate {
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        // fetch data
        koloda.reloadData()
    }
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        let photo = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetailVC") as! ProfileDetailVC
        photo.selectedUser = self.myUsers[index]
        self.navigationController?.pushViewController(photo, animated: true)
    }
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        if direction == .right {
            print("Swipe")
            RequestModel.makeRequest(toId: myUsers[index].uid) { (success) in
                if success {
                    self.showOkAlert("Chat request sent successfully")
                }
                print(success)
            }
        }
    }
}

extension HomeScreenVC: KolodaViewDataSource {
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        return self.myUsers.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let view = ViewSwipe.fromNib(xibName: "ViewSwipe") as! ViewSwipe
        view.makeRoundCorner(10)
        view.Newdelegate = self
        view.applyGradient(colors: AppColor.kBLUE_COLOR_GRADIENT, orientation: .topLeftBottomRight)
        view.configureView(user: myUsers[index])
        return view
    }
    
}

extension HomeScreenVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        
        let info = ["lat":"\(locValue.latitude)", "long":"\(locValue.longitude)"]
        User.updateUser(data: info)
        curentLat = locValue.latitude
        curentLong = locValue.longitude
        viewSwipe.reloadData()
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
}
