//
//  ViewSwipe.swift
//  FESTI
//
//  Created by salman on 07/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import UIKit
import FirebaseStorage

protocol ButtonDelegate {
    
    func didPressButton(button:UIButton)
    
}

//delegate.didPressButton(button)

class ViewSwipe: UIView {
    
    var Newdelegate:ButtonDelegate?
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblArtists: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    var arrImages = [String]()
    
    private var arrFavArtist = [FavouriteArtistModel]()
    
    var newFAvArr = [String]()
    
    func configureView(user: User) {
        Storage.storage().reference().child(user.profileImage).downloadURL { (url, error) in
            if error != nil {
                print(error.debugDescription)
                return
            }
            self.imgViewUser.sd_setImage(with: url, placeholderImage: UIImage(named: "profile"))
        }
        arrFavArtist = FavouriteArtistModel.getDummyData()
        self.lblName.text = "\(user.firstName) \(user.lastName)"
        //        print(user.dob)
        //        print(user.dob?.age)
      //  self.lblAge.text = "\(user.dob?.age ?? 0) year"
        self.lblAge.text = "\(user.dob?.age ?? 0)"
        var favArts = ""
        for item in user.favMusicId {
            for itemObj in self.arrFavArtist {
                if itemObj.id == item {
                    if arrFavArtist.count == 1 {
                        favArts = favArts+"\(itemObj.userName)"
                    }else{
                        favArts = favArts+"\(itemObj.userName),"
                    }
                    newFAvArr.append(favArts)
                    print(newFAvArr)
                }
                if newFAvArr.count == 1 {
                    self.lblArtists.text = favArts.replacingOccurrences(of: #","#,
                                                                        with: "",
                                                                        options: .regularExpression,
                                                                        range: nil)
                }else{
                    self.lblArtists.text = favArts
                }
            }
        }
        print("curentLat:-\(curentLat) :::-\(curentLong)")
        print("user:-\(user.lat) :::-\(user.long)")
        
        let coordinate₀ = CLLocation(latitude: curentLat, longitude: curentLong)
        let coordinate₁ = CLLocation(latitude: ((user.lat) as NSString).doubleValue, longitude: ((user.long) as NSString).doubleValue)
        
        let distanceInMeters = coordinate₀.distance(from: coordinate₁)
        print("distanceInMeters:-\(distanceInMeters)")
        if(distanceInMeters <= 1609){
            lblDistance.text = "\(distanceInMeters) meters"
        }
        else if(distanceInMeters >= 1609) {
            let miles = distanceInMeters / 1609
            print("miles:-\(miles)")
            lblDistance.text = "\(Int(distanceInMeters)/1609) miles"
        }
        var favGenre = ""
        for item in user.favMusicId {
            //            for itemObj in self.arrFavMusic {
            //                if itemObj.id == item {
            //                    favGenre = favGenre+" \(itemObj.name),"
            //
            //                }
            //            }
        }
        //self.lblFavGenres.text = favGenre
        //self.lblDistance.text = ""
        //self.lblBio.text = user.bio
        print(user.lastName)
        self.arrImages.append(user.galleryImage0)
        self.arrImages.append(user.galleryImage1)
        self.arrImages.append(user.galleryImage2)
        self.arrImages.append(user.galleryImage3)
        self.arrImages.append(user.galleryImage4)
        self.arrImages = self.arrImages.filter({ (url) -> Bool in
            return url != ""
        })
    }
    
    @IBAction func menuTap(_ sender: UIButton) {
        Newdelegate?.didPressButton(button: btnMenu)
    }
}
