//
//  MyProfileDetailVC.swift
//  FESTI
//
//  Created by salman on 07/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseStorage
import Lightbox


class MyProfileDetailVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!

    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblFavArtists: UILabel!
    @IBOutlet weak var lblFavGenres: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblBio: UILabel!
    var arrFavMusic = [FavouriteMusicModel]()
    var arrFavArtist = [FavouriteArtistModel]()

    var arrImages = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        arrFavMusic = FavouriteMusicModel.getDummyData()
        arrFavArtist = FavouriteArtistModel.getDummyData()

        btnEdit.applyGradient(colors: AppColor.kPINK_COLOR_GRADIENT)
        btnConfirm.applyGradient(colors: AppColor.kBLUE_COLOR_GRADIENT)
        User.getUser { (user) in
            Storage.storage().reference().child(user.profileImage).downloadURL { (url, error) in
                if error != nil {
                    print(error.debugDescription)
                    return
                }
                self.imgViewUser.sd_setImage(with: url, placeholderImage: UIImage(named: "profile"))
            }
            
            self.lblName.text = "\(user.firstName) \(user.lastName)"
            self.lblAge.text = "\(user.dob?.age ?? 0)"
            var favArts = ""
            for item in user.favArtistId {
                for itemObj in self.arrFavArtist {
                    if itemObj.id == item {
                        favArts = favArts+"\(itemObj.userName),"

                    }
                }

            }
            
            self.lblFavArtists.text = favArts
            
            var favGenre = ""
            for item in user.favMusicId {
                for itemObj in self.arrFavMusic {
                    if itemObj.id == item {
                        favGenre = favGenre+"\(itemObj.name),"
                        
                    }
                }
            }
            self.lblFavGenres.text = favGenre
            //self.lblDistance.text = ""
            self.lblBio.text = user.bio
            print(user.lastName)
            self.arrImages.append(user.galleryImage0)
            self.arrImages.append(user.galleryImage1)
            self.arrImages.append(user.galleryImage2)
            self.arrImages.append(user.galleryImage3)
            self.arrImages.append(user.galleryImage4)
            self.arrImages = self.arrImages.filter({ (url) -> Bool in
                return url != ""
            })
            self.collectionView.reloadData()
        }
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapConfirm(_ sender: Any) {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    @IBAction func tapEdit(_ sender: Any) {
        let myProfile = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileDetailVC")
        self.navigationController?.pushViewController(myProfile!, animated: true)
    }
    

}
extension MyProfileDetailVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellImage", for: indexPath) as! CellImage
        cell.url = self.arrImages[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.height, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var arrImage = [LightboxImage]()
        // let imageUrl = URL.init(string: self.arrImages[indexPath.row])
        Storage.storage().reference().child(self.arrImages[indexPath.row] ).downloadURL { (url, error) in
            if error != nil {
                print(error.debugDescription)
                return
            }
            arrImage.append(LightboxImage(imageURL: url!))
            let controller = LightboxController(images: arrImage, startIndex: indexPath.row)
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
            
        }
        
        //        for number in 1...5 {
        //            arrImage.append(LightboxImage(image: UIImage(named: "\((number)*100)")!))
        //        }
        //        let controller = LightboxController(images: arrImage, startIndex: indexPath.row)
        //        controller.modalPresentationStyle = .fullScreen
        //        self.present(controller, animated: true, completion: nil)
    }
}
