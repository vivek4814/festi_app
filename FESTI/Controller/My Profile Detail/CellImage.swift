//
//  CellImage.swift
//  FESTI
//
//  Created by salman on 05/11/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseStorage

class CellImage: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    var url: String? {
        didSet {
            Storage.storage().reference().child(url ?? "").downloadURL { (url, error) in
                if error != nil {
                    print(error.debugDescription)
                    return
                }
                self.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "profile"))
            }
        }
    }
}
