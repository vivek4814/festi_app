//
//  CellFavArtists.swift
//  FESTI
//
//  Created by salman on 21/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
protocol cellFavArtistsDelegate {
    
    func getSelectedModel(model : FavouriteArtistModel?)
}

class CellFavArtists: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnFav: UIButton!
    var selectedModel : FavouriteArtistModel?
    
    var cellArtistsDelegate : cellFavArtistsDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var info: FavouriteArtistModel? {
        didSet {
            self.selectedModel = info
            imgView.image = info?.image
            imgView.makeRounded()
            lblName.text = info?.userName
            btnFav.isSelected = info?.isSelected ?? false
        }
    }
    
    @IBAction func tapHeart(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        info?.isSelected = sender.isSelected
        self.cellArtistsDelegate?.getSelectedModel(model: self.selectedModel)
    }

}
