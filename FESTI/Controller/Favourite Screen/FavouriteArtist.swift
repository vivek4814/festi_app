//
//  FavouriteVC.swift
//  FESTI
//
//  Created by salman on 01/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit



class FavouriteArtist: UIViewController {

    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var tblView: UITableView!
    var arrFav = [FavouriteArtistModel]()
    var classPrefix : String?
    
    var filterArrFav = [FavouriteArtistModel]()
    var isFiltered : Bool = false
    
    @IBOutlet weak var txtSearch: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrFav = FavouriteArtistModel.getDummyData()
        txtSearch.delegate = self
               txtSearch.attributedPlaceholder = NSAttributedString(string: "Search Artists...",
               attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        if self.classPrefix == "SwippingPre" {
            btnAdd.setTitle("Add", for: .normal)
            btnAdd.setImage(nil, for: .normal)

        }
    }
    
    func applyFilter(_text : String) -> Void {
        self.filterArrFav.removeAll()
        self.filterArrFav  = arrFav.filter{(x) -> Bool in
            (x.userName.lowercased().range(of: _text.lowercased()) != nil)
        }
        if self.filterArrFav.count > 0 {
            isFiltered = true
            print("applyFilter:-\(self.filterArrFav.count)")

        }else {
            isFiltered = false

        }
        tblView.reloadData()
    }

    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapDone(_ sender: Any) {
        
        //favourites_artist_ids
        
        var ids = [String]()
        if isFiltered {
            for fav in filterArrFav {
                if fav.isSelected {
                    ids.append(fav.id)
                }
                
            }
        }else {
            for fav in arrFav {
                if fav.isSelected {
                    ids.append(fav.id)
                }
                
            }
        }
        
        User.updateUser(data: ["favourites_artist_ids": ids])
        let music = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteMusicVC") as! FavouriteMusicVC
        self.navigationController?.pushViewController(music, animated: true)
    }
   
   
    
    
}

extension FavouriteArtist: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (isFiltered) ? self.filterArrFav.count : arrFav.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFavArtists", for: indexPath) as! CellFavArtists
        cell.info = (isFiltered) ? filterArrFav[indexPath.row] : arrFav[indexPath.row]
        return cell
    }
}


extension FavouriteArtist : UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            self.applyFilter(_text: updatedText)
            
            debugPrint("updatedText:-\(updatedText)")
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
