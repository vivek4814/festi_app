//
//  CellFavMusic.swift
//  FESTI
//
//  Created by salman on 10/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

protocol cellFavMusicDelegate {
    func getSelectedModel(model :FavouriteMusicModel?)
}

class CellFavMusic: UITableViewCell {

    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    var musicModel : FavouriteMusicModel?
    
    var cellMusicDelegate : cellFavMusicDelegate?
    
    
    
    @IBAction func tapHeart(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        info?.isSelected = sender.isSelected
        self.cellMusicDelegate?.getSelectedModel(model: self.musicModel)
    }
    
    var info: FavouriteMusicModel? {
        didSet {
            self.musicModel = info
            btnFav.isSelected = info?.isSelected ?? false
            lblTitle.text = info?.name
        }
    }
    
}
