//
//  FavouriteMusicVC.swift
//  FESTI
//
//  Created by salman on 03/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit


class FavouriteMusicVC: UIViewController {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblView: UITableView!
    var arrFav = [FavouriteMusicModel]()
    
    var filterArrFav = [FavouriteMusicModel]()
    var isFiltered : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrFav = FavouriteMusicModel.getDummyData()
        txtSearch.delegate = self
              txtSearch.attributedPlaceholder = NSAttributedString(string: "Search music...",
                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapDone(_ sender: Any) {
        var ids = [String]()
        if isFiltered {
            for fav in filterArrFav {
                if fav.isSelected {
                    ids.append(fav.id)
                }
            }
        }else {
            for fav in arrFav {
                if fav.isSelected {
                    ids.append(fav.id)
                }
            }
        }
        
        User.updateUser(data: ["favourites_music_ids": ids])
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileDetailVC") as! MyProfileDetailVC
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    func applyFilter(_text : String) -> Void {
        self.filterArrFav.removeAll()
        self.filterArrFav  = arrFav.filter{(x) -> Bool in
            (x.name.lowercased().range(of: _text.lowercased()) != nil)
        }
        if self.filterArrFav.count > 0 {
            isFiltered = true
            print("applyFilter:-\(self.filterArrFav.count)")

        }else {
            isFiltered = false

        }
        tblView.reloadData()
    }
}
extension FavouriteMusicVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (isFiltered) ? self.filterArrFav.count :  self.arrFav.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFavMusic", for: indexPath) as! CellFavMusic
        cell.info = (isFiltered) ? filterArrFav[indexPath.row] : arrFav[indexPath.row]
        return cell
    }
}


extension FavouriteMusicVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            self.applyFilter(_text: updatedText)
            
            debugPrint("updatedText:-\(updatedText)")
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
