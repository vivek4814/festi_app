//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import UIKit
import Photos
import Firebase
import CoreLocation
import IQKeyboardManagerSwift
import Lightbox

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate {
    
    //MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstrainntView: NSLayoutConstraint!
    @IBOutlet weak var lblLastSeen: UILabel!
    
    @IBOutlet weak var txtViewContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    

    var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    
    let locationManager = CLLocationManager()
    var items = [Message]()
    let imagePicker = UIImagePickerController()
    let barHeight: CGFloat = 50
    var currentUser: User?
    var groupConvo: Conversation?
    var canSendLocation = true
    var isTyping = false
    var timer = Timer()
    var lastTextViewRect = CGRect.zero
   
    //MARK: ViewController lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.layoutIfNeeded()
        IQKeyboardManager.shared.enableAutoToolbar = false
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.hideKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        if groupConvo != nil {
            Message.markMessagesRead(forUserID: groupConvo?.id ?? "")
        }else {
            Message.markMessagesRead(forUserID: self.currentUser!.id)
        }
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = false
        inputTextField.delegate = self
        self.customization()
        self.fetchData()
    }
    override func viewWillLayoutSubviews() {
        print(self.view.frame)
        print("viewWillLayoutSubviews")
    }
   
    
    
    @objc func updateTypingStatus() {
        if let myId = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(myId).child("credentials").updateChildValues(["isTyping": false])
        }
    }
    
    //MARK: Methods
    func customization() {
        
        self.imagePicker.delegate = self
        self.tableView.estimatedRowHeight = self.barHeight
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.contentInset.bottom = self.barHeight
        self.tableView.scrollIndicatorInsets.bottom = self.barHeight
        if groupConvo != nil {
            lblTitle.text = groupConvo?.groupName
            let path = Storage.storage().reference().child(groupConvo?.groupImage ?? "")
            path.downloadURL(completion: { (url, nil) in
                if url == nil {
                    return
                }
                self.imgView.setImage(url: url!)
            })
        }else {
            lblTitle.text = self.currentUser?.name
            imgView.image = currentUser?.profilePic
        }
        imgView.makeRounded()
        if let lastSeen = self.currentUser?.lastSeen {
            let date = Date(timeIntervalSince1970: TimeInterval(lastSeen))
            self.lblLastSeen.text = date.convertToString("dd-MMM-yyyy hh:mm a")
        }
        self.locationManager.delegate = self
    }
    
    //Downloads messages
    func userOnlineStatus() {
        User.userStatus(id: self.currentUser!.id) { (data) in
            print(data)
            if let isTyping = data["isTyping"] as? Bool, isTyping {
                DispatchQueue.main.async {
                    self.lblLastSeen.text = "Typing..."
                }
            } else {
                if let online = data["isOnline"] as? Bool, online {
                    DispatchQueue.main.async {
                        self.lblLastSeen.text = "Online"
                    }
                    
                } else {
                    if let lastSeen = data["lastSeen"] as? String {
                        DispatchQueue.main.async {
                            self.lblLastSeen.text = lastSeen
                        }
                    } else if let lastSeen = data["lastSeen"] as? Int {
                        let date = Date(timeIntervalSince1970: TimeInterval(lastSeen))
                        DispatchQueue.main.async {
                            self.lblLastSeen.text = date.convertToString("dd-MMM-yyyy hh:mm a")
                        }
                    }
                }
            }
        }
    }
    
    func fetchData() {
        if groupConvo == nil {
            userOnlineStatus()
        }else {
            self.lblLastSeen.text = ""
        }
        
        var toId = groupConvo?.id ?? ""
        if groupConvo?.id == nil || groupConvo?.id == ""{
            toId = self.currentUser!.id
        }
        Message.downloadAllMessages(forUserID: toId, completion: { [weak weakSelf = self] (message) in
            weakSelf?.items.append(message)
            weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
            DispatchQueue.main.async {
                if let state = weakSelf?.items.isEmpty, state == false {
                    weakSelf?.tableView.reloadData()
                    weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                    if message.owner == .sender && weakSelf?.navigationController?.viewControllers.last != nil{
                        Message.markSingleMessageAsRead(messageID: message.messageId!, forUserID: self.currentUser!.id)
                    }
                }
            }
        }) { [weak weakSelf = self] (updatedMsg) in
            let object = weakSelf?.items.filter({$0.messageId! == updatedMsg.messageId!}).first
            object?.isRead = updatedMsg.isRead
            if updatedMsg.owner == .receiver {
                weakSelf?.tableView.reloadData()
                weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
            }
            //reload cell
        }
        Message.markMessagesRead(forUserID: self.currentUser!.id)
    }
    
    //Hides current viewcontroller
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        print("deinit")
    }
    
    func composeMessage(type: MessageType, content: Any)  {
        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, msgId: "")
        var toId = groupConvo?.id ?? ""
        if groupConvo?.id == nil || groupConvo?.id == "" {
            toId = self.currentUser!.id
        }
        Message.send(message: message, toID: toId, completion: {(_) in})
    }
    
    func checkLocationPermission() -> Bool {
        var state = false
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            state = true
        case .authorizedAlways:
            state = true
        default: break
        }
        return state
    }
    

    @IBAction func showMessage(_ sender: Any) {
       //self.animateExtraButtons(toHide: true)
    }
    
    @IBAction func selectGallery(_ sender: Any) {
        //self.animateExtraButtons(toHide: true)
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .savedPhotosAlbum;
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectCamera(_ sender: Any) {
        //self.animateExtraButtons(toHide: true)
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        self.canSendLocation = true
       // self.animateExtraButtons(toHide: true)
        if self.checkLocationPermission() {
            self.locationManager.startUpdatingLocation()
        } else {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    @IBAction func showOptions(_ sender: Any) {
        //self.animateExtraButtons(toHide: false)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        self.txtViewContainerHeight.constant = 40
        if let text = self.inputTextField.text {
            if text.count > 0 {
                self.composeMessage(type: .text, content: self.inputTextField.text!)
                self.inputTextField.text = ""
            }
        }
    }
    
    //MARK: NotificationCenter handlers
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            //self.tableView.contentInset.bottom = height
            self.tableView.scrollIndicatorInsets.bottom = height
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                if self.items.count > 0 {
                    self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
                }
                if self.hasTopNotch {
                    if UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 == 40 {
                        self.bottomConstraint.constant = height+20
                    }else {
                        self.bottomConstraint.constant = height-35
                    }
                } else {
                    self.bottomConstraint.constant = height
                }
                self.bottomConstrainntView.constant = height
                self.view.frame = APP_DELEDATE.window!.frame
                self.view.setNeedsLayout()
            }
        }
    }
    
    @objc func hideKeyboard(notification: Notification) {
        print(UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0)
        if UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 == 40 {
            self.bottomConstraint.constant = 28
        } else {
            self.bottomConstraint.constant = 8
        }
        self.tableView.contentInset.bottom = 0
        self.tableView.scrollIndicatorInsets.bottom = 0
        bottomConstrainntView.constant = 0
    }
    
    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.items[indexPath.row].owner {
        case .receiver:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
            cell.clearCellData()
            let lastSeen = self.items[indexPath.row].timestamp
            let date = Date(timeIntervalSince1970: TimeInterval(lastSeen))
            cell.lblTime.text = date.convertToString("hh:mm a")
            if self.items[indexPath.row].isRead {
                cell.imgViewReadStatus.image = UIImage(named: "ic_chat_read")
            } else {
                cell.imgViewReadStatus.image = UIImage(named: "ic_chat_sent")
            }
            switch self.items[indexPath.row].type {
            case .text:
                cell.message.text = self.items[indexPath.row].content as! String
            case .photo:
                if let image = self.items[indexPath.row].image {
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                } else {
                    cell.messageBackground.image = UIImage.init(named: "loading")
                    self.items[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        if state == true {
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    })
                }
            case .location:
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
            }
            return cell
        case .sender:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            cell.clearCellData()
            let lastSeen = self.items[indexPath.row].timestamp
            let date = Date(timeIntervalSince1970: TimeInterval(lastSeen))
            cell.lblTime.text = date.convertToString("hh:mm a")
            
            switch self.items[indexPath.row].type {
            case .text:
                cell.message.text = self.items[indexPath.row].content as! String
            case .photo:
                if let image = self.items[indexPath.row].image {
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                } else {
                    cell.messageBackground.image = UIImage.init(named: "loading")
                    self.items[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        if state == true {
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    })
                }
            case .location:
                cell.messageBackground.image = UIImage.init(named: "location")
                cell.message.isHidden = true
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.inputTextField.resignFirstResponder()
        switch self.items[indexPath.row].type {
        case .photo:
            if let photo = self.items[indexPath.row].image {
                showImage(image: photo)
                //let info = ["viewType" : ShowExtraView.preview, "pic": photo] as [String : Any]
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
                self.inputAccessoryView?.isHidden = true
            }
        case .location:
            //let coordinates = (self.items[indexPath.row].content as! String).components(separatedBy: ":")
            //let location = CLLocationCoordinate2D.init(latitude: CLLocationDegrees(coordinates[0])!, longitude: CLLocationDegrees(coordinates[1])!)
            //let info = ["viewType" : ShowExtraView.map, "location": location] as [String : Any]
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
            self.inputAccessoryView?.isHidden = true
            default: break
        }
    }
    
    func showImage(image: UIImage) {
        let images = [
            LightboxImage(
                image: image,
                text: ""
            )
        ]
        // Create an instance of LightboxController.
        let controller = LightboxController(images: images)
        
        // Set delegates.
        //controller.pageDelegate = self
        //controller.dismissalDelegate = self
        
        // Use dynamic background.
        controller.dynamicBackground = true
        
        // Present your controller.
        present(controller, animated: true, completion: nil)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        switch self.items[indexPath.row].type {
//        case .text:
//            return UITableView.automaticDimension
//        case .photo:
//            return 100
//        case .location:
//            return UITableView.automaticDimension
//        }
//    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            self.composeMessage(type: .photo, content: pickedImage)
        }else {
            let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            self.composeMessage(type: .photo, content: pickedImage)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        if let lastLocation = locations.last {
            if self.canSendLocation {
                let coordinate = String(lastLocation.coordinate.latitude) + ":" + String(lastLocation.coordinate.longitude)
                let message = Message.init(type: .location, content: coordinate, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, msgId: "")
                Message.send(message: message, toID: self.currentUser!.id, completion: {(_) in
                })
                self.canSendLocation = false
            }
        }
    }

    
}
extension ChatVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let pos = textView.endOfDocument
        let currentRect = textView.caretRect(for: pos)
        if currentRect.origin.y >= lastTextViewRect.origin.y {
            UIView.animate(withDuration: 0.3) {
                if self.txtViewContainerHeight.constant < 100 {
                    self.txtViewContainerHeight.constant = 35+currentRect.origin.y
                }
                self.view.layoutIfNeeded()
            }
        } else  {
            UIView.animate(withDuration: 0.3) {
                self.txtViewContainerHeight.constant = 30+currentRect.origin.y
                self.view.layoutIfNeeded()
            }
        }
        lastTextViewRect = currentRect
        isTyping = true
        timer.invalidate()
        if let myId = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(myId).child("credentials").updateChildValues(["isTyping": true])
        }
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTypingStatus), userInfo: nil, repeats: false)
    }
    
}
