//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import Foundation
import UIKit
import Firebase
import FirebaseDatabase
var databaseRef: DatabaseReference!
var storageRef: StorageReference!

class User: Hashable {
    
    //MARK: Properties
    let name: String
    let email: String
    let id: String
    var profilePic: UIImage
    var lastSeen: Int
    var isOnline: Bool
    var isTyping: Bool
    
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    
    //MARK: Methods
    class func registerUser(withName: String, email: String, password: String, profilePic: UIImage, completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (result, error) in
            if error == nil {
                databaseRef = Database.database().reference()
                result!.user.sendEmailVerification(completion: nil)
                storageRef = Storage.storage().reference()
                let imageData = profilePic.jpegData(compressionQuality: 0.5)
                storageRef.child("usersProfilePics").child("\(result!.user.uid).jpeg").putData(imageData!, metadata: nil, completion: { (metadata, err) in
                    if err == nil {
                        let path = metadata?.path
                        
                        let values = ["name": withName, "email": email, "profilePicLink": path!]
                            databaseRef.child("users").child((result!.user.uid)).child("credentials").setValue(values, withCompletionBlock: { (errr, _) in
                            if errr == nil {
                                let userInfo = ["email" : email, "password" : password]
                                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                                completion(true)
                            }
                        })
                    }
                    else {
                        print(err)
                        completion(false)
                    }
                })
            }
            else {
                print(error)
                completion(false)
            }
        })
    }
    
    class func changeProfilePicture(image: UIImage = UIImage(), url: String = "") {
        guard let id = Auth.auth().currentUser?.uid else {
            return
        }
        if url != "" {
            Database.database().reference().child("users").child(id).child("credentials").updateChildValues(["profilePicLink": url])
            return
        }
        storageRef = Storage.storage().reference()
        let imageData = image.jpegData(compressionQuality: 0.5)
        if imageData == nil {return}
        storageRef.child("usersProfilePics").child(id+".jpeg").putData(imageData!, metadata: nil, completion: { (metadata, err) in
            if err == nil {
                let path = metadata?.path
                let values = ["profilePicLink": path!]
                databaseRef.child("users").child(id).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                    if errr == nil {
                        print(path)
                    }
                })
            }
            else {
                print(err)
            }
        })
    }
    
   class func loginUser(withEmail: String, password: String, completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().signIn(withEmail: withEmail, password: password, completion: { (result, error) in
            if error == nil {
                User.info(forUserID: result!.user.uid, completion: { (downLoadUser) in
                })
                let userInfo = ["email": withEmail, "password": password]
                UserDefaults.standard.set(userInfo, forKey: "userInformation")
                completion(true)
            } else {
                print(error)
                completion(false)
            }
        })
    }
    
    class func userStatus(id: String, completion: @escaping ([String: Any]) -> Swift.Void) {
        User.info(forUserID: id) { (user) in
            completion(["isOnline":user.isOnline, "lastSeen":user.lastSeen, "isTyping": user.isTyping])
        }
        Database.database().reference().child("users").child(id).observe(.childChanged) { (snapshot) in
            let data = snapshot.value as! [String: Any]
            let onlineStatus = data["isOnline"]as? Bool ?? false
            let lastSeen = data["lastSeen"] as? Int ?? 0
            let isTyping = data["isTyping"]as? Bool ?? false
            if lastSeen == 0{
                return
            }
            completion(["isOnline":onlineStatus, "lastSeen":lastSeen, "isTyping": isTyping])
        }
    }
    
    class func logOutUser(completion: @escaping (Bool) -> Swift.Void) {
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.removeObject(forKey: "userInformation")
            completion(true)
        } catch _ {
            completion(false)
        }
    }
    
   class func info(forUserID: String, completion: @escaping (User) -> Swift.Void) {
    if forUserID == "" {
        return
    }
    Database.database().reference().child("users").child(forUserID).child("credentials").observeSingleEvent(of: .value, with: { (snapshot) in
        print("user received:",snapshot.value)
            guard let data = snapshot.value as? [String: Any] else {
                return
            }
            let name = data["name"] as? String ?? ""
            let email = data["email"] as? String ?? ""
            let onlineStatus = data["isOnline"]as? Bool ?? false
            let isTyping = data["isTyping"]as? Bool ?? false
            let lastSeen = data["lastSeen"] as? Int ?? 0
            let link = data["profilePicLink"]as? String ?? ""
        
        
            if link.contains("https") {
                URLSession.shared.dataTask(with: URL(string: link)!, completionHandler: { (data, response, error) in
                    if error == nil {
                        let profilePic = UIImage.init(data: data!)
                        let user = User.init(name: name, email: email, id: forUserID, profilePic: profilePic!, isOnline: onlineStatus, lastSeen: lastSeen, isTyping: isTyping)
                        completion(user)
                    }
                }).resume()
                return
            }
            let path = Storage.storage().reference().child(data["profilePicLink"]as? String ?? "")
            print(path)
            print(data["profilePicLink"]as? String ?? "")
            path.downloadURL(completion: { (url, nil) in
                if url == nil {
                    let user = User.init(name: name, email: email, id: forUserID, profilePic: UIImage(named: "chatUser")!, isOnline: onlineStatus, lastSeen: lastSeen, isTyping: isTyping)
                    completion(user)
                    return
                }
                URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                    if error == nil {
                        let profilePic = UIImage.init(data: data!)
                        let user = User.init(name: name, email: email, id: forUserID, profilePic: profilePic!, isOnline: onlineStatus, lastSeen: lastSeen, isTyping: isTyping)
                        completion(user)
                    }
                }).resume()
            })
        })
    }
    
    class func downloadAllUsers(exceptID: String, completion: @escaping (User) -> Swift.Void) {
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            let id = snapshot.key
            let data = snapshot.value as! [String: Any]
            let credentials = data["credentials"] as! [String: Any]
            if id != exceptID {
                let name = credentials["name"]as? String ?? ""
                let email = credentials["email"]as? String ?? ""
                let onlineStatus = credentials["isOnline"]as? Bool ?? false
                let isTyping = data["isTyping"]as? Bool ?? false
                let lastSeen = credentials["lastSeen"] as? Int ?? 0
                print(credentials)
                if let link = credentials["profilePicLink"] as? String {
                    URLSession.shared.dataTask(with: URL(string: link)!, completionHandler: { (data, response, error) in
                        if error == nil {
                            let profilePic = UIImage.init(data: data!)
                            let user = User.init(name: name, email: email, id: id, profilePic: profilePic!, isOnline: onlineStatus, lastSeen: lastSeen, isTyping: isTyping)
                            completion(user)
                        }else {
                            let user = User.init(name: name, email: email, id: id, profilePic: UIImage(named: "user")!, isOnline: onlineStatus, lastSeen: lastSeen, isTyping: isTyping)
                            completion(user)
                        }
                    }).resume()
                }else {
                    let user = User.init(name: name, email: email, id: id, profilePic: UIImage(named: "user")!, isOnline: onlineStatus, lastSeen: lastSeen, isTyping: isTyping)
                    completion(user)
                }
            }
        })
    }
    
    class func checkUserVerification(completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().currentUser?.reload(completion: { (_) in
            let status = (Auth.auth().currentUser?.isEmailVerified)!
            completion(status)
        })
    }

    
    //MARK: Inits
    init(name: String, email: String, id: String, profilePic: UIImage, isOnline: Bool, lastSeen: Int, isTyping: Bool) {
        self.name = name
        self.email = email
        self.id = id
        self.profilePic = profilePic
        self.lastSeen = lastSeen
        self.isOnline = isOnline
        self.isTyping = isTyping
    }
}

