//
//  GroupConversation.swift
//  Beyond
//
//  Created by salman on 08/07/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.
//

import UIKit
import Firebase
import Foundation

class GroupConversation: NSObject {
    var lastMessage: Message
    var unreadCount: Int
    var isArchive = false
    var id: String
    var groupImage: String
    var groupName: String
    var location: String
    typealias UpdateGroupConversation = ([String: Any]?) -> Void
    static var conversion: UpdateGroupConversation?
    
    //MARK: Methods
    class func showGroupConversations(completion: @escaping ([GroupConversation]) -> Swift.Void, convo: @escaping UpdateGroupConversation) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            var conversations = [GroupConversation]()
            conversion = convo
            Database.database().reference().child("users").child(currentUserID).child("GroupConversation").observe(.childAdded, with: { (snapshot) in
                if snapshot.exists() {
                    let fromID = snapshot.key
                    let values = snapshot.value as! [String: Any]
                    let location = values["location"] as? String ?? ""
                    let unreadCount = values["unreadCount"] as? Int ?? 0
                    let archive = values["isArchive"] as? Bool ?? false
                    let groupImage = values["groupImage"] as? String ?? ""
                    let groupName = values["groupName"] as? String ?? ""
                    addUnreadCountListener(senderId: fromID)
                    let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, msgId: "")
                    let conversation = GroupConversation.init(id: fromID, lastMessage: emptyMessage, unreadCount: unreadCount, isArchive: archive, groupImage: groupImage, groupName: groupName, location: location)
                    conversations.append(conversation)
                    conversation.lastMessage.downloadLastMessage(forLocation: location, completion: {
                        completion(conversations)
                    })
                }
            })
        }
    }
    
    class func setUnreadCount(toId: String) {
        if let senderID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(toId).child("GroupConversations").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: Any]
                    let unreadCount = data["unreadCount"] as? Int ?? 0
                    Database.database().reference().child("users").child(toId).child("conversations").child(senderID).updateChildValues(["unreadCount": unreadCount+1])
                } else {
                    
                }
            })
        }
    }
    
    private class func addUnreadCountListener(senderId: String) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserID).child("conversations").observe(.childChanged) { (snapshot) in
                var values = snapshot.value as? [String: Any]
                values?["id"] = senderId
                conversion!(values)
            }
        }
    }
    
    //MARK: Init
    init(id: String, lastMessage: Message, unreadCount: Int, isArchive: Bool, groupImage: String, groupName: String, location: String) {
        self.id = id
        self.lastMessage = lastMessage
        self.unreadCount = unreadCount
        self.isArchive = isArchive
        self.groupImage = groupImage
        self.groupName = groupName
        self.location = location
    }
}
