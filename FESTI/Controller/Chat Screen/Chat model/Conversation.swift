//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Foundation
import UIKit
import Firebase



class Conversation: Hashable {
    static func == (lhs: Conversation, rhs: Conversation) -> Bool {
        return lhs.id == rhs.id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    //MARK: Properties
    let user: User?
    var lastMessage: Message
    var unreadCount: Int
    var isArchive = false
    var isGroup = false
    var id: String?
    var groupImage: String?
    var groupName: String?
    var location: String?
    typealias UpdateConversation = ([String: Any]?) -> Void
    static var conversion: UpdateConversation?
    
    //MARK: Methods
    class func showConversations(completion: @escaping ([Conversation]) -> Swift.Void, convo: @escaping UpdateConversation) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            var conversations = [Conversation]()
            conversion = convo
            SwiftLoader.hide()
            Database.database().reference().child("users").child(currentUserID).child("conversations").observe(.childAdded, with: { (snapshot) in
                print(snapshot.key)
                if snapshot.exists() {
                    let id = snapshot.key
                    let values = snapshot.value as! [String: Any]
                    let location = values["location"] as? String ?? ""
                    let unreadCount = values["unreadCount"] as? Int ?? 0
                    let archive = values["isArchive"] as? Bool ?? false
                    print("snap:",snapshot.key)
                    if location == "" {
                        return
                    }
                    if let groupName = values["groupName"] as? String, let groupImage = values["groupImage"] as? String {
                        addUnreadCountListener(senderId: id)
                        let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, msgId: "")
                        emptyMessage.downloadLastMessage(forLocation: location, completion: {
                            User.info(forUserID: emptyMessage.fromID ?? "", completion: { (user) in
                                let conversation = Conversation.init(user: user, lastMessage: emptyMessage, unreadCount: unreadCount, isArchive: archive)
                                conversation.groupName = groupName
                                conversation.groupImage = groupImage
                                conversation.id = id
                                conversation.location = location
                                conversation.isGroup = true
                                conversations.append(conversation)
                                completion(conversations)
                            })
                        })
                    } else {
                        addUnreadCountListener(senderId: id)
                        User.info(forUserID: id, completion: { (user) in
                            let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, msgId: "")
                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage, unreadCount: unreadCount, isArchive: archive)
                            conversation.id = id
                            conversations.append(conversation)
                            conversation.lastMessage.downloadLastMessage(forLocation: location, completion: {
                                completion(conversations)
                            })
                        })
                    }
                }else {
                    print("No Data avaialble")
                     SwiftLoader.hide()
                }
            })
        }else {
            print("User not authorized")
            SwiftLoader.hide()
        }
    }
    
    class func createGroupWithUser(users: [String], name: String, image: UIImage) {
        storageRef = Storage.storage().reference()
        let groupChatKey = Database.database().reference().child("users").childByAutoId().key
        if let senderID = Auth.auth().currentUser?.uid {
            let message = Message.init(type: .text, content: "Welcome to \(name)", owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, msgId: "")
            let values = ["type": "text", "content": message.content, "fromID": senderID, "toID": groupChatKey!, "timestamp": message.timestamp, "isRead": false]
            Database.database().reference().child("conversations").childByAutoId().childByAutoId().setValue(values, withCompletionBlock: { (error, reference) in
                let location = reference.parent!.key
                let imageData = image.jpegData(compressionQuality: 0.5)
                storageRef.child("groupPicture").child("\(groupChatKey!).jpeg").putData(imageData!, metadata: nil, completion: { (metadata, err) in
                    if err == nil {
                        let path = metadata?.path ?? ""
                        for user in users {
                            let data = ["groupImage":path, "groupName":name, "unreadCount": 0,"location": location] as [String : Any]
                            Database.database().reference().child("users").child(user).child("conversations").child(groupChatKey!).updateChildValues(data)
                        }
                    }
                    else {
                        print(err)
                    }
                })
            })
        }
    }
    
    class func setUnreadCount(toId: String) {
        if let senderID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(toId).child("conversations").child(senderID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: Any]
                    let unreadCount = data["unreadCount"] as? Int ?? 0
                    Database.database().reference().child("users").child(toId).child("conversations").child(senderID).updateChildValues(["unreadCount": unreadCount+1])
                } else {
                    
                }
            })
        }
    }
    
    class func markAs(forArchive: Bool,toId: String) {
        if let senderID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(senderID).child("conversations").child(toId).updateChildValues(["isArchive": forArchive])
        }
    }
    
    class func deleteConvo(toUser: String, forGroup: Bool) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserID).child("conversations").child(toUser).observe(.value) { (snap) in
                if snap.exists() {
                    let values = snap.value as! [String: Any]
                    let location = values["location"] as? String ?? ""
                    Database.database().reference().child("users").child(currentUserID).child("conversations").child(toUser).removeValue()
                    Database.database().reference().child("users").child(toUser).child("conversations").child(currentUserID).removeValue()
                    if !forGroup {
                        Database.database().reference().child("conversations").child(location).removeValue()
                    }
                }
            }
        }
    }
    
    
    
    private class func addUnreadCountListener(senderId: String) {
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserID).child("conversations").observe(.childChanged) { (snapshot) in
                var values = snapshot.value as? [String: Any]
                print(snapshot)
                values?["id"] = snapshot.key
                if let _ = values?["groupName"] as? String, let _ = values?["groupImage"] as? String {
                    values?["id"] = snapshot.key
                }
                conversion!(values)
            }
        }
    }
    
    //MARK: Inits
    init(user: User, lastMessage: Message, unreadCount: Int, isArchive: Bool) {
        self.user = user
        self.lastMessage = lastMessage
        self.unreadCount = unreadCount
        self.isArchive = isArchive
    }
}
