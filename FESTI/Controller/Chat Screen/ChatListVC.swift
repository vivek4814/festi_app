//
//  ChatVC.swift
//  Beyond
//
//  Created by Kamlesh Kumar on 13/06/19.
//  Copyright © 2019 Virtual Employee. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage


class ChatCell: UITableViewCell{
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var currentStatus: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var lastMsgLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var unreadMsgLbl: UILabel!
    @IBOutlet weak var lblArchive: UILabel!
    @IBOutlet weak var imageConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgViewAttachement: UIImageView!
    
    
    func downloadImage(convo: Conversation) {
        userImg.makeRounded()
        if convo.isGroup {
            userImg.image = UIImage(named: "group")
            let path = Storage.storage().reference().child(convo.groupImage ?? "")
            path.downloadURL(completion: { (url, nil) in
                if url == nil {
                    return
                }
                self.userImg.setImage(url: url!)
            })
        } else {
            print(convo.user?.profilePic)
            userImg.image = convo.user?.profilePic
        }
    }
    
    func updateOnlineStatus(id: String) {
        User.userStatus(id: id) { (data) in
        if let isTyping = data["isTyping"] as? Bool, isTyping {
//            DispatchQueue.main.async {
//                self.lblLastSeen.text = "Typing..."
//            }
        } else {
            if let online = data["isOnline"] as? Bool, online {
                DispatchQueue.main.async {
                    //self.lblLastSeen.text = "Online"
                    self.currentStatus.backgroundColor = UIColor.green
                }
                
            } else {
                DispatchQueue.main.async {
                    self.currentStatus.backgroundColor = UIColor.red
                }
                
//                if let lastSeen = data["lastSeen"] as? String {
//                    DispatchQueue.main.async {
//                        self.lblLastSeen.text = lastSeen
//                    }
//                } else if let lastSeen = data["lastSeen"] as? Int {
//                    let date = Date(timeIntervalSince1970: TimeInterval(lastSeen))
//                    DispatchQueue.main.async {
//                        self.lblLastSeen.text = date.convertToString("dd-MMM-yyyy hh:mm a")
//                    }
//                }
            }
        }
    }}
}

class ArchiveChatCell: UITableViewCell{
    @IBOutlet weak var totalArchivedLbl: UILabel!
}

class ChatListVC: UIViewController {
    @IBOutlet weak var chatTblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var userImage: UIImageView!
    var items = [Conversation]()
    var archiveItems = [Conversation]()
    var isArchivedChatsRowVisible = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchData()
    }
    override func viewWillAppear(_ animated: Bool) {
        chatTblView.delegate = self
        //if let url = URL(string: getUserDefault(Constant.kPROFILE_IMAGE)) {
            //userImage.sd_setImage(with: url, placeholderImage: UIImage(named: "chatUser"))
        //}
    }
  
    
    
 
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Downloads conversations
    func fetchData() {
        SwiftLoader.show(title: "Loading...", animated: true)
        Conversation.showConversations(completion: { (conversations) in
            DispatchQueue.main.async(execute: {
                SwiftLoader.hide()
            })
            self.items = Array(Set(conversations))
            
            self.archiveItems = self.items.filter({ (item) -> Bool in
                return item.isArchive == true
            })
            self.items = self.items.filter({ (item) -> Bool in
                return item.isArchive == false
            })
            self.items.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp }
            DispatchQueue.main.async {
                self.chatTblView.reloadData()
                for conversation in self.items {
                    if conversation.lastMessage.isRead == false {
                        break
                    }
                }
            }
        }) { (updateConvo) in
            if updateConvo == nil {return}
            print("updateConvo",updateConvo ?? "")
            if let count  = updateConvo!["unreadCount"] as? Int{
                let object = self.items.filter({$0.user!.id == updateConvo!["id"]as? String}).first
                if object?.unreadCount != count {
                    object?.unreadCount = count
                    self.chatTblView.reloadData()
                }
            }
            if let isArchive = updateConvo!["isArchive"] as? Bool {
                if isArchive {
                    var itemIndex = [Int]()
                    if let _ = updateConvo!["groupName"] as? String, let _ = updateConvo!["groupImage"] as? String {
                        itemIndex = self.items.enumerated().compactMap { $0.element.id == (updateConvo!["id"]as? String ?? "") ? $0.offset : nil }
                    }else {
                        itemIndex = self.items.enumerated().compactMap { $0.element.user!.id == (updateConvo!["id"]as? String ?? "") ? $0.offset : nil }
                        if itemIndex.count == 0 {
                            return
                        }
                        if self.items[itemIndex.first!].isGroup {
                            return
                        }
                    }
                    if itemIndex.count == 0 {
                        return
                    }
                   
                    let archiveItem = self.items.remove(at: itemIndex.first!)
                    self.archiveItems.append(archiveItem)
                    self.chatTblView.reloadData()
                } else {
                    var itemIndex = [Int]()
                    if let _ = updateConvo!["groupName"] as? String, let _ = updateConvo!["groupImage"] as? String {
                        itemIndex = self.archiveItems.enumerated().compactMap { $0.element.id == (updateConvo!["id"]as? String ?? "") ? $0.offset : nil }
                    }else {
                        itemIndex = self.archiveItems.enumerated().compactMap { $0.element.user!.id == (updateConvo!["id"]as? String ?? "") ? $0.offset : nil }
                    }
                    if itemIndex.count == 0 {
                        return
                    }
                    let unarchiveItem = self.archiveItems.remove(at: itemIndex.first!)
                    self.items.append(unarchiveItem)
                    self.chatTblView.reloadData()
                    //self.chatTblView.insertRows(at: [IndexPath(row: itemIndex.first!, section: 0)], with: .automatic)
                }
                self.items.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp }
            }            
        }
    }
    
    func deleteChat(indexPath: IndexPath) {
        var item: Conversation!
        if isArchivedChatsRowVisible {
            item = items[indexPath.row-1]
        } else {
            item = items[indexPath.row]
        }
        var msg = "Delete chat with \(item.user?.name ?? "")?"
        if item.isGroup {
            msg = "Delete chat of \(item.groupName ?? "")?"
        }
        self.showAlertWithActions(msg: msg, titles: ["Delete Chat", "Archive Instead", "Cancel"]) { (index) in
            print(index)
            if index == 1 {
                var id = item.user?.id
                if item.isGroup {
                    id = item.id
                }
                Conversation.deleteConvo(toUser: id ?? "", forGroup: item.isGroup)
                self.items.remove(at: indexPath.row)
                self.chatTblView.deleteRows(at: [indexPath], with: .none)
                //delete
            }
            if index == 2 {
                self.archiveChat(indexPath: indexPath)
            }
        }
    }
    
    func archiveChat(indexPath: IndexPath) {
        var item: Conversation!
        if isArchivedChatsRowVisible {
            item = items[indexPath.row-1]
        } else {
            item = items[indexPath.row]
        }
        var id = item.user?.id
        if item.isGroup {
            id = item.id
        }
        print("make archive",id! )
        Conversation.markAs(forArchive: true, toId: id!)
        

//        var itemIndex = [Int]()
//        if item.isGroup {
//            itemIndex = self.items.enumerated().compactMap { $0.element.id == item.id ? $0.offset : nil }
//        } else {
//            itemIndex = self.items.enumerated().compactMap { $0.element.user!.id == item.user!.id ? $0.offset : nil }
//        }
//        self.items.remove(at: itemIndex.first!)
//        self.archiveItems.append(items[itemIndex.first!])
//        self.chatTblView.reloadData()
    }
    
}

extension ChatListVC: UITableViewDataSource, UITableViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView.contentOffset.y > 50 && isArchivedChatsRowVisible {
//            isArchivedChatsRowVisible = false
//            chatTblView.beginUpdates()
//            chatTblView.deleteRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
//            chatTblView.endUpdates()
//        }else if scrollView.contentOffset.y < -30 && !isArchivedChatsRowVisible {
//            isArchivedChatsRowVisible = true
//            chatTblView.beginUpdates()
//            chatTblView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
//            chatTblView.endUpdates()
//        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if isArchivedChatsRowVisible {
//            return items.count+1
//        }
//        else {
            return items.count
        //}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 0 && isArchivedChatsRowVisible {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ArchiveChatCell", for: indexPath) as! ArchiveChatCell
//            cell.totalArchivedLbl.text = "\(archiveItems.count)"
//            return cell
//        } else {
            var data: Conversation!
//            if isArchivedChatsRowVisible {
//                data = items[indexPath.row-1]
//            } else {
                data = items[indexPath.row]
           // }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
            cell.unreadMsgLbl.layer.cornerRadius = 10.0
            cell.unreadMsgLbl.layer.masksToBounds = true
            if data.lastMessage.type == .photo {
                cell.lastMsgLbl.text = "Photo"
                cell.imgViewAttachement.isHidden = false
                cell.imageConstraint.constant = 28
            }else {
                cell.lastMsgLbl.text = data.lastMessage.content as? String
                cell.imageConstraint.constant = 10
                cell.imgViewAttachement.isHidden = true
            }
            if data.isGroup {
                cell.userName.text = data.groupName
                cell.currentStatus.isHidden = true
            } else {
                cell.userName.text =  data.user?.name
                cell.updateOnlineStatus(id: data.user!.id)
                cell.currentStatus.isHidden = false
            }
            
            if data.unreadCount == 0 {
                cell.unreadMsgLbl.isHidden = true
            }else {
                cell.unreadMsgLbl.isHidden = false
                cell.unreadMsgLbl.text = "\(data.unreadCount)"
            }
            let messageDate = Date(timeIntervalSince1970: TimeInterval(data.lastMessage.timestamp))
            if messageDate.day() == Date().day() && messageDate.year() == Date().year() && messageDate.month() == Date().month() {
                cell.timeLbl.text = "Today \(messageDate.convertToString("hh:mm a"))"
            } else {
                cell.timeLbl.text = messageDate.convertToString("dd-MMM-yyyy hh:mm a")
            }
            cell.downloadImage(convo: data)
            return cell
        //}
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 0 && isArchivedChatsRowVisible {
//            return 50
//        }
        return 70
    }

    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if indexPath.row == 0 && isArchivedChatsRowVisible {
            return nil
        }
        let closeAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("delete action")
            self.deleteChat(indexPath: indexPath)
            success(true)
        })
        closeAction.image = UIImage(named: "ic_delete")
        UIImageView.appearance(
            whenContainedInInstancesOf: [UITableView.self])
            .tintColor = .red
        closeAction.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?{
        if indexPath.row == 0 && isArchivedChatsRowVisible {
            return nil
        }
        let modifyAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("Archive action")
            self.archiveChat(indexPath: indexPath)
            success(true)
        })
        UIImageView.appearance(
            whenContainedInInstancesOf: [UITableView.self])
            .tintColor = UIColor.blue
        modifyAction.image = UIImage(named: "ic_archive")
        modifyAction.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        return UISwipeActionsConfiguration(actions: [modifyAction])
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0 && isArchivedChatsRowVisible {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chat = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
//        if isArchivedChatsRowVisible {
//            if items[indexPath.row-1].isGroup {
//                chat.groupConvo = items[indexPath.row-1]
//            }
//            chat.currentUser = items[indexPath.row-1].user
//        }
//        else {
            if items[indexPath.row].isGroup {
                chat.groupConvo = items[indexPath.row]
            }
            chat.currentUser = items[indexPath.row].user
        //}
        self.navigationController?.pushViewController(chat, animated: true)
    }
}

