//
//  ChatUserListVC.swift
//  FESTI
//
//  Created by Salman on 27/04/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class ChatUserListVC: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var _tableView: UITableView!
    
    let userID = UserManager().currentUserID() ?? ""

    private let manager = ConversationManager()
    private let userManager = UserManager()
    private var currentUser: ObjectUser?
    private var conversations = [ObjectConversation]()
    
    private var conversation = ObjectConversation()
    
    var filterdata = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        _tableView.delegate = self
        searchbar.delegate = self
        if let textfield = searchbar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.1215686275, blue: 0.1215686275, alpha: 1)
            textfield.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            textfield.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           
        }
        _tableView.dataSource = self
        _tableView.separatorStyle = .none
        _tableView.backgroundColor = UIColor.clear
        print("sddsmmds",globalUser?.uid)
        fetchConversations()
        filterdata = conversations
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    @IBAction func _backBtnClcked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let id = conversations.first?.userIDs.filter({$0 != userID}).first else { return }
        self.filterdata = []
        User.getUserById(userId: id) { (_user) in
            let new = "\(_user?.firstName ?? "") \(_user?.lastName ?? "")"
            if searchText == "" {
                self.filterdata = self.conversations
            }else{
                if new.lowercased().contains(searchText.lowercased()){
                    self.filterdata.append(new)
                }
            }
            self._tableView.reloadData()
        }
    }
}

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText \(String(describing: searchBar.text))")
    }


extension ChatUserListVC : UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let vc: MessageVC = UIStoryboard.initial(storyboard: .messages)
        vc.conversation = conversations[indexPath.row]
        manager.markAsRead(conversations[indexPath.row])
        let backItem = UIBarButtonItem()
        backItem.tintColor = .white
        backItem.title = ""
        self.navigationItem.backBarButtonItem = backItem
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension ChatUserListVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !filterdata.isEmpty {
                return filterdata.count
            }
            return conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ChatUserListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatUserListTableViewCell", for: indexPath) as!
        ChatUserListTableViewCell
        cell.info = self.conversations[indexPath.row]
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        return cell
        
    }
}

extension ChatUserListVC {
  
  func fetchConversations() {
    manager.currentConversations {[weak self] conversations in
      self?.conversations = conversations.sorted(by: {$0.timestamp > $1.timestamp})
        print(" self?.conversations:\( self?.conversations.count)")
      self?._tableView.reloadData()
      self?.playSoundIfNeeded()
    }
  }
  
  func fetchProfile() {
    userManager.currentUserData {[weak self] user in
      self?.currentUser = user
      if let urlString = user?.profilePicLink {
      //  self?.profileImageView.setImage(url: URL(string: urlString))
      }
    }
  }
  
  func playSoundIfNeeded() {
    guard let id = userManager.currentUserID() else { return }
    if conversations.last?.isRead[id] == false {
      AudioService().playSound()
    }
  }
}

