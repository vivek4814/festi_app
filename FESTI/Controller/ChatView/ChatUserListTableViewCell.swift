//
//  ChatUserListTableViewCell.swift
//  FESTI
//
//  Created by Salman on 27/04/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

class ChatUserListTableViewCell: UITableViewCell {

    @IBOutlet weak var _messageLbl: UILabel!
    @IBOutlet weak var _datCreationLbl: UILabel!
    @IBOutlet weak var _userNameLbl: UILabel!
    @IBOutlet weak var _userImageView: UIImageView!
    let userID = UserManager().currentUserID() ?? ""

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var info : ObjectConversation?  {
        didSet {
            _userImageView.layer.cornerRadius = _userImageView.frame.size.width/2
            _userImageView.layer.masksToBounds = true
            self._messageLbl.text = info?.lastMessage
            self._userNameLbl.text = "Test"
            _datCreationLbl.text = Date(timeIntervalSince1970: TimeInterval(info?.timestamp ?? 0)).timeAgoSinceDate()
           
            guard let id = info?.userIDs.filter({$0 != userID}).first else { return }
            
            User.getUserById(userId: id) { (_user) in
                self._userNameLbl.text = "\(_user?.firstName ?? "") \(_user?.lastName ?? "")"
              guard let urlString = _user?.profileImage else {
                    self._userImageView.image = UIImage(named: "profile")
                    return
                }
                self._userImageView.image = UIImage(named: "profile")
            }
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
