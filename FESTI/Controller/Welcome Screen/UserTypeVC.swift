
//
//  UserTypeVC.swift
//  FESTI
//
//  Created by salman on 26/09/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class UserTypeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func tapYes(_ sender: Any) {
        let signup = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC")
        self.navigationController?.pushViewController(signup!, animated: true)
    }
    
    @IBAction func tapNo(_ sender: Any) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")
        self.navigationController?.pushViewController(login!, animated: true)
    }
    
}
