//
//  ViewController.swift
//  FESTI
//
//  Created by salman on 26/09/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import AVFoundation

class WelcomeVC: UIViewController {
    
    var pianoSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: "In Abstract Loop", ofType: "mp3")!)
      var audioPlayer = AVAudioPlayer()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: pianoSound as URL)
                   audioPlayer.play()
              } catch {
                 // couldn't load file :(
              } 
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayer.stop()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func tapContinue(_ sender: Any) {
        let userType = self.storyboard?.instantiateViewController(withIdentifier: "UserTypeVC")
        self.navigationController?.pushViewController(userType!, animated: true)
    }
    
}

