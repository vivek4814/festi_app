//
//  PrivacyPolicyVC.swift
//  FESTI
//
//  Created by Salman on 29/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

class PrivacyPolicyVC: UIViewController {

    @IBOutlet weak var _crossImageView: UIImageView!
    @IBOutlet weak var lblTitel: UILabel!
    @IBOutlet weak var txtPrivacy: UITextView!
    
    var headText : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.lblTitel.text = headText
        txtPrivacy.isEditable = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
              _crossImageView.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer?
        = nil) {
        self.dismiss(animated: true, completion: nil)

    }
    

   
}
