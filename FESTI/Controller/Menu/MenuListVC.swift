//
//  MenuListVC.swift
//  FESTI
//
//  Created by salman on 3/18/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit
import Firebase

class MenuListVC: UIViewController {

    @IBOutlet weak var _menuCrossIcon: UIImageView!
    @IBOutlet weak var _userNameLbl: UILabel!
    @IBOutlet weak var _userImageView: UIImageView!
    @IBOutlet weak var _tableView: UITableView!
    
   private var textArray : [String?] = ["Chat List", "Muzes", "Swiping Preferences",  "Rate Us" , "Privacy Policy" , "Terms of Service" , "Contact Us", "Logout" , "Close Account"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.intlizeSetting()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        _menuCrossIcon.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.transform = CGAffineTransform(scaleX: 0.00001, y: 0.00001)
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.view.transform = CGAffineTransform.identity
        })
    }
    
    func intlizeSetting() -> Void {
        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        // self.view.isOpaque = false
        self._tableView.delegate = self
        self._tableView.dataSource = self
        self._tableView.separatorStyle = .none
        _userImageView.layer.cornerRadius = _userImageView.frame.size.width/2
        _userImageView.layer.masksToBounds = true
        print("jjjkkjk",globalUser?.profileImage)
        
        User.getUser { (user) in
                  Storage.storage().reference().child(globalUser?.profileImage ?? "").downloadURL { (url, error) in
                      if error != nil {
                          print(error.debugDescription)
                          return
                      }
                      self._userImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "profile"))
                  }
        }
        
      
        _userNameLbl.text = "\(globalUser?.firstName ?? "") \(globalUser?.lastName ?? "")"
        self.view.backgroundColor = UIColor.clear.withAlphaComponent(0.0)
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      
        self.view.insertSubview(blurEffectView, at: 0)
        
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        let transition: CATransition = CATransition()
               transition.duration = 0.5
               transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
               transition.type = CATransitionType.reveal
               transition.subtype = CATransitionSubtype.fromRight
               self.view.window!.layer.add(transition, forKey: nil)
               self.dismiss(animated: false, completion: nil)
    }
    
    func logout() -> Void {
        
        self.showOkCancelAlertWithAction("You will now be logged out of Muzified") { (isOk) in
            if isOk {
                try! Auth.auth().signOut()
                APP_DELEDATE.logout()
            }
        }

    }
  
   private func navigateToSwippingPrefrence() -> Void {
           let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "SwippingPreferenecVC") as! SwippingPreferenecVC
          self.navigationController?.pushViewController(ctrl, animated: true)
      }
 
   private func navigateToChatRequestVC() -> Void {
       
         let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "ChatRequestVC") as! ChatRequestVC
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
    
    private func navigateToContactUs() -> Void {
            let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
           self.navigationController?.pushViewController(ctrl, animated: true)
       }
    
   private func navigateToChatList() -> Void {
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "ChatUserListVC")
             self.navigationController!.pushViewController(destViewController, animated: true)
    }
    
    private func navigateToPrivacyPolicy(withTitle : String) -> Void {
        let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        ctrl.modalPresentationStyle = .fullScreen
        ctrl.headText = withTitle
        self.present(ctrl, animated: true, completion: nil)
    }
    
    private func navigateToTermsAndConditions(withTitle : String) -> Void {
        let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
        ctrl.modalPresentationStyle = .fullScreen
        ctrl.headText = withTitle
        self.present(ctrl, animated: true, completion: nil)
    }
    
}


extension MenuListVC : UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            
        case 0:
            self.navigateToChatList()
            break
        case 1:
            self.navigateToChatRequestVC()
            break
            
        case 2:
            self.navigateToSwippingPrefrence()
            break
            
        case 4:
            self.navigateToPrivacyPolicy(withTitle: "Privacy Policy")
            break
            
        case 5:
            self.navigateToTermsAndConditions(withTitle: "Terms of Service")
            break
            
        case 6:
            self.navigateToContactUs()
            break
            
        case 7:
            self.logout()
            break
        default:
            break
        }
    }
}

extension MenuListVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.selectionStyle = .none
        cell._menuLbl.text = self.textArray[indexPath.row]
        cell._menuLbl.textColor = (indexPath.row == 7) ? UIColor.red : UIColor.white
        cell.backgroundColor = UIColor.clear
        return cell
        
    }
    
    
}
