//
//  BioScreenVC.swift
//  FESTI
//
//  Created by salman on 03/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class BioScreenVC: UIViewController {

    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var viewBoth: UIView!
    @IBOutlet weak var viewFemale: UIView!
    @IBOutlet weak var viewMale: UIView!
    @IBOutlet weak var btnBoth: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgViewMale: UIImageView!
    @IBOutlet weak var imgViewFemale: UIImageView!
    @IBOutlet weak var imgViewBoth: UIImageView!
    var selectedGender = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSubmit.applyGradient(colors: AppColor.kBLUE_COLOR_GRADIENT)
        txtDescription.delegate = self
    }
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapSubmit(_ sender: Any) {
        if selectedGender == 0 {
            self.showOkAlert("Please select gender.")
            return
        }
        if txtDescription.text == "Write here..."  ||  txtDescription.text == ""{
            self.showOkAlert("Please enter bio.")
        }
        if selectedGender == 1 {
            User.updateUser(data: ["bio": txtDescription.text!, "gender_preference": "male"])
        }
        else if selectedGender == 2 {
            User.updateUser(data: ["bio": txtDescription.text!, "gender_preference": "female"])
        }
        else if selectedGender == 3 {
            User.updateUser(data: ["bio": txtDescription.text!, "gender_preference": "both"])
        }
//        if selectedGender == 1 {
//            let photo = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteArtist") as! FavouriteArtist
//            self.navigationController?.pushViewController(photo, animated: true)
//        }
//        if selectedGender == 2 {
//            let photo = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteMusicVC") as! FavouriteMusicVC
//            self.navigationController?.pushViewController(photo, animated: true)
//        }
//        if selectedGender == 3 {
            let photo = self.storyboard?.instantiateViewController(withIdentifier: "PhotoSelectionVC") as! PhotoSelectionVC
            self.navigationController?.pushViewController(photo, animated: true)
        //}
    }
    
    @IBAction func tapType(_ sender: UIButton) {
        selectedGender = sender.tag
        viewBoth.backgroundColor = AppColor.kDARKGREY_COLOR
        viewFemale.backgroundColor = AppColor.kDARKGREY_COLOR
        viewMale.backgroundColor = AppColor.kDARKGREY_COLOR
        imgViewMale.image = UIImage(named: "unselected")
        imgViewFemale.image = UIImage(named: "unselected")
        imgViewBoth.image = UIImage(named: "unselected")
        
        if sender == btnBoth {
            imgViewBoth.image = UIImage(named: "selected")
            viewBoth.backgroundColor = AppColor.kPINK_COLOR
        }
        if sender == btnFemale {
            imgViewFemale.image = UIImage(named: "selected")
            viewFemale.backgroundColor = AppColor.kPINK_COLOR
        }
        if sender == btnMale {
            imgViewMale.image = UIImage(named: "selected")
            viewMale.backgroundColor = AppColor.kPINK_COLOR
        }
        
    }
    
    

}


extension BioScreenVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text.trim() == "Write here..." {
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trim() == "" {
            textView.text = "Write here..."
        }
    }
}
