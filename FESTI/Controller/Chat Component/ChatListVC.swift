//
//  ChatListVC.swift
//  FESTI
//
//  Created by salman on 10/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class ChatListVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapList(_ sender: Any) {
        let chat = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        self.navigationController?.pushViewController(chat, animated: true)
    }
    
    @IBAction func tapLogo(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //FOR CHAT
    
    /*let uiConfig = ATCChatUIConfiguration(primaryColor: UIColor(hexString: "#0084ff"),
                                          secondaryColor: UIColor(hexString: "#f0f0f0"),
                                          inputTextViewBgColor: UIColor(hexString: "#f4f4f6"),
                                          inputTextViewTextColor: .black,
                                          inputPlaceholderTextColor: UIColor(hexString: "#979797"))
    
        let user = ATCUser(uid: "hZgfkike6tA6cCXHmVVD", firstName: "Salman", lastName: "G", avatarURL: "", email: "salman@gmail.com", isOnline: true)
        let viewer = ATCUser(uid: "18kCj5rt5QeyTGMdpyeCd0FcSLC3", firstName: "abc", lastName: "xyz", avatarURL: "", email: "abc@gmail.com", isOnline: true)
    
        let id1 = (user.uid ?? "")
        let id2 = (viewer.uid ?? "")
        let channelId = "\(id1):\(id2)"
        print("loading thread for channelID: \(channelId)")
        let vc = ATCChatThreadViewController(user: viewer, channel: ATCChatChannel(id: channelId, name: user.fullName()), uiConfig: uiConfig)*/
    
}
