//
//  ProfileDetailVC.swift
//  FESTI
//
//  Created by salman on 07/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import Lightbox
import Firebase
import CoreLocation
import FirebaseStorage

class ProfileDetailVC: UIViewController {
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblFavArtist: UILabel!
    @IBOutlet weak var lblFavMusic: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    var selectedUser : User?
    var arrImages = [String]()
    
    var arrFavMusic = [FavouriteMusicModel]()
    var arrFavArtist = [FavouriteArtistModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrFavMusic = FavouriteMusicModel.getDummyData()
        arrFavArtist = FavouriteArtistModel.getDummyData()
        // Do any additional setup after loading the view.
        self.setIntialValue()
        
    }
    
    
    func setIntialValue() -> Void {
        
        popUpView.isHidden = true
        
        viewTop.layer.masksToBounds = true
        viewTop.layer.cornerRadius = 16
        viewTop.clipsToBounds = true
        
        viewCancel.layer.cornerRadius = 12
        viewCancel.clipsToBounds = true
        viewCancel.layer.masksToBounds = true
        
        Storage.storage().reference().child(selectedUser?.profileImage ?? "").downloadURL { (url, error) in
            if error != nil {
                self.imageUser.image = UIImage(named: "profile")
                print(error.debugDescription)
                return
            }
            self.imageUser.sd_setImage(with: url, placeholderImage: UIImage(named: "profile"))
        }
        
        self.lblUserName.text = "\(selectedUser?.firstName ?? "") \(selectedUser?.lastName ?? "")"
        
        
        self.lblAge.text = "\(selectedUser?.dob?.age ?? 0)"
        var favArts = ""
        
        selectedUser?.favArtistId .forEach({ (item) in
            
            for itemObj in self.arrFavArtist {
                if itemObj.id == item {
                    favArts = favArts+"\(itemObj.userName),"
                    
                }
            }
            
        })
        
        
        
        
        self.lblFavArtist.text = favArts
        
        var favGenre = ""
        
        selectedUser?.favMusicId .forEach({ (item) in
            
            for itemObj in self.arrFavMusic {
                if itemObj.id == item {
                    favGenre = favGenre+"\(itemObj.name),"
                    
                }
            }
            
        })
        
        let coordinate₀ = CLLocation(latitude: curentLat, longitude: curentLong)
        
        if let lat = selectedUser?.lat, let long = selectedUser?.long{
            let coordinate₁ = CLLocation(latitude: ((lat) as NSString).doubleValue, longitude: ((long) as NSString).doubleValue)
            let distanceInMeters = coordinate₀.distance(from: coordinate₁)
            if(distanceInMeters <= 1609){
              let new =  Int(distanceInMeters)
                lblDistance.text = "\(new) meters"
            }
            else if(distanceInMeters >= 1609) {
                let new =  Int(distanceInMeters)
                lblDistance.text = "\(new) mile(s)"
            }
        }else {
            lblDistance.text = "\(0.0) mile(s)"
        }
        
        self.lblFavMusic.text = favGenre
        //self.lblDistance.text = ""
        //self.lblBio.text = user.bio
        self.arrImages.append(self.selectedUser?.galleryImage0 ?? "")
        self.arrImages.append(self.selectedUser?.galleryImage1 ?? "")
        self.arrImages.append(self.selectedUser?.galleryImage2 ?? "")
        self.arrImages.append(self.selectedUser?.galleryImage3 ?? "")
        self.arrImages.append(self.selectedUser?.galleryImage4 ?? "")
        self.arrImages = self.arrImages.filter({ (url) -> Bool in
            return url != ""
        })
        
        collectionView.reloadData()
    }
    
    @IBAction func tapBlock(_ sender: UIButton) {
        
        self.showOkAlert("User has been blocked from viewing your account.")
        popUpView.isHidden = true
        
    }
    
    @IBAction func tapReport(_ sender: UIButton) {
        
        self.showOkAlert("Thank your for your submission.Your request will be reviewed shortly.")
        popUpView.isHidden = true
        
    }
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapMenu(_ sender: UIButton) {
        
        popUpView.isHidden = false
    }
    @IBAction func tapCancel(_ sender: UIButton) {
        popUpView.isHidden = true
        // print("rajaji")
    }
    
    @IBAction func tapEdit(_ sender: Any) {
        let myProfile = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileDetailVC")
        self.navigationController?.pushViewController(myProfile!, animated: true)
    }
}


extension ProfileDetailVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellImage", for: indexPath)
        //        let imgView = cell.viewWithTag(123) as! UIImageView
        //        imgView.image = UIImage(named: "\((indexPath.row+1)*100)")
        //        cell.url = self.arrImages[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellImage", for: indexPath) as! CellImage
        cell.url = self.arrImages[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.height, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var arrImage = [LightboxImage]()
        // let imageUrl = URL.init(string: self.arrImages[indexPath.row])
        Storage.storage().reference().child(self.arrImages[indexPath.row] ).downloadURL { (url, error) in
            if error != nil {
                print(error.debugDescription)
                return
            }
            arrImage.append(LightboxImage(imageURL: url!))
            let controller = LightboxController(images: arrImage, startIndex: indexPath.row)
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
            
        }
        
        //        for number in 1...5 {
        //            arrImage.append(LightboxImage(image: UIImage(named: "\((number)*100)")!))
        //        }
        //        let controller = LightboxController(images: arrImage, startIndex: indexPath.row)
        //        controller.modalPresentationStyle = .fullScreen
        //        self.present(controller, animated: true, completion: nil)
    }
}

