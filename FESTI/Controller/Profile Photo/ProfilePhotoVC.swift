//
//  ProfilePhotoVC.swift
//  FESTI
//
//  Created by salman on 03/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class ProfilePhotoVC: UIViewController {

    @IBOutlet weak var btnContinue: UIButton!
    let picker = SGImagePicker(enableEditing: true)
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinue.applyGradient(colors: AppColor.kPINK_COLOR_GRADIENT)
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapContinue(_ sender: Any) {
        let bio = self.storyboard?.instantiateViewController(withIdentifier: "BioScreenVC") as! BioScreenVC
        self.navigationController?.pushViewController(bio, animated: true)
    }
    
    @IBAction func tapUpload(_ sender: UIButton) {
        self.showAlertWithActions(msg: "Please select an option", titles: ["Camera", "Libray", "Cancel"]) { (selected) in
            if selected == 1 {
                self.picker.getImage(from: .camera) { (image) in
                    sender.setImage(image, for: .normal)
                    sender.imageView?.makeRounded()
                    User.updateProfile(image: image!) { (success) in
                        if success {
                            self.showOkAlertWithHandler("Image successfully updated.") {}
                        }else {
                            self.showOkAlertWithHandler("Something went wrong, Try Again!") {
                            }
                        }
                        
                    }
                }
            }else if selected == 2{
                self.picker.getImage(from: .library) { (image) in
                    sender.setImage(image, for: .normal)
                    sender.imageView?.makeRounded()
                    User.updateProfile(image: image!) { (success) in
                        if success {
                            self.showOkAlertWithHandler("Image successfully updated.") {}
                        }else {
                            self.showOkAlertWithHandler("Something went wrong, Try Again!") {
                            }
                        }
                    }
                }
            }
        }
    }
    

}
