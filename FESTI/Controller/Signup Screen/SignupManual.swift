//
//  SignupManual.swift
//  FESTI
//
//  Created by salman on 30/09/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class SignupManual: UIViewController {
    
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnPrivacy: UIButton!
    @IBOutlet weak var btnAnd: UIButton!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var btnAgree: UIButton!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFisrtName: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var imgViewCheck: UIImageView!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    var isAcceptTerms = false
    let datePicker = UIDatePicker()
    let picker = SGPicker()
    var dob = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCheck.setImage(UIImage(named: "checked"), for: .normal)
        btnAnd.isUserInteractionEnabled = false
        btnRegister.applyGradient(colors: AppColor.kPINK_COLOR_GRADIENT)
        
        self.txtDOB.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
           
      //  txtDOB.inputView = datePicker
//        let screenWidth = UIScreen.main.bounds.width
//        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
//                datePicker.datePickerMode = .date //2
//        txtDOB.inputView = datePicker
//        datePicker.sizeToFit()
//        
//        
//        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
//                let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
//                let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
//                let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: #selector(didPickerChange(_:))) //7
//                toolBar.setItems([cancel, flexible, barButton], animated: false) //8
//                txtDOB.inputAccessoryView = toolBar
        
//        datePicker.datePickerMode = .date
//        if #available(iOS 13.4, *) {
//            datePicker.preferredDatePickerStyle = UIDatePickerStyle.automatic
//        } else {
//            // Fallback on earlier versions
//        }
        
//        var minDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
//        datePicker.maximumDate = minDate
//        minDate = Calendar.current.date(byAdding: .year, value: -80, to: Date())
//        datePicker.minimumDate = minDate
//        datePicker.addTarget(self, action: #selector(didPickerChange(_:)), for: .valueChanged)
        txtGender.inputView = picker.showPicker(datasource: ["Male", "Female", "Other"], completion: { (item) in
            self.txtGender.text = item
        })
        self.txtGender.delegate = self
    }
    
    @objc func didPickerChange(_ sender: UIDatePicker) {
        txtDOB.text = sender.date.toString("dd MMM, yyyy")
        dob = sender.date
    }
    
    @objc func tapCancel() {
            self.resignFirstResponder()
        }
    
    @IBAction func tapCheck(_ sender: UIButton) {
       
        isAcceptTerms = !sender.isSelected
        if sender.isSelected {
            btnCheck.setImage(UIImage(named: "checked"), for: .normal)
        } else {
            btnCheck.setImage(UIImage(named: "check"), for: .normal)
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func tapPrivacy(_ sender: UIButton) {
        self.navigateToPrivacyPolicy(withTitle: "Privacy Policy")
    }
    
    @IBAction func tapTerms(_ sender: UIButton) {
        self.navigateToTerms(withTitle: "Terms of Service")
    }
    
    @IBAction func tapRegister(_ sender: Any) {
        if isValid() {
            User.signup(firstName: txtFisrtName.text!, lastName: txtLastName.text!, gender: txtGender.text!, dob: dob.timeIntervalSince1970, email: txtEmail.text!, pass: txtPassword.text!) { (success) in
                if success {
                    let profile = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoVC")
                    self.navigationController?.pushViewController(profile!, animated: true)
                }
            }
        }
    }
    
    private func navigateToPrivacyPolicy(withTitle : String) -> Void {
        let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        ctrl.modalPresentationStyle = .fullScreen
        ctrl.headText = withTitle
        self.present(ctrl, animated: true, completion: nil)
    }
    
    private func navigateToTerms(withTitle : String) -> Void {
        let ctrl = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
        ctrl.modalPresentationStyle = .fullScreen
        ctrl.headText = withTitle
        self.present(ctrl, animated: true, completion: nil)
    }
    
    @objc func tapDone() {
           if let datePicker = self.txtDOB.inputView as? UIDatePicker {
               let dateformatter = DateFormatter()
               dateformatter.dateStyle = .medium
               self.txtDOB.text = dateformatter.string(from: datePicker.date) //2-4
           }
           self.txtDOB.resignFirstResponder()
       }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapAccept(_ sender: UIButton) {
        
    }
    
    func isValid() -> Bool {
        if txtFisrtName.text?.trim() == "" {
            self.showOkAlertWithHandler("Please enter your first name.") {
                self.txtFisrtName.becomeFirstResponder()
            }
            return false
        }
        if txtLastName.text?.trim() == "" {
            self.showOkAlertWithHandler("Please enter your last name.") {
                self.txtLastName.becomeFirstResponder()
            }
            return false
        }
        if txtDOB.text?.trim() == "" {
            self.showOkAlertWithHandler("Please enter your dob.") {
                self.txtLastName.becomeFirstResponder()
            }
            return false
        }
        
        if txtGender.text?.trim() == "" {
            self.showOkAlertWithHandler("Please enter choose your gender") {
                self.txtGender.becomeFirstResponder()
            }
            return false
        }
        
        if txtEmail.text?.trim() == "" {
            self.showOkAlertWithHandler("Please enter your email.") {
                self.txtEmail.becomeFirstResponder()
            }
            return false
        }
        if !SGHelper.isValidEmail(txtEmail.text?.trim() ?? "") {
            self.showOkAlertWithHandler("Please enter your valid email.") {
                self.txtEmail.becomeFirstResponder()
            }
            return false
        }
        if txtPassword.text?.trim() == "" {
            self.showOkAlertWithHandler("Please enter your password.") {
                self.txtPassword.becomeFirstResponder()
            }
            return false
        }
        if isAcceptTerms {
            self.showOkAlertWithHandler("Before you can proceed, you must accept our Terms of Service and Privacy Policy.") {
            }
            return false
        }
        
        return true
    }
}

extension SignupManual: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtGender && textField.text == "" {
            self.txtGender.text = "Male"
        }
    }
}

extension UITextField {
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 1000))//1
        datePicker.datePickerMode = .date
        
        if #available(iOS 14, *) {
            datePicker.preferredDatePickerStyle = .wheels
          datePicker.sizeToFit()
        }
        self.inputView = datePicker
//        var minDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
//               datePicker.maximumDate = minDate
//               minDate = Calendar.current.date(byAdding: .year, value: -80, to: Date())
//               datePicker.minimumDate = minDate
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        toolBar.setItems([cancel, flexible, barButton], animated: false)
        self.inputAccessoryView = toolBar
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
}
