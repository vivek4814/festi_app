//
//  SignupVC.swift
//  FESTI
//
//  Created by salman on 26/09/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FacebookLogin
import FacebookCore
import FirebaseAuth
import FBSDKLoginKit


class FirebaseAuthManager {

    func login(credential: AuthCredential, completionBlock: @escaping (_ success: Bool) -> Void) {
        Auth.auth().signIn(with: credential, completion: { (firebaseUser, error) in
            print(firebaseUser)
            completionBlock(error == nil)
        })
    }

    func createUser(email: String, password: String, completionBlock: @escaping (_ success: Bool) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) {(authResult, error) in
            if let user = authResult?.user {
                print(user)
                completionBlock(true)
            } else {
                completionBlock(true)
            }
        }
    }
    
    func signIn(email: String, pass: String, completionBlock: @escaping (_ success: Bool) -> Void) {
        Auth.auth().signIn(withEmail: email, password: pass) { (result, error) in
            if let error = error, let _ = AuthErrorCode(rawValue: error._code) {
                completionBlock(false)
            } else {
                print(result)
                completionBlock(true)
            }
        }
    }
}


class SignupVC: UIViewController {
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnManual: UIButton!
    
    private let readPermissions: [Permission] = [ .publicProfile, .email, .userFriends, .custom("user_posts") ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnFacebook.applyGradient(colors: AppColor.kBLUE_COLOR_GRADIENT)
        btnManual.applyGradient(colors: AppColor.kPINK_COLOR_GRADIENT)
    }
    
    @IBAction func tapWithEmail(_ sender: Any) {
        let signup = self.storyboard?.instantiateViewController(withIdentifier: "SignupManual")
        self.navigationController?.pushViewController(signup!, animated: true)
    }
    
    @IBAction func tapOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapFb(_ sender: Any) {
    
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["email"], from: self) { (result, error) in

            print("\n\n result: \(String(describing: result))")
            print("\n\n Error: \(String(describing: error))")

            if (error == nil) {
                if(result?.isCancelled ?? false) {
                    SwiftLoader.hide()
                    //Show Cancel alert
                } else if(result?.grantedPermissions.contains("email") ?? true) {
                    self.returnUserData()
                  // LoginManager().logOut()
                }
            }else {
                SwiftLoader.hide()
            }
         // self.returnUserData()
        }
    }
    
    
    func login(credential: AuthCredential, completionBlock: @escaping (_ success: Bool) -> Void) {
        Auth.auth().signIn(with: credential, completion: { (firebaseUser, error) in
            print(firebaseUser)
            completionBlock(error == nil)
        })
    }
    
    @objc func didTapFacebookButton() {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: readPermissions, viewController: self, completion: didReceiveFacebookLoginResult)
    }
    
    func returnUserData() {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"email,name,id,first_name,gender,birthday,last_name"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil) {
                // Process error
                print("\n\n Error: \(String(describing: error) )")
            } else {
                guard let accessToken = AccessToken.current else {
                    print("Failed to get access token")
                    SwiftLoader.hide()
                    return
                }
                let resultDic = result as! [String : Any]
                print("fetched user",resultDic)
                let email = resultDic["email"] as? String ?? ""
                let first_name = resultDic["first_name"] as? String ?? ""
                let last_name = resultDic["last_name"] as? String ?? ""
                var birthday = ""
                if let _birthday = resultDic["birthday"] as? String {
                    birthday = _birthday
                }
                let facebookID = resultDic["id"] as! String
                if birthday == "" {
                }else {
                    print("birthday",birthday, birthday.toDate(withFormat: "dd/MM/yyyy"))
                }
                let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
                User.SignUpWithFacebook(faceBookToken: credential, firstName: first_name, lastName: last_name, gender: "", dob: (birthday == "") ? 0.0 : birthday.toDate(withFormat: "dd/MM/yyyy").timeIntervalSince1970, email: email) { (success) in
                    if success {
                        let profile = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePhotoVC")
                        self.navigationController?.pushViewController(profile!, animated: true)
                    }
                }
            }
        })
    }
    
    private func didReceiveFacebookLoginResult(loginResult: LoginResult) {
        switch loginResult {
        case .success:
            didLoginWithFacebook()
        case .failed(_): break
        default: break
        }
    }

    fileprivate func didLoginWithFacebook() {
        // Successful log in with Facebook
        if let accessToken = AccessToken.current {
            // If Firebase enabled, we log the user into Firebase
        }
    }
}

extension String {
    func toDate(withFormat format: String = "dd/MM/yyyy") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        guard let date = dateFormatter.date(from: self) else {
            preconditionFailure("Take a look to your format")
        }
        return date
    }
}
