
App Thinning Size Report for All Variants of Runner

Variant: Runner-02218413-3FF1-44F1-9151-7CCC2045E8BC.ipa
Supported variant descriptors: [device: iPad6,8, os-version: 9.0], [device: iPad4,7, os-version: 9.0], [device: iPad5,1, os-version: 9.0], [device: iPad6,12, os-version: 10.0], [device: iPad4,3, os-version: 9.0], [device: iPad7,4, os-version: 10.0], [device: iPad7,2, os-version: 10.0], [device: iPad6,12, os-version: 11.0], [device: iPad7,6, os-version: 11.0], [device: iPad6,11, os-version: 10.0], [device: iPad6,11, os-version: 11.0], [device: iPad6,4, os-version: 9.0], [device: iPad4,1, os-version: 9.0], [device: iPad5,4, os-version: 9.0], [device: iPad4,4, os-version: 9.0], [device: iPad7,1, os-version: 11.0], [device: iPad7,3, os-version: 11.0], [device: iPad5,3, os-version: 9.0], [device: iPad7,2, os-version: 11.0], [device: iPad7,3, os-version: 10.0], [device: iPad7,5, os-version: 11.0], [device: iPad6,7, os-version: 9.0], [device: iPad4,9, os-version: 9.0], [device: iPad7,1, os-version: 10.0], [device: iPad6,3, os-version: 9.0], [device: iPad4,6, os-version: 9.0], [device: iPad4,8, os-version: 9.0], [device: iPad4,2, os-version: 9.0], [device: iPad4,5, os-version: 9.0], [device: iPad5,2, os-version: 9.0], and [device: iPad7,4, os-version: 11.0]
App + On Demand Resources size: 17.8 MB compressed, 42.6 MB uncompressed
App size: 17.8 MB compressed, 42.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-47519624-6E84-4DF8-8ECA-6B01A4148EA1.ipa
Supported variant descriptors: [device: iPad11,4, os-version: 12.2], [device: iPad5,3, os-version: 12.2], [device: iPad6,12, os-version: 12.2], [device: iPad4,2, os-version: 12.2], [device: iPad6,11, os-version: 12.2], [device: iPad4,8, os-version: 12.2], [device: iPad6,8, os-version: 12.2], [device: iPad6,7, os-version: 12.2], [device: iPad11,2, os-version: 12.2], [device: iPad11,3, os-version: 12.2], [device: iPad6,4, os-version: 12.2], [device: iPad4,5, os-version: 12.2], [device: iPad4,7, os-version: 12.2], [device: iPad11,1, os-version: 12.2], [device: iPad7,4, os-version: 12.2], [device: iPad5,2, os-version: 12.2], [device: iPad7,3, os-version: 12.2], [device: iPad7,6, os-version: 12.2], [device: iPad4,1, os-version: 12.2], [device: iPad4,9, os-version: 12.2], [device: iPad8,4, os-version: 12.2], [device: iPad7,2, os-version: 12.2], [device: iPad8,8, os-version: 12.2], [device: iPad6,3, os-version: 12.2], [device: iPad8,2, os-version: 12.2], [device: iPad8,6, os-version: 12.2], [device: iPad8,3, os-version: 12.2], [device: iPad4,3, os-version: 12.2], [device: iPad4,6, os-version: 12.2], [device: iPad7,1, os-version: 12.2], [device: iPad7,5, os-version: 12.2], [device: iPad8,1, os-version: 12.2], [device: iPad4,4, os-version: 12.2], [device: iPad5,4, os-version: 12.2], [device: iPad8,5, os-version: 12.2], [device: iPad5,1, os-version: 12.2], and [device: iPad8,7, os-version: 12.2]
App + On Demand Resources size: 15.3 MB compressed, 34.9 MB uncompressed
App size: 15.3 MB compressed, 34.9 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-4A1E746B-13D4-4176-A167-E79DEE6499CF.ipa
Supported variant descriptors: [device: iPhone11,8, os-version: 12.0]
App + On Demand Resources size: 17.8 MB compressed, 42.5 MB uncompressed
App size: 17.8 MB compressed, 42.5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-6D92BA24-A6E5-450C-B61A-7DCE92EA60F2.ipa
Supported variant descriptors: [device: iPad3,5, os-version: 9.0], [device: iPad3,6, os-version: 9.0], [device: iPad3,1, os-version: 9.0], [device: iPad3,4, os-version: 9.0], [device: iPad3,3, os-version: 9.0], and [device: iPad3,2, os-version: 9.0]
App + On Demand Resources size: 18.2 MB compressed, 38.1 MB uncompressed
App size: 18.2 MB compressed, 38.1 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-6DF3C7A3-E6EB-4022-BF1E-0DB54253737F.ipa
Supported variant descriptors: [device: iPhone11,2, os-version: 12.0], [device: iPhone10,5, os-version: 12.0], [device: iPhone9,2, os-version: 12.0], [device: iPhone10,3, os-version: 12.0], [device: iPhone11,4, os-version: 12.0], [device: iPhone9,4, os-version: 12.0], [device: iPhone10,6, os-version: 12.0], [device: iPhone7,1, os-version: 12.0], [device: iPhone11,6, os-version: 12.0], [device: iPhone8,2, os-version: 12.0], and [device: iPhone10,2, os-version: 12.0]
App + On Demand Resources size: 17.6 MB compressed, 42.4 MB uncompressed
App size: 17.6 MB compressed, 42.4 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-AA60FAD6-5D99-4A27-993A-925195BB85F5.ipa
Supported variant descriptors: [device: iPhone10,3, os-version: 11.0], [device: iPhone10,5, os-version: 11.0], [device: iPhone9,2, os-version: 10.0], [device: iPhone8,2, os-version: 9.0], [device: iPhone9,4, os-version: 10.0], [device: iPhone9,4, os-version: 11.0], [device: iPhone10,2, os-version: 11.0], [device: iPhone9,2, os-version: 11.0], [device: iPhone10,6, os-version: 11.0], and [device: iPhone7,1, os-version: 9.0]
App + On Demand Resources size: 17.6 MB compressed, 42.4 MB uncompressed
App size: 17.6 MB compressed, 42.4 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-BA8586DB-FED6-4B8C-B07E-73BCD7DC2B56.ipa
Supported variant descriptors: [device: iPad2,3, os-version: 9.0], [device: iPad2,1, os-version: 9.0], [device: iPad2,2, os-version: 9.0], [device: iPad2,7, os-version: 9.0], [device: iPad2,5, os-version: 9.0], [device: iPad2,4, os-version: 9.0], and [device: iPad2,6, os-version: 9.0]
App + On Demand Resources size: 18.2 MB compressed, 38.1 MB uncompressed
App size: 18.2 MB compressed, 38.1 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-BB50A9E3-556E-4148-9613-ADA27749CC65.ipa
Supported variant descriptors: [device: iPad4,4, os-version: 12.0], [device: iPad7,2, os-version: 12.0], [device: iPad4,6, os-version: 12.0], [device: iPad5,2, os-version: 12.0], [device: iPad8,5, os-version: 12.0], [device: iPad7,5, os-version: 12.0], [device: iPad8,7, os-version: 12.0], [device: iPad8,4, os-version: 12.0], [device: iPad7,3, os-version: 12.0], [device: iPad6,3, os-version: 12.0], [device: iPad8,6, os-version: 12.0], [device: iPad8,3, os-version: 12.0], [device: iPad5,4, os-version: 12.0], [device: iPad6,4, os-version: 12.0], [device: iPad4,3, os-version: 12.0], [device: iPad4,9, os-version: 12.0], [device: iPad8,8, os-version: 12.0], [device: iPad4,1, os-version: 12.0], [device: iPad6,7, os-version: 12.0], [device: iPad7,4, os-version: 12.0], [device: iPad4,8, os-version: 12.0], [device: iPad4,2, os-version: 12.0], [device: iPad5,1, os-version: 12.0], [device: iPad5,3, os-version: 12.0], [device: iPad6,11, os-version: 12.0], [device: iPad8,1, os-version: 12.0], [device: iPad8,2, os-version: 12.0], [device: iPad7,1, os-version: 12.0], [device: iPad4,5, os-version: 12.0], [device: iPad4,7, os-version: 12.0], [device: iPad6,8, os-version: 12.0], [device: iPad6,12, os-version: 12.0], and [device: iPad7,6, os-version: 12.0]
App + On Demand Resources size: 17.8 MB compressed, 42.5 MB uncompressed
App size: 17.8 MB compressed, 42.5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-D6599EAF-0BA7-4DA5-AA3D-649C368D045F.ipa
Supported variant descriptors: [device: iPhone8,1, os-version: 12.2], [device: iPod7,1, os-version: 12.2], [device: iPhone10,4, os-version: 12.2], [device: iPhone9,3, os-version: 12.2], [device: iPhone8,4, os-version: 12.2], [device: iPhone6,1, os-version: 12.2], [device: iPhone7,2, os-version: 12.2], [device: iPhone9,1, os-version: 12.2], [device: iPhone6,2, os-version: 12.2], and [device: iPhone10,1, os-version: 12.2]
App + On Demand Resources size: 15.3 MB compressed, 34.9 MB uncompressed
App size: 15.3 MB compressed, 34.9 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-E668ACF0-3877-4AC5-AB5E-60A2983FDB17.ipa
Supported variant descriptors: [device: iPhone11,8, os-version: 12.2]
App + On Demand Resources size: 15.3 MB compressed, 35 MB uncompressed
App size: 15.3 MB compressed, 35 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-EA7C7AA1-2038-4D1E-B96B-7784FAD16495.ipa
Supported variant descriptors: [device: iPhone9,2, os-version: 12.2], [device: iPhone10,5, os-version: 12.2], [device: iPhone8,2, os-version: 12.2], [device: iPhone10,6, os-version: 12.2], [device: iPhone11,2, os-version: 12.2], [device: iPhone9,4, os-version: 12.2], [device: iPhone11,6, os-version: 12.2], [device: iPhone10,3, os-version: 12.2], [device: iPhone11,4, os-version: 12.2], [device: iPhone10,2, os-version: 12.2], and [device: iPhone7,1, os-version: 12.2]
App + On Demand Resources size: 15.2 MB compressed, 34.8 MB uncompressed
App size: 15.2 MB compressed, 34.8 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-EF18657F-FC05-49AD-B2F5-687F56BBF992.ipa
Supported variant descriptors: [device: iPod5,1, os-version: 9.0], [device: iPhone5,1, os-version: 9.0], [device: iPhone5,2, os-version: 9.0], [device: iPhone5,3, os-version: 9.0], [device: iPhone4,1, os-version: 9.0], and [device: iPhone5,4, os-version: 9.0]
App + On Demand Resources size: 18.2 MB compressed, 38.1 MB uncompressed
App size: 18.2 MB compressed, 38.1 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-F0213355-46B4-432F-B17C-052BB0BA8FAA.ipa
Supported variant descriptors: [device: iPhone6,2, os-version: 9.0], [device: iPhone10,1, os-version: 11.0], [device: iPhone9,1, os-version: 11.0], [device: iPhone7,2, os-version: 9.0], [device: iPhone8,1, os-version: 9.0], [device: iPod7,1, os-version: 9.0], [device: iPhone9,1, os-version: 10.0], [device: iPhone6,1, os-version: 9.0], [device: iPhone9,3, os-version: 10.0], [device: iPhone10,4, os-version: 11.0], [device: iPhone9,3, os-version: 11.0], and [device: iPhone8,4, os-version: 9.0]
App + On Demand Resources size: 17.8 MB compressed, 42.6 MB uncompressed
App size: 17.8 MB compressed, 42.6 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner-F4F1B038-FA5A-4D59-974D-3A5A2E7A7A88.ipa
Supported variant descriptors: [device: iPhone8,4, os-version: 12.0], [device: iPhone10,1, os-version: 12.0], [device: iPhone10,4, os-version: 12.0], [device: iPhone6,2, os-version: 12.0], [device: iPhone9,3, os-version: 12.0], [device: iPhone9,1, os-version: 12.0], [device: iPhone6,1, os-version: 12.0], [device: iPhone8,1, os-version: 12.0], [device: iPod7,1, os-version: 12.0], and [device: iPhone7,2, os-version: 12.0]
App + On Demand Resources size: 17.8 MB compressed, 42.5 MB uncompressed
App size: 17.8 MB compressed, 42.5 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Runner.ipa
Supported variant descriptors: Universal
App + On Demand Resources size: 30 MB compressed, 72.8 MB uncompressed
App size: 30 MB compressed, 72.8 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed
