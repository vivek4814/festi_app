//
//  ChatRequestVC.swift
//  FESTI
//
//  Created by salman on 03/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import Firebase

class ChatRequestVC: UIViewController , UISearchBarDelegate  {
    private let manager = ConversationManager()
    private var conversations = [ObjectConversation]()
    
    var arrFav = [ObjectConversation]()
    let userID = UserManager().currentUserID() ?? ""
    var filterArrFav = [ObjectConversation]()

    @IBOutlet weak var searchbar: UISearchBar!
    var arrRequest = [RequestModel]()
    @IBOutlet weak var tblView: UITableView!
    var filterdata = [Any]()
    var isFiltered : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchbar.delegate = self
        if let textfield = searchbar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.1215686275, blue: 0.1215686275, alpha: 1)
            textfield.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            textfield.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        filterdata = conversations
        RequestModel.getAllMyRequest { (allReq) in
            if allReq == nil {
                return
            }
            self.arrRequest = allReq!
            self.tblView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //  guard let id = conversation.userIDs.filter({$0 != userID}).first else { return }
        guard let id = conversations.first?.userIDs.filter({$0 != userID}).first else { return }
        self.filterdata = []
        User.getUserById(userId: id) { (_user) in
            let new = "\(_user?.firstName ?? "") \(_user?.lastName ?? "")"
            if searchText == "" {
                self.filterdata = self.conversations
            }else{
                if new.lowercased().contains(searchText.lowercased()){
                    self.filterdata.append(new)
                }
            }
            self.tblView.reloadData()
        }
    }
    
    func applyFilter(_text : String) -> Void {
        self.arrRequest.removeAll()
        self.filterArrFav  = arrFav.filter{(x) -> Bool in
            (x.id.lowercased().range(of: _text.lowercased()) != nil)
        }
        if self.filterArrFav.count > 0 {
            isFiltered = true
            print("applyFilter:-\(self.filterArrFav.count)")
        }else {
            isFiltered = false
        }
        tblView.reloadData()
    }
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        //  guard let id = conversation.userIDs.filter({$0 != userID}).first else { return }
//        guard let id = conversations.first?.userIDs.filter({$0 != userID}).first else { return }
//        self.filterdata = []
//        User.getUserById(userId: id) { (_user) in
//            let new = "\(_user?.firstName ?? "") \(_user?.lastName ?? "")"
//            if searchText == "" {
//                self.filterdata = self.conversations
//            }else{
//                if new.lowercased().contains(searchText.lowercased()){
//                    self.filterdata.append(new)
//                }
//            }
//            self._tableView.reloadData()
//        }
//    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapLogout(_ sender: Any) {
        self.showOkCancelAlertWithAction("You will now be logged out of Muzified") { (isOk) in
            if isOk {
                try! Auth.auth().signOut()
                APP_DELEDATE.logout()
            }
        }
    }
}

extension ChatRequestVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellChatReq", for: indexPath) as! CellChatReq
        cell.info = arrRequest[indexPath.row]
        cell.vc = self
        cell.cellChatReqDelegate = self
        print("info:-\(arrRequest[indexPath.row].fromId) ::::\(arrRequest[indexPath.row].toId)")
        print("globel::::-\(globalUser?.uid)")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetailVC") as! ProfileDetailVC
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
//    @available(iOS 11.0, *)
//    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let closeAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
//            print("reject action")
//            success(true)
//        })
//        print(UIImage(named: "ok"))
//        closeAction.image = UIImage(named: "ok")
////        UIImageView.appearance( whenContainedInInstancesOf: [UITableView.self]).tintColor = .white
//        closeAction.backgroundColor = UIColor.black.withAlphaComponent(0.1)
//        return UISwipeActionsConfiguration(actions: [closeAction])
//
//    }
//
//    @available(iOS 11.0, *)
//    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//        let closeAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
//            print("accept action")
//            success(true)
//        })
//        closeAction.image = UIImage(named: "no")
//        UIImageView.appearance( whenContainedInInstancesOf: [UITableView.self]).tintColor = .white
//        closeAction.backgroundColor = UIColor.black.withAlphaComponent(0.1)
//        return UISwipeActionsConfiguration(actions: [closeAction])
//    }
}

extension ChatRequestVC : CellChatReqDeleagte {
    func getAcceptAction(requsetModel: RequestModel?) {
        
        let vc: MessageVC = UIStoryboard.initial(storyboard: .messages)
        
        if let conversation = self.conversations.filter({$0.userIDs.contains(requsetModel?.toId ?? "")}).first {
            vc.conversation = conversation
            let backItem = UIBarButtonItem()
            backItem.title = ""
            self.navigationItem.backBarButtonItem = backItem
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        let conversation = ObjectConversation()
        conversation.userIDs.append(contentsOf: [requsetModel?.toId ?? "", requsetModel?.fromId ?? ""])
        conversation.isRead = [requsetModel?.toId ?? "": true, requsetModel?.fromId ?? "": true]
        vc.conversation = conversation
        vc.classPrefix = "chatRequest"
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.navigationItem.backBarButtonItem = backItem
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension ChatRequestVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            self.applyFilter(_text: updatedText)
            debugPrint("updatedText:-\(updatedText)")
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
