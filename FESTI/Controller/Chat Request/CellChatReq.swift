//
//  CellChatReq.swift
//  FESTI
//
//  Created by salman on 10/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit
import Firebase
import MessageKit
import FirebaseStorage
import FirebaseFirestore


protocol CellChatReqDeleagte {
    func getAcceptAction(requsetModel : RequestModel?)
}
class CellChatReq: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnrReject: UIButton!
    var vc: ChatRequestVC?
    
    var cellChatReqDelegate : CellChatReqDeleagte?
    
    
    var info: RequestModel? {
        didSet {
            lblName.text = (info?.fromFirstName ?? "")+" "+(info?.fromLastName ?? "")
            Storage.storage().reference().child(info?.fromImage ?? "").downloadURL { (url, error) in
                if error != nil {
                    print(error.debugDescription)
                    return
                }
                self.imgView.sd_setImage(with: url, placeholderImage: UIImage(named: "profile"))
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func tapAccept(_ sender: Any) {
        
        btnAccept.isHidden = true
        btnrReject.isHidden = true
        cellChatReqDelegate?.getAcceptAction(requsetModel: info)
        
        /*let uiConfig = ATCChatUIConfiguration(primaryColor: UIColor(hexString: "#0084ff"),
                                                 secondaryColor: UIColor(hexString: "#f0f0f0"),
                                                 inputTextViewBgColor: UIColor(hexString: "#f4f4f6"),
                                                 inputTextViewTextColor: .black,
                                                 inputPlaceholderTextColor: UIColor(hexString: "#979797"))
           
               let user = ATCUser(uid: "hZgfkike6tA6cCXHmVVD", firstName: "Salman", lastName: "G", avatarURL: "", email: "salman@gmail.com", isOnline: true)
               let viewer = ATCUser(uid: "18kCj5rt5QeyTGMdpyeCd0FcSLC3", firstName: "abc", lastName: "xyz", avatarURL: "", email: "abc@gmail.com", isOnline: true)
        let vc = ATCChatThreadViewController(user: viewer, channel: ATCChatChannel(id: channelId, name: user.fullName()), uiConfig: uiConfig)
         
         */
//        let id1 = (info?.toId ?? "")
//        let id2 = (info?.fromId ?? "")
//        let channelId = "\(id1):\(id2)"

        
//        print("globalUser?.uid:-\(globalUser?.uid) :: info?.fromId:-\(info?.fromId)")
//        
//        let user = ATCUser(uid: globalUser?.uid ?? "", firstName: globalUser?.firstName ?? "", lastName: globalUser?.firstName ?? "", avatarURL: globalUser?.profileImage ?? "", email: globalUser?.email ?? "", isOnline: true,senderId : "", reciverId : "")
//        
//        let recipient = ATCUser(uid: info?.fromId ?? "", firstName: info?.fromFirstName ?? "", lastName: info?.fromLastName ?? "", avatarURL: info?.fromImage ?? "", email: info?.fromEmail ?? "", isOnline: false,senderId : "", reciverId : "")
//        
//        let channel = ATCChatChannel(id: channelId, name: user.fullName())
//          let reference = Firestore.firestore().collection(["channels", channelId, "thread"].joined(separator: "/"))
//                ATCRemoteData().checkPath(path: ["channels", channelId, "thread"], dbRepresentation: channel.representation)
//        
//        let message = ATChatMessage(messageId: UUID().uuidString,messageKind: MessageKind.text("We are now connected."),createdAt: Date(),atcSender: user,recipient: recipient,seenByRecipient: false)
//        
//        reference.addDocument(data: message.representation) { error in
//            if let e = error {
//                print("Error sending message: \(e.localizedDescription)")
//                return
//            }else {
//                print("loading thread for channelID: \(channelId)")
//                print(message.representation)
//            }
//        }
        
               
    }
    
    @IBAction func tapReject(_ sender: Any) {
        if let index = vc?.tblView.indexPath(for: self) {
            vc?.arrRequest.remove(at: index.row)
            vc?.tblView.deleteRows(at: [index], with: .left)
        }
    }
}
