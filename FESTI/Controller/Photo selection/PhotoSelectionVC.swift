//
//  PhotoSelectionVC.swift
//  FESTI
//
//  Created by salman on 01/10/19.
//  Copyright © 2019 Salman. All rights reserved.
//

import UIKit

class PhotoSelectionVC: UIViewController {

    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var collectionPhotos: UICollectionView!
    var arrImages = [UIImage?]()
    let picker = SGImagePicker(enableEditing: true)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinue.applyGradient(colors: AppColor.kPINK_COLOR_GRADIENT)
        for _ in 1...5 {
            arrImages.append(nil)
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapContinue(_ sender: Any) {
        
        let photo = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteArtist") as! FavouriteArtist
        self.navigationController?.pushViewController(photo, animated: true)
    }
    
  
}

extension PhotoSelectionVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2, height: collectionView.frame.size.width/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPhotos", for: indexPath)
        let imgView = cell.viewWithTag(120) as! UIImageView
        let imgViewCamera = cell.viewWithTag(140) as! UIImageView
        if arrImages[indexPath.row] != nil {
            imgViewCamera.isHidden = true
        } else {
            imgViewCamera.isHidden = false
        }
        imgView.image = arrImages[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            self.showAlertWithActions(msg: "Please select an option", titles: ["Camera", "Libray", "Cancel"]) { (selected) in
                let imgView = cell.viewWithTag(120) as! UIImageView
                if selected == 1 {
                    self.picker.getImage(from: .camera) { (image) in
                        imgView.image = image
                        self.arrImages[indexPath.row] = image
                        User.updateGalleryPhoto(image: image!, atIndex: indexPath.row) { (success) in
                            print("")
                        }
                        imgView.makeRoundCorner(10)
                        collectionView.reloadData()
                    }
                }else if selected == 2 {
                    self.picker.getImage(from: .library) { (image) in
                        imgView.image = image
                        User.updateGalleryPhoto(image: image!, atIndex: indexPath.row) { (success) in
                            print("")
                        }
                        self.arrImages[indexPath.row] = image
                        imgView.makeRoundCorner(10)
                        collectionView.reloadData()
                    }
                }
            }
        }
    }
}
