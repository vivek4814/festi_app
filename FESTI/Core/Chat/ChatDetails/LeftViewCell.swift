//
//  LeftViewCell.swift
//  ChatSample
//
//  Created by Salman on 29/06/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

class LeftViewCell: UITableViewCell {

    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var timeLbl: UILabel!
   // @IBOutlet weak var messageContainerView: BubbleView!
    @IBOutlet weak var textMessageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      messageContainerView.rounded(radius: 12)
       // messageContainerView.backgroundColor =  UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1)
        
        messageContainerView.backgroundColor =  UIColor.white
        
        
        bubbleImageView.image = UIImage.init(named: "chat_bubble_received")!
                         .resizableImage(withCapInsets:
                           UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
                                                 resizingMode: .stretch)
                         .withRenderingMode(.alwaysTemplate)
               bubbleImageView.tintColor = UIColor(named: "chat_bubble_color_received")
        
//        contentView.backgroundColor = .clear
//        backgroundColor = .clear


    }
    
    func configureCell(message: ObjectMessage) {
        //messageContainerView.isIncoming = true
              timeLbl.text = DateService.shared.format(Date(timeIntervalSince1970: TimeInterval(message.timestamp )))
        textMessageLabel.text = message.message
    }
    
}
