//
//  RightViewCell.swift
//  ChatSample
//
//  Created by Salman on 29/06/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

class RightViewCell: UITableViewCell {
    //@IBOutlet weak var messageContainerView: BubbleView!
    @IBOutlet weak var messageContainerView: UIView!
    
    @IBOutlet weak var bubbleImageView: UIImageView!
    @IBOutlet weak var textMessageLabel: UILabel!
    
    @IBOutlet weak var timeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
      // messageContainerView.rounded(radius: 12)
     // messageContainerView.backgroundColor = UIColor(hexString: "E1F7CB")
        messageContainerView.backgroundColor = UIColor.white
        bubbleImageView.image = UIImage.init(named: "chat_bubble_sent")!
                  .resizableImage(withCapInsets:
                    UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
                                          resizingMode: .stretch)
                  .withRenderingMode(.alwaysTemplate)
        bubbleImageView.tintColor = UIColor(named: "chat_bubble_color_sent")

          
    
      //  messageContainerView.backgroundColor = UIColor.white

        
//        contentView.backgroundColor = .clear
//        backgroundColor = .clear
      
        
    }
    

    
    
 
    
    func configureCell(message: ObjectMessage) {
        
       timeLbl.text = DateService.shared.format(Date(timeIntervalSince1970: TimeInterval(message.timestamp )))
        textMessageLabel.text = message.message
    }
    
    
}



extension UIView {
    func rounded(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}



