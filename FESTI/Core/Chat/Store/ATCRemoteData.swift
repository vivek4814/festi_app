//
//  ATCRemoteData.swift
//  ChatApp
//
//  Created by Dan Burkhardt on 3/20/19.
//  Copyright © 2019 Instamobile. All rights reserved.
//

import Foundation
import Firebase
import MessageKit
import MessageKit
import FirebaseStorage
import FirebaseFirestore
import FirebaseDatabase

/// Sample class that gets all channels from the remote data store.
class ATCRemoteData{
    let db = Firestore.firestore()
     let dbRef = Database.database().reference()
    let serialQueue = DispatchQueue(label: "com.queue.Serial")
    let semaphore = DispatchSemaphore(value: 0)

    func getChannels(handler: @escaping (_ allUsers: [ATCUser]) -> Void, thereadHandler: @escaping (_ allUsers: [ATChatMessage]) -> Void) {
        print("getting all channels")
        var users = [ATCUser]()
        var allusers = [ATCUser]()
        
        var thereads = [ATChatMessage]()
      
            db.collection("users").getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Firebase returned an error while getting chat documents: \(err)")
                } else {
                    if querySnapshot?.documents == nil{
                        print("no channels or threads found for this user's organization\n. No worries a brand new one will automatically be created when you first attempt to send a message")
                    }else{
                        // Uncomment to see all documents in this user's org
                        // Usually a bad thing though, only use to debug and do not release
                        for document in querySnapshot!.documents {
                        
                            allusers.append(ATCUser(uid: document.documentID, firstName: document.data()["first_name"] as? String ?? "", lastName: document.data()["last_name"] as? String ?? "", avatarURL: document.data()["profile"] as? String ?? "", email: document.data()["email"] as? String ?? "", isOnline: false ,senderId : "", reciverId : "") )
                            
                            
                            // print("\(document.documentID) => \(document.data())")
                        }
                        
                        
                        self.db.collection("channels").getDocuments() { (querySnapshot, err) in
                            if let err = err {
                                print("Firebase returned an error while getting chat documents: \(err)")
                            } else {
                                if querySnapshot?.documents == nil{
                                    print("no channels ")
                                }else{
                                    // Uncomment to see all documents in this user's org
                                    // Usually a bad thing though, only use to debug and do not release
                                    for document in querySnapshot!.documents {
                                        print("\(document.documentID) => \(document.data())")
                                        let fullNameArr = document.documentID.split{$0 == ":"}.map(String.init)
                                        
                                        print("fullNameArr",fullNameArr)
                                        if document.documentID == "dan:sandra"  || document.documentID == "dan:simulator"{
                                            
                                        }else {
                                            
                                          
                                            users.append(ATCUser(uid: document.documentID, firstName: document.data()["name"] as! String, lastName: "", avatarURL: "https://cdn1.iconfinder.com/data/icons/avatar-2-2/512/Programmer-512.png", email: "", isOnline: false ,senderId : fullNameArr[1], reciverId : fullNameArr[0]))
                                            
                                        }
                                    }
                                    
                                    
                                    if users.count < 2{
                                        return
                                    }
                                    print("com.queue.Serial",users.count)
                                    var count = 0
                                    
                                    self.serialQueue.async {
                                        for user in users {
                                            
                                            
                                            let  reference11 = self.db.collection(["channels", user.uid!, "thread"].joined(separator: "/"))
                                            
                                            reference11.getDocuments() { (querySnapshot11, err) in
                                                
                                                if querySnapshot11!.documents.count > 1 {
                                                    
                                                    let document11 = querySnapshot11!.documents.last
                                                    print("pppppppppppppp\(document11!.documentID) => \(document11!.data()) jndjjkakjas:-\(document11!.data()["content"] as? String ?? "")")
                                                    
                                                    if let sender = self.getuser(_users: allusers, id: user.senderID ?? ""), let reciver_id =  self.getuser(_users: allusers, id: user.reciverID ?? ""){
                                                        thereads.append(ATChatMessage(messageId: "2", messageKind: MessageKind.text(document11!.data()["content"] as? String ?? ""), createdAt: Date().yesterday, atcSender: sender, recipient: reciver_id, seenByRecipient: true))
                                                        
                                                    }
                                               
                                                    
                                                }else {
                                                    let document11 = querySnapshot11!.documents[0]
                                                    print("pppppppppppppp\(document11.documentID) => \(document11.data()) jndjjkakjas:-\(document11.data()["content"] as? String ?? "")")
                                                    if let sender = self.getuser(_users: allusers, id: user.senderID ?? ""), let reciver_id =  self.getuser(_users: allusers, id: user.reciverID ?? ""){
                                                           thereads.append(ATChatMessage(messageId: "2", messageKind: MessageKind.text(document11.data()["content"] as? String ?? ""), createdAt: Date().yesterday, atcSender: sender, recipient: reciver_id, seenByRecipient: true))
                                                    }
                                                 
                                                }
                                                
                                                //                                            for document11 in querySnapshot11!.documents {
                                                //
                                                //
                                                
                                                count = count + 1
                                                if count == users.count {
                                                    handler(users)
                                                    thereadHandler(thereads)
                                                    self.semaphore.signal()
                                                    print("usersusersusers",users.count)
                                                    
                                                    
                                                }
                                            }
                                            
                                        }

                                        self.semaphore.wait()
                                        }
                                        
                                    }
                                    
                                    
                                  
                                }
                            }
                        }
                        
                    }
                }
            }
            
  
    
    func getuser(_users:[ATCUser] , id : String) -> ATCUser? {

        for user in _users {
            if user.uid == id {
                return user
            }
        }
        return nil
    }
    func getUsers(handler: @escaping (_ allUsers: [ATCUser]) -> Void, thereadHandler: @escaping (_ allUsers: [ATChatMessage]) -> Void) {
        var users = [ATCUser]()
        var thereads = [ATChatMessage]()
        print("getting all users")
        db.collection("users").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Firebase returned an error while getting chat documents: \(err)")
            } else {
                if querySnapshot?.documents == nil{
                    print("no channels or threads found for this user's organization\n. No worries a brand new one will automatically be created when you first attempt to send a message")
                }else{
                    // Uncomment to see all documents in this user's org
                    // Usually a bad thing though, only use to debug and do not release
                    for document in querySnapshot!.documents {
                        print("sdjsdkds:- ",document.data()["first_name"] as? String ?? "")
                        users.append(ATCUser(uid: document.documentID, firstName: document.data()["first_name"] as? String ?? "", lastName: document.data()["last_name"] as? String ?? "", avatarURL: document.data()["profile"] as? String ?? "", email: document.data()["email"] as? String ?? "", isOnline: false ,senderId : "", reciverId : "") )
                        
                        
                       // print("\(document.documentID) => \(document.data())")
                    }
                    if users.count < 2{
                        return
                    }
                    //Fetch last message
                    thereads.append(ATChatMessage(messageId: "1", messageKind: MessageKind.text("from the message"), createdAt: Date().yesterday, atcSender: users[0], recipient: users[1], seenByRecipient: false))// <- try this one
                    thereads.append(ATChatMessage(messageId: "2", messageKind: MessageKind.text("Again second one"), createdAt: Date().yesterday, atcSender: users[1], recipient: users[2], seenByRecipient: true))
                    handler(users)
                    thereadHandler(thereads)
                }
            }
        }
    }

    
    /// Function ensures that all
    func checkPath(path: [String], dbRepresentation: [String:Any]){
        print("checking for the db snapshopt of main chat store: '\(path[0])'")
        db.collection(path[0]).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                
                if querySnapshot?.documents != nil {
                    print("checking for channelID: \(path[1])")
                    let channelIDRef = self.db.collection(path[0]).document(path[1])
                    channelIDRef.getDocument { (document, error) in
                        print(document?.data())
                        print(error)
                        if let document = document, document.exists {
                            
                            print("chat thread exists for \(dbRepresentation)")
                            // Uncomment to see the data description
                            //let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                        } else {
                            print("adding indexable values to the database representation")
                            var modifiedDBRepresentation = dbRepresentation
                            
                            // We will always have a thread ID with participants separated
                            // by a ':' character, so let's work with that
                            if let participants = (dbRepresentation["id"] as? String)?.components(separatedBy: ":"){
                                modifiedDBRepresentation["participants"] = participants
                                // Now, on Firestore, set your database to be indexed on "participants"
                                // This allows you to search for all documents that have a particular participant present
                                // say for example, retrieving only the threads to which the current logged-in user belongs
                            }else{
                                print("somehow we didn't have participant IDs, big issues")
                            }
                            
                            print("chat thread does not currently exist for \(dbRepresentation)")
                            print("creating chat thread for \(dbRepresentation)")
                            channelIDRef.setData(dbRepresentation) { err in
                                if let err = err {
                                    print("Firestore error returned when creating chat thread!: \(err)")
                                } else {
                                    print("chat thread successfully created")
                                }
                            }
                        }
                    }
                }else{
                    print("querySnapshot.documents was nil")
                    // TODO: usually, you don't want all of your users to see all of the other users of your app
                    // append an organization or "group" name to the path, BEFORE channels to make sure your user
                    // groups do not bleed over
                    self.db.collection(path[0]).document(path[1]).setData(dbRepresentation){ err in
                        if let err = err {
                            print("Firestore error returned when creating chat thread!: \(err)")
                        } else {
                            print("chat channel and thread successfully created!")
                        }
                    }
                }
            }
        }
    }
    
}
