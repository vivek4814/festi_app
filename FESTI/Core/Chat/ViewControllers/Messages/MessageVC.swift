//
//  MessageVC.swift
//  FESTI
//
//  Created by Salman on 04/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AVFoundation
import AVKit
import FirebaseFirestore
import Firebase
import MediaPlayer


class MessageVC: UIViewController, KeyboardHandler {
  
  //MARK: IBOutlets
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var inputTextField: UITextField!
  @IBOutlet weak var expandButton: UIButton!
  @IBOutlet weak var barBottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var stackViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet var actionButtons: [UIButton]!

  //MARK: Private properties
  private let manager = MessageManager()
  private let imageService = ImagePickerService()
  private let locationService = LocationService()
  private var messages = [ObjectMessage]()
    
  var sections = [GroupedSection<Date, ObjectMessage>]()

  
  //MARK: Public properties
  var conversation = ObjectConversation()
  var bottomInset: CGFloat {
    return view.safeAreaInsets.bottom + 50
  }
  var classPrefix : String?
    
    
  
  //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        addKeyboardObservers() {[weak self] state in
            guard state else { return }
         //   self?.tableView.scroll(to: .bottom, animated: true)
        }
        manager.managerDelegate = self
        tableView.register(UINib(nibName: "RightViewCell", bundle: nil), forCellReuseIdentifier: "RightViewCell")
        tableView.register(UINib(nibName: "LeftViewCell", bundle: nil), forCellReuseIdentifier: "LeftViewCell")
        fetchMessages()
        //fetchUserName()
        inputTextField.attributedPlaceholder = NSAttributedString(string: "Type message...",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 93.0/255.0, green: 93.0/255.0, blue: 93.0/255.0, alpha: 1.0)])
        self.addNavigationView()
        
        if self.classPrefix == "chatRequest" {
            let message = ObjectMessage()
               message.message = "We are now connected."
               message.ownerID = UserManager().currentUserID()
               inputTextField.text = nil
               showActionButtons(false)
               send(message)
        }
    }
    
    func addNavigationView() -> Void {
        
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width , height: 55))
        view.backgroundColor  = UIColor.clear
        
//        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
//        imageView.image = UIImage.init(named: "profile pic")
//        imageView.layer.cornerRadius = 17.5
//        imageView.layer.masksToBounds = true
//        view.addSubview(imageView)
//
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 80, height: 35))
        label.textAlignment = .center
        label.textColor = .white
        label.text = "Test User"
        label.font = UIFont.systemFont(ofSize: 14)
        view.addSubview(label)
        navigationItem.titleView = view
        guard let currentUserID = UserManager().currentUserID() else { return }
        guard let userID = conversation.userIDs.filter({$0 != currentUserID}).first else { return }
        
        User.getUserById(userId: userID) { (_userObj) in
//            if _userObj?.profilePic != "" {
//                imageView.setImage(url: URL(string: _userObj?.profileImage ?? ""))
//            }
            
            label.text = "\(_userObj?.firstName ?? "") \(_userObj?.lastName ?? "")"
        }


        
    }
}

extension MessageVC  {
    
    
    
    private func firstDayOfMonth(date: Date) -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        return calendar.date(from: components)!
    }

    private func parseDate(_ str : String) -> Date {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        return dateFormat.date(from: str)!
    }
  
  private func fetchMessages() {
    manager.messages(for: conversation) {[weak self] messages in
      self?.messages = messages.sorted(by: {$0.timestamp < $1.timestamp})
       
        self?.sections = GroupedSection.group(rows: self!.messages, by: { self?.firstDayOfMonth(date: Date(timeIntervalSince1970: TimeInterval($0.timestamp ))) as! Date })
        self?.sections.sort { lhs, rhs in lhs.sectionItem < rhs.sectionItem }

      self?.tableView.reloadData()
     // self?.tableView.scroll(to: .bottom, animated: true)
    }
  }
  
    private func send(_ message: ObjectMessage) {
        SwiftLoader.show(title: "Please wait...", animated: true)
        manager.create(message, conversation: conversation) {[weak self] response in
            guard let weakSelf = self else { return }
            SwiftLoader.hide()
            if response == .failure {
                weakSelf.showAlert()
                return
            }
            weakSelf.conversation.timestamp = Int(Date().timeIntervalSince1970)
            switch message.contentType {
            case .none: weakSelf.conversation.lastMessage = message.message
            case .photo: weakSelf.conversation.lastMessage = "Attachment"
            case .location: weakSelf.conversation.lastMessage = "Location"
            case .video: weakSelf.conversation.lastMessage = "Video"
            default: break
            }
            if let currentUserID = UserManager().currentUserID() {
                weakSelf.conversation.isRead[currentUserID] = true
            }
            ConversationManager().create(weakSelf.conversation)
        }
    }
  
  private func fetchUserName() {
    guard let currentUserID = UserManager().currentUserID() else { return }
    guard let userID = conversation.userIDs.filter({$0 != currentUserID}).first else { return }
    UserManager().userData(for: userID) {[weak self] user in
      guard let name = user?.name else { return }
      self?.navigationItem.title = name
    }
  }
  
  private func showActionButtons(_ status: Bool) {
    guard !status else {
      stackViewWidthConstraint.constant = 112
       //stackViewWidthConstraint.constant = 80

      UIView.animate(withDuration: 0.3) {
        self.expandButton.isHidden = true
        self.expandButton.alpha = 0
        
        self.actionButtons.forEach({$0.isHidden
            = false
            
        })
        self.view.layoutIfNeeded()
      }
      return
    }
    guard stackViewWidthConstraint.constant != 32 else { return }
    stackViewWidthConstraint.constant = 32
    UIView.animate(withDuration: 0.3) {
      self.expandButton.isHidden = false

      self.expandButton.alpha = 1
      self.actionButtons.forEach({$0.isHidden = true})
      self.view.layoutIfNeeded()
    }
  }
}

extension MessageVC : MessageManagerDelagate {
    func getImageProgressOnMessageView(value: Double) {
         print("imageProgress:-\(value)")
    }
    
    func getVideoProgressOnMessageView(value: Double) {
        print("VideoProgress:-\(value)")

    }
    
    func getSuccessfullOnImageUpload() {
        print("Image upload Successful")

    }
    
    func getSuccessfullOnIVideoUpload() {
        print("Video upload Successful")
    }
}

extension MessageVC{
  
  @IBAction func sendMessagePressed(_ sender: Any) {
    guard let text = inputTextField.text, !text.isEmpty else { return }
    let trimmedString = text.trimmingCharacters(in: .whitespaces)
    let message = ObjectMessage()
    message.message = trimmedString
    message.ownerID = UserManager().currentUserID()
    inputTextField.text = nil
    showActionButtons(false)
    send(message)
  }
  
  @IBAction func sendImagePressed(_ sender: UIButton) {
    imageService.pickImage(from: self, allowEditing: false, source: sender.tag == 0 ? .photoLibrary : .camera) {[weak self] image in
      let message = ObjectMessage()
      message.contentType = .photo
      message.profilePic = image
      message.ownerID = UserManager().currentUserID()
      self?.send(message)
      self?.inputTextField.text = nil
      self?.showActionButtons(false)
    }
  }
    
    func locationSend() -> Void {
        locationService.getLocation {[weak self] response in
          switch response {
          case .denied:
            self?.showAlert(title: "Error", message: "Please enable locattion services")
          case .location(let location):
            let message = ObjectMessage()
            message.ownerID = UserManager().currentUserID()
            message.content = location.string
            message.contentType = .location
            self?.send(message)
            self?.inputTextField.text = nil
            self?.showActionButtons(false)
          }
            
        }
        
    }
  
  @IBAction func sendLocationPressed(_ sender: UIButton) {
    imageService.pickVideo(from: self, allowEditing: false,source:  .photoLibrary) { [weak self] url in
        let message = ObjectMessage()
        message.contentType = .video
        message.videoURL = url
        message.ownerID = UserManager().currentUserID()
        self?.send(message)
        self?.inputTextField.text = nil
        self?.showActionButtons(false)
    }
  }
  
  @IBAction func expandItemsPressed(_ sender: UIButton) {
    showActionButtons(true)
  }
}

//MARK: UITableView Delegate & DataSource
extension MessageVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return 1
        return self.sections.count

    }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let section = self.sections[section]
    return section.rows.count

    //return messages.count
  }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
       // return "Today"
        let section = self.sections[section]
        let date = section.sectionItem
        return date.timeAgoSinceDate1()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        view.backgroundColor = UIColor.clear
        view.autoresizingMask = .flexibleWidth
        
        let label = UILabel()
        label.text = self.tableView(tableView, titleForHeaderInSection: section)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 20)
        label.sizeToFit()
        label.center = view.center
        label.font = UIFont.systemFont(ofSize: 13)
        label.backgroundColor = UIColor.init(red: 207/255.0, green: 220/255.0, blue: 252/255.0, alpha: 1.0)
        label.layer.cornerRadius = 10
        label.layer.masksToBounds = true
        label.autoresizingMask = []
        view.addSubview(label)
        return view
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let message = messages[indexPath.row]
        let section = self.sections[indexPath.section]
              let message = section.rows[indexPath.row]
        if message.contentType == .none {
//            if  message.ownerID == UserManager().currentUserID() {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "RightViewCell") as! RightViewCell
//
//               // cell.messageContainerView.isIncoming = false
//
//                cell.configureCell(message: message)
//                return cell
//
//            }else {
//
//                let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewCell") as! LeftViewCell
//                cell.configureCell(message: message)
//                return cell
//            }
            let cell = tableView.dequeueReusableCell(withIdentifier: message.ownerID == UserManager().currentUserID() ? "MessageTableViewCell" : "UserMessageTableViewCell") as! MessageTableViewCell
           
            cell.set(message)
            return cell
            
        }else if message.contentType == .video {
            if message.ownerID == UserManager().currentUserID() {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatVideoCell") as! ChatVideoCell
                cell.set(message)
                cell.delegate = self

                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatUserVideoCell") as! ChatUserVideoCell
                cell.set(message)
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
                
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: message.ownerID == UserManager().currentUserID() ? "MessageAttachmentTableViewCell" : "UserMessageAttachmentTableViewCell") as! MessageAttachmentTableViewCell
        cell.delegate = self
        cell.imageBubbleView?.isIncoming = (message.ownerID == UserManager().currentUserID()) ? true : false
        cell.set(message)
        return cell
    }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard tableView.isDragging else { return }
    cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
    UIView.animate(withDuration: 0.3, animations: {
      cell.transform = CGAffineTransform.identity
    })
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //let message = messages[indexPath.row]
    let section = self.sections[indexPath.section]
    let message = section.rows[indexPath.row]
    switch message.contentType {
    case .location:
        let vc: MapPreviewController = UIStoryboard.controller(storyboard: .previews)
        vc.locationString = message.content
        navigationController?.present(vc, animated: true)
    case .photo:
        let vc: ImagePreviewController = UIStoryboard.controller(storyboard: .previews)
        vc.imageURLString = message.profilePicLink
        navigationController?.present(vc, animated: true)
    case .video:
     
       dwonloVideoFromURl(url: message.videoLink ?? "")
        default: break
    }
  }
}

extension MessageVC {
    
    
    func playVido(_url : URL) -> Void {
        let player = AVPlayer(url:_url )
        let vc = AVPlayerViewController()
        vc.player = player
        self.present(vc, animated: true) { vc.player?.play() }
    }
    func dwonloVideoFromURl(url: String) {
        
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let destinationUrl = documentsUrl.appendingPathComponent(URL(string:url)!.lastPathComponent)
        
        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            debugPrint("destinationUrl.pathdestinationUrl.pathdestinationUrl.pathdestinationUrl.path",destinationUrl.path)
            DispatchQueue.main.async {
                self.playVido(_url: URL.init(fileURLWithPath: destinationUrl.path))
                
            }
            
        }
        else
        {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL(string:url)!)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
            {
                data, response, error in
                if error == nil
                {
                    if let response = response as? HTTPURLResponse
                    {
                        if response.statusCode == 200
                        {
                            if let data = data
                            {
                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                {
                                    debugPrint("scccxx",destinationUrl.path)
                                    DispatchQueue.main.async {
                                        self.playVido(_url: URL.init(fileURLWithPath: destinationUrl.path))
                                        
                                    }
                                    
                                }
                                else
                                {
                                    debugPrint("kekekeke",destinationUrl.path)
                                    DispatchQueue.main.async {
                                        self.playVido(_url: URL.init(fileURLWithPath: destinationUrl.path))
                                        
                                    }
                                    
                                }
                            }
                            else
                            {
                                debugPrint("elseelseelse",destinationUrl.path)
                            }
                        }
                    }
                }
                else
                {
                    debugPrint("errorerror",error?.localizedDescription ?? "")
                }
            })
            task.resume()
        }
        
        
    }
    
}

//MARK: UItextField Delegate
extension MessageVC: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    return textField.resignFirstResponder()
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    showActionButtons(false)
    return true
  }
}

//MARK: MessageTableViewCellDelegate Delegate
extension MessageVC: MessageTableViewCellDelegate {
  
  func messageTableViewCellUpdate() {
    tableView.beginUpdates()
    tableView.endUpdates()
  }
}


extension MessageVC: ChatVideoCellDelegate {

  func videoTableViewCellUpdate() {
    tableView.beginUpdates()
    tableView.endUpdates()
  }
}

extension MessageVC: ChatUserVideoCellDelegate {

  func userVideoTableViewCellUpdate() {
    tableView.beginUpdates()
    tableView.endUpdates()
  }
}



extension KeyboardHandler {
  func addKeyboardObservers(_ completion: CompletionObject<Bool>? = nil) {
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) {[weak self] (notification) in
      self?.handleKeyboard(notification: notification)
      completion?(true)
    }
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) {[weak self] (notification) in
      self?.handleKeyboard(notification: notification)
      completion?(false)
    }
  }
  
  private func handleKeyboard(notification: Notification) {
    guard let userInfo = notification.userInfo else { return }
    guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
    barBottomConstraint.constant = notification.name == UIResponder.keyboardWillHideNotification ? 0 : keyboardFrame.height - view.safeAreaInsets.bottom
    UIView.animate(withDuration: 0.3) {
      self.view.layoutIfNeeded()
    }
  }
}


protocol KeyboardHandler: UIViewController {
  var barBottomConstraint: NSLayoutConstraint! { get }
  var bottomInset: CGFloat { get }
}

extension KeyboardHandler {
  func addKeyboardObservers1(_ completion: CompletionObject<Bool>? = nil) {
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) {[weak self] (notification) in
      self?.handleKeyboard(notification: notification)
      completion?(true)
    }
    NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) {[weak self] (notification) in
      self?.handleKeyboard(notification: notification)
      completion?(false)
    }
  }
  
  private func handleKeyboard1(notification: Notification) {
    guard let userInfo = notification.userInfo else { return }
    guard let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
    barBottomConstraint.constant = notification.name == UIResponder.keyboardWillHideNotification ? 0 : keyboardFrame.height - view.safeAreaInsets.bottom
    UIView.animate(withDuration: 0.3) {
      self.view.layoutIfNeeded()
    }
  }
}

extension UITableView {
  
  func scroll(to: Position, animated: Bool) {
    let sections = numberOfSections
    let rows = numberOfRows(inSection: numberOfSections - 1)
    switch to {
    case .top:
      if rows > 0 {
        let indexPath = IndexPath(row: 0, section: 0)
        self.scrollToRow(at: indexPath, at: .top, animated: animated)
      }
      break
    case .bottom:
      if rows > 0 {
        let indexPath = IndexPath(row: rows - 1, section: sections - 1)
        self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
      }
      break
    }
  }
  
  enum Position {
    case top
    case bottom
  }
}

extension UIViewController {
  
  func showAlert(title: String = "Error", message: String = "Something went wrong", completion: EmptyCompletion? = nil) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: {_ in
      completion?()
    }))
    present(alert, animated: true, completion: nil)
  }
}

extension UIStoryboard {
  
  class func controller<T: UIViewController>(storyboard: StoryboardEnum) -> T {
    return UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateViewController(withIdentifier: T.className) as! T
  }
  
  class func initial<T: UIViewController>(storyboard: StoryboardEnum) -> T {
    return UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateInitialViewController() as! T
  }
  
  enum StoryboardEnum: String {
    case auth = "Auth"
    case conversations = "Conversations"
    case profile = "Profile"
    case previews = "Previews"
    case messages = "Messages"
  }
}

extension NSObject {
  class var className: String {
    return String(describing: self.self)
  }
}

extension  CLLocationCoordinate2D {
  
  var string: String {
    return "\(latitude):\(longitude)"
  }
}



extension Date {
    var day_name: Int? {
        let components = Calendar.current.dateComponents([.day], from: self)
        return components.day
    }
}
