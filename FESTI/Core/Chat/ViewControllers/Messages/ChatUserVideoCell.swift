//
//  ChatUserVideoCell.swift
//  FESTI
//
//  Created by Salman on 07/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

protocol ChatUserVideoCellDelegate: class {
  func userVideoTableViewCellUpdate()
}

class ChatUserVideoCell: UITableViewCell {
    @IBOutlet weak var _videoPreviewImageWidth: NSLayoutConstraint!
    @IBOutlet weak var _timeLbl: UILabel!
    @IBOutlet weak var _profilePic: UIImageView!
    @IBOutlet weak var _videoPreviewImageHeight: NSLayoutConstraint!
    @IBOutlet weak var _videoPreviewImage: UIImageView!
    
    weak var delegate: ChatUserVideoCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
      super.prepareForReuse()
      _videoPreviewImageHeight.constant = 150
      _videoPreviewImageHeight.constant = 150
       
    
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func set(_ message: ObjectMessage) {
        _timeLbl?.text = DateService.shared.format(Date(timeIntervalSince1970: TimeInterval(message.timestamp )))
        _timeLbl?.backgroundColor =   UIColor(white: 0, alpha: 0.2)

        guard let urlString = message.videoLink else { return }
        
        _videoPreviewImage.getThumbnailImage(url: URL(string: urlString)) {[weak self] image in
            guard let image = image, let weakSelf = self else { return }
            guard weakSelf._videoPreviewImageHeight.constant != image.size.height, weakSelf._videoPreviewImageWidth.constant != image.size.width else { return }
            if max(image.size.height, image.size.width) <= 150 {
//                weakSelf._videoPreviewImageHeight.constant = image.size.height
//                weakSelf._videoPreviewImageWidth.constant = image.size.width
               // weakSelf.delegate?.userVideoTableViewCellUpdate()
                weakSelf._videoPreviewImage.image = image


                return
            }
            weakSelf._videoPreviewImage.image = image

//            weakSelf._videoPreviewImageWidth.constant = 150
//            weakSelf._videoPreviewImageHeight.constant = 150
            //weakSelf.delegate?.userVideoTableViewCellUpdate()

        }
        
    }
    
}
