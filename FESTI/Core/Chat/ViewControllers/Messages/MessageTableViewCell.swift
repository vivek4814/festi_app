//
//  MessageAttachmentTableViewCell.swift
//  FESTI
//
//  Created by Salman on 04/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//


import UIKit

protocol MessageTableViewCellDelegate: class {
  func messageTableViewCellUpdate()
}

class MessageTableViewCell: UITableViewCell {
  
  @IBOutlet weak var profilePic: UIImageView?
  @IBOutlet weak var messageTextView: UITextView?
  
    @IBOutlet weak var bubbleView: BubbleView?
   
    @IBOutlet weak var _timeLbl: UILabel?
    
    override func draw(_ rect: CGRect) {
        
    }
    
    func set(_ message: ObjectMessage) {
       _timeLbl?.text = DateService.shared.format(Date(timeIntervalSince1970: TimeInterval(message.timestamp )))
        messageTextView?.text = message.message!  + " "
        guard let imageView = profilePic else { return }
        guard let userID = message.ownerID else { return }
        print("userIDuserID:-\(userID)")

        
        User.getUser(id: userID) { (user) in
            if user.profileImage == "" {
                return
            }
            imageView.setImage(url: URL(string: user.profileImage))

        }
       
    }
    

}

class MessageAttachmentTableViewCell: MessageTableViewCell {
  
  
    @IBOutlet weak var imageBubbleView: BubbleView?
   
    @IBOutlet weak var _imageTimeLbl: UILabel?
    @IBOutlet weak var attachmentImageView: UIImageView!
  @IBOutlet weak var attachmentImageViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var attachmentImageViewWidthConstraint: NSLayoutConstraint!
  weak var delegate: MessageTableViewCellDelegate?
  
  override func prepareForReuse() {
    super.prepareForReuse()
    attachmentImageView.cancelDownload()
  
    
    attachmentImageView.image = nil
    attachmentImageViewHeightConstraint.constant = 150
    attachmentImageViewWidthConstraint.constant = 150
    attachmentImageView.isHidden = false

  }
 
  
    override func set(_ message: ObjectMessage) {
        super.set(message)
        switch message.contentType {
        case .location:
            attachmentImageView.image = UIImage(named: "locationThumbnail")
        case .photo:
            _imageTimeLbl?.text = DateService.shared.format(Date(timeIntervalSince1970: TimeInterval(message.timestamp )))
            _imageTimeLbl?.backgroundColor =   UIColor(white: 0, alpha: 0.2)

            
            guard let urlString = message.profilePicLink else { return }
            attachmentImageView.setImage(url: URL(string: urlString)) {[weak self] image in
                guard let image = image, let weakSelf = self else { return }
                guard weakSelf.attachmentImageViewHeightConstraint.constant != image.size.height, weakSelf.attachmentImageViewWidthConstraint.constant != image.size.width else { return }
                if max(image.size.height, image.size.width) <= 150 {
//                   weakSelf.attachmentImageViewHeightConstraint.constant = image.size.height
//                    weakSelf.attachmentImageViewWidthConstraint.constant = image.size.width
//                    weakSelf.delegate?.messageTableViewCellUpdate()
                    return
                }
//               weakSelf.attachmentImageViewWidthConstraint.constant = 150
//                weakSelf.attachmentImageViewHeightConstraint.constant = 150
//                
//                weakSelf.delegate?.messageTableViewCellUpdate()
            }
        default: break
        }
    }
    

    

}


extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }

        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }

        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0

        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}
