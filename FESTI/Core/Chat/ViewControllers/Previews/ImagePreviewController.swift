//
//  ImagePreviewController.swift
//  FESTI
//
//  Created by Salman on 04/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//
import UIKit
import Kingfisher
import AVFoundation

class ImagePreviewController: UIViewController, UIScrollViewDelegate {
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var scrollView: UIScrollView!
  var imageURLString: String?
  
  @IBAction func closePressed(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func doubleTapGesture(_ sender: UITapGestureRecognizer) {
    if scrollView.zoomScale == 1 {
      scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: sender.location(in: sender.view)), animated: true)
      return
    }
    scrollView.setZoomScale(1, animated: true)
  }
  
  private func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
    var zoomRect = CGRect.zero
    zoomRect.size.height = imageView.frame.size.height / scale
    zoomRect.size.width  = imageView.frame.size.width  / scale
    let newCenter = imageView.convert(center, from: scrollView)
    zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
    zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
    return zoomRect
  }
  
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return imageView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    imageView.setImage(url: URL(string: imageURLString ?? ""))
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    modalTransitionStyle = .crossDissolve
    modalPresentationStyle = .overFullScreen
  }
}

extension UIImageView {
  
  func setImage(url: URL?, completion: CompletionObject<UIImage?>? = nil) {
    kf.setImage(with: url) { result in
      switch result {
      case .success(let value):
        completion?(value.image)
      case .failure(_):
        completion?(nil)
      }
    }
  }
    
    func getThumbnailImage( url: URL?, completion: CompletionObject<UIImage?>? = nil) {
 
     DispatchQueue.global().async {
           let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

               let destinationUrl = documentsUrl.appendingPathComponent(url!.lastPathComponent)

           if FileManager().fileExists(atPath: destinationUrl.path)
           {
            debugPrint("destinationUrl.pathdestinationUrl.pathdestinationUrl.pathdestinationUrl.path",destinationUrl.path)
            // completion(destinationUrl.path, nil)
            let asset = AVAsset(url: URL.init(fileURLWithPath: destinationUrl.path))
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            avAssetImageGenerator.appliesPreferredTrackTransform = true
            let thumnailTime = CMTimeMake(value: 2, timescale: 1)
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    print("thumbImagethumbImage:-\(thumbImage)")
                    completion?(thumbImage)
                    
                    //9
                }
            } catch {
                print("ddssddsds",error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion?(nil)
                }
            }
            
            
           }
           else
           {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url!)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
            {
                data, response, error in
                if error == nil
                {
                    if let response = response as? HTTPURLResponse
                    {
                        if response.statusCode == 200
                        {
                            if let data = data
                            {
                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                {
                                    print("scccxx",destinationUrl.path)
                                    let asset = AVAsset(url: URL.init(fileURLWithPath: destinationUrl.path))
                                    let avAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                                    avAssetImageGenerator.appliesPreferredTrackTransform = true
                                    let thumnailTime = CMTimeMake(value: 2, timescale: 1)
                                    do {
                                        let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                                        let thumbImage = UIImage(cgImage: cgThumbImage) //7
                                        DispatchQueue.main.async { //8
                                            print("thumbImagethumbImage:-\(thumbImage)")
                                            completion?(thumbImage)
                                            
                                            //9
                                        }
                                    } catch {
                                        print("ddssddsds",error.localizedDescription) //10
                                        DispatchQueue.main.async {
                                            completion?(nil)
                                        }
                                    }
                                    
                                    
                                }
                                else
                                {
                                    print("kekekeke",destinationUrl.path)
                                    let asset = AVAsset(url: URL.init(fileURLWithPath: destinationUrl.path))
                                    let avAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                                    avAssetImageGenerator.appliesPreferredTrackTransform = true
                                    let thumnailTime = CMTimeMake(value: 2, timescale: 1)
                                    do {
                                        let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                                        let thumbImage = UIImage(cgImage: cgThumbImage) //7
                                        DispatchQueue.main.async { //8
                                            print("thumbImagethumbImage:-\(thumbImage)")
                                            completion?(thumbImage)
                                            
                                            //9
                                        }
                                    } catch {
                                        print("ddssddsds",error.localizedDescription) //10
                                        DispatchQueue.main.async {
                                            completion?(nil)
                                        }
                                    }
                                    
                                }
                            }
                            else
                            {
                                print("elseelseelse",destinationUrl.path)
                            }
                        }
                    }
                }
                else
                {
                    print("errorerror",error)
                }
            })
            task.resume()
        }
        
        
        
        
        
           
        }
        
   
   
        
    }
  
  func cancelDownload() {
    kf.cancelDownloadTask()
  }
}
