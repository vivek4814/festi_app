//
//  ObjectMessage.swift
//  FESTI
//
//  Created by Salman on 04/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

class ObjectMessage: FireStorageCodable {
  
  var id = UUID().uuidString
  var message: String?
  var content: String?
  var contentType = ContentType.none
  var timestamp = Int(Date().timeIntervalSince1970)
  var ownerID: String?
  var profilePicLink: String?
  var profilePic: UIImage?
  var videoLink: String?
  var videoURL: URL?


  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(id, forKey: .id)
    try container.encodeIfPresent(message, forKey: .message)
    try container.encodeIfPresent(timestamp, forKey: .timestamp)
    try container.encodeIfPresent(ownerID, forKey: .ownerID)
    try container.encodeIfPresent(profilePicLink, forKey: .profilePicLink)
    try container.encodeIfPresent(contentType.rawValue, forKey: .contentType)
    try container.encodeIfPresent(content, forKey: .content)
    try container.encodeIfPresent(videoLink, forKey: .videoLink)

  }
  
  init() {}
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    id = try container.decode(String.self, forKey: .id)
    message = try container.decodeIfPresent(String.self, forKey: .message)
    timestamp = try container.decodeIfPresent(Int.self, forKey: .timestamp) ?? Int(Date().timeIntervalSince1970)
    ownerID = try container.decodeIfPresent(String.self, forKey: .ownerID)
    profilePicLink = try container.decodeIfPresent(String.self, forKey: .profilePicLink)
    videoLink = try container.decodeIfPresent(String.self, forKey: .videoLink)
    content = try container.decodeIfPresent(String.self, forKey: .content)
    if let contentTypeValue = try container.decodeIfPresent(Int.self, forKey: .contentType) {
      contentType = ContentType(rawValue: contentTypeValue) ?? ContentType.unknown
    }
  }
}

extension ObjectMessage {
  private enum CodingKeys: String, CodingKey {
    case id
    case message
    case timestamp
    case ownerID
    case profilePicLink
    case contentType
    case videoLink
    case content
  }
  
  enum ContentType: Int {
    case none
    case photo
    case location
    case video
    case unknown
  }
}

protocol FireStorageCodable: FireCodable {
  
  var profilePic: UIImage? { get set }
  var videoURL: URL? { get set }
  var profilePicLink: String? { get set }
  var videoLink: String? { get set }

}

protocol BaseCodable: class {
  
  var id: String { get set }
  
}

protocol FireCodable: BaseCodable, Codable {
  
  var id: String { get set }
  
}
