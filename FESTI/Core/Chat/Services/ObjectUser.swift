//
//  ObjectUser.swift
//  FESTI
//
//  Created by Salman on 04/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import UIKit

class ObjectUser: FireStorageCodable {
    
  var videoLink: String?
  var id = UUID().uuidString
  var name: String?
  var email: String?
  var profilePicLink: String?
  var profilePic: UIImage?
  var videoURL: URL?

  var password: String?
  var first_name: String?
   var last_name: String?

  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(id, forKey: .id)
    try container.encodeIfPresent(name, forKey: .name)
    try container.encodeIfPresent(email, forKey: .email)
    try container.encodeIfPresent(first_name, forKey: .first_name)
    try container.encodeIfPresent(last_name, forKey: .last_name)
    try container.encodeIfPresent(profilePicLink, forKey: .profilePicLink)
    try container.encodeIfPresent(videoLink, forKey: .videoLink)

  }
  
  init() {}
  
  public required convenience init(from decoder: Decoder) throws {
    self.init()
    let container = try decoder.container(keyedBy: CodingKeys.self)
    id = try container.decode(String.self, forKey: .id)
    name = try container.decodeIfPresent(String.self, forKey: .name)
    email = try container.decodeIfPresent(String.self, forKey: .email)
    profilePicLink = try container.decodeIfPresent(String.self, forKey: .profilePicLink)
    first_name = try container.decodeIfPresent(String.self, forKey: .first_name)
    last_name = try container.decodeIfPresent(String.self, forKey: .last_name)
    videoLink = try container.decodeIfPresent(String.self, forKey: .videoLink)

  }
}

extension ObjectUser {
  private enum CodingKeys: String, CodingKey {
    case id
    case email
    case name
    case profilePicLink
    case first_name
    case last_name
    case videoLink


  }
}
