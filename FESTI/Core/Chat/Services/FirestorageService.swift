//
//  FirestorageService.swift
//  FESTI
//
//  Created by Salman on 04/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import FirebaseStorage
import UIKit
import AVFoundation
import AVKit

protocol FirestorageServiceDelegate {
    func tarckImageProgress(value : Double)
    func SuccessFullyUplodedImage()
    func tarckVideoUploadProgress(value : Double)
    func SuccessFullyUplodedVideo()

}

public typealias EmptyCompletion = () -> Void
public typealias CompletionObject<T> = (_ response: T) -> Void
public typealias CompletionOptionalObject<T> = (_ response: T?) -> Void
public typealias CompletionResponse = (_ response: Result<Void, Error>) -> Void

class FirestorageService : NSObject {
    
    var delgate :  FirestorageServiceDelegate?
    
    
    func update<T>(_ object: T, reference: FirestoreCollectionReference, completion: @escaping CompletionObject<FirestoreResponse>) where T: FireStorageCodable {
        
        guard let imageData = object.profilePic?.scale(to: CGSize(width: 350, height: 350))?.jpegData(compressionQuality: 0.3) else { completion(.success); return }
        let ref = Storage.storage().reference().child(reference.rawValue).child(object.id).child(object.id + ".jpg")
        let uploadMetadata = StorageMetadata()
        uploadMetadata.contentType = "image/jpg"
       let uploadTask =  ref.putData(imageData, metadata: uploadMetadata) { (_, error) in
            guard error.isNone else { completion(.failure); return }
            ref.downloadURL(completion: { (url, err) in
                if let downloadURL = url?.absoluteString {
                    object.profilePic = nil
                    object.profilePicLink = downloadURL
                    print("Sucess ")
                    completion(.success)
                    return
                }
                completion(.failure)
            })
        }
        
        uploadTask.observe(.progress) { (snapshot) in
           
             let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount)
               / Double(snapshot.progress!.totalUnitCount)
            self.delgate?.tarckImageProgress(value: percentComplete)
      
        }
        
        uploadTask.observe(.success) { snapshot in
          // Upload completed successfully
            self.delgate?.SuccessFullyUplodedImage()
        }
        
       
    }
    
    func updateVideo<T>(_ object: T, reference: FirestoreCollectionReference, completion: @escaping CompletionObject<FirestoreResponse>) where T: FireStorageCodable {
        
        let name = "\(Int(Date().timeIntervalSince1970)).mp4"
        let path = NSTemporaryDirectory() + name
        
        let dispatchgroup = DispatchGroup()
        
        dispatchgroup.enter()
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let outputurl = documentsURL.appendingPathComponent(name)
        var ur = outputurl
        self.convertVideo(toMPEG4FormatForVideo: object.videoURL!, outputURL: outputurl) { (session) in
            
            ur = session.outputURL!
            dispatchgroup.leave()
            
        }
        dispatchgroup.wait()
        
        do {
            let imageData = try Data(contentsOf: ur as URL)
            
            let ref = Storage.storage().reference().child(reference.rawValue).child(object.id).child(object.id + ".mp4")
            let uploadMetadata = StorageMetadata()
            uploadMetadata.contentType = ".mp4"
        let uploadTask = ref.putData(imageData, metadata: uploadMetadata) { (_, error) in
                guard error.isNone else { completion(.failure); return }
                ref.downloadURL(completion: { (url, err) in
                    if let downloadURL = url?.absoluteString {
                        object.videoLink = nil
                        object.videoLink = downloadURL
                        completion(.success)
                        return
                    }
                    completion(.failure)
                })
            }
            
            uploadTask.observe(.progress) { (snapshot) in
               
                 let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount)
                   / Double(snapshot.progress!.totalUnitCount)
                self.delgate?.tarckVideoUploadProgress(value: percentComplete)
               
            }
            
            uploadTask.observe(.success) { snapshot in
              // Upload completed successfully
                self.delgate?.SuccessFullyUplodedVideo()
            }
            
        } catch {
            print("Unable to load data: \(error)")
        }
        
        
    }
    
    func convertVideo(toMPEG4FormatForVideo inputURL: URL, outputURL: URL, handler: @escaping (AVAssetExportSession) -> Void) {
       // try! FileManager.default.removeItem(at: outputURL as URL)
        let asset = AVURLAsset(url: inputURL as URL, options: nil)
        
        let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        exportSession.exportAsynchronously(completionHandler: {
            handler(exportSession)
        })
    }
}

public enum FirestoreCollectionReference: String {
  case users = "users"
  case conversations = "Conversations"
  case messages = "Messages"
}

public enum FirestoreResponse {
  case success
  case failure
}

extension Optional {
  var isNone: Bool {
    return self == nil
  }
  
  var isSome: Bool {
    return self != nil
  }
}
extension UIImage {
  
  func fixOrientation() -> UIImage {
    if (imageOrientation == .up) { return self }
    UIGraphicsBeginImageContextWithOptions(size, false, scale)
    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    draw(in: rect)
    let image = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return image
  }
  
  func scale(to newSize: CGSize) -> UIImage? {
    let horizontalRatio = newSize.width / size.width
    let verticalRatio = newSize.height / size.height
    let ratio = max(horizontalRatio, verticalRatio)
    let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
    draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage
  }
}
