//
//  DateService.swift
//  FESTI
//
//  Created by Salman on 04/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import Foundation

class DateService {
  
  static let shared = DateService()
  private let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
   // formatter.timeStyle = .short
    formatter.dateFormat = "h:mm a, MMM dd, yyyy"
    return formatter
  }()
    
    private let dateFormatter1: DateFormatter = {
      let formatter = DateFormatter()
     // formatter.timeStyle = .short
      formatter.dateFormat = "h:mm a"
      return formatter
    }()
  
  private init() {}
  
  func format(_ date: Date) -> String {
    return dateFormatter1.string(from: date)
  }
}
