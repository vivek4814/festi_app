//
//  MessageManager.swift
//  FESTI
//
//  Created by Salman on 04/05/20.
//  Copyright © 2020 Salman. All rights reserved.
//

import Foundation

protocol MessageManagerDelagate {
    func getImageProgressOnMessageView(value : Double)
    func getVideoProgressOnMessageView(value : Double)
    func getSuccessfullOnImageUpload()
    func getSuccessfullOnIVideoUpload()

}

class MessageManager: FirestorageServiceDelegate {
    func tarckVideoUploadProgress(value: Double) {
        managerDelegate?.getVideoProgressOnMessageView(value: value)
    }
    
    func SuccessFullyUplodedVideo() {
        managerDelegate?.getSuccessfullOnIVideoUpload()
    }
    
    func tarckImageProgress(value: Double) {
        managerDelegate?.getImageProgressOnMessageView(value: value)
    }
    
    func SuccessFullyUplodedImage() {
        managerDelegate?.getSuccessfullOnImageUpload()

    }

  
    
  
  let service = FirestoreService()
  let service_object = FirestorageService()
  var managerDelegate : MessageManagerDelagate?
    
  
  func messages(for conversation: ObjectConversation, _ completion: @escaping CompletionObject<[ObjectMessage]>) {
    let reference = FirestoreService.Reference(first: .conversations, second: .messages, id: conversation.id)
    service.objectWithListener(ObjectMessage.self, reference: reference) { results in
      completion(results)
    }
  }
  
    func create(_ message: ObjectMessage, conversation: ObjectConversation, _ completion: @escaping CompletionObject<FirestoreResponse>) {
        

        if message.contentType == .video {
            
            service_object.updateVideo(message, reference: .messages) { response in
                switch response {
                case .failure: completion(response)
                case .success:
                    let reference = FirestoreService.Reference(first: .conversations, second: .messages, id: conversation.id)
                    FirestoreService().update(message, reference: reference) { result in
                        completion(result)
                    }
                    if let id = conversation.isRead.filter({$0.key != UserManager().currentUserID() ?? ""}).first {
                        conversation.isRead[id.key] = false
                    }
                    ConversationManager().create(conversation)
                }
            }
            
        }else {
            service_object.delgate = self
            service_object.update(message, reference: .messages) { response in
                switch response {
                case .failure: completion(response)
                case .success:
                    let reference = FirestoreService.Reference(first: .conversations, second: .messages, id: conversation.id)
                    FirestoreService().update(message, reference: reference) { result in
                        completion(result)
                    }
                    if let id = conversation.isRead.filter({$0.key != UserManager().currentUserID() ?? ""}).first {
                        conversation.isRead[id.key] = false
                    }
                    ConversationManager().create(conversation)
                }
            }
        }
        
        
    }
}
